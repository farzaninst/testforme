<?php

class CenterUsers extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $validator;
	private $paginate=20;
	private $keys=['نام','نام خانوادگی','تحصیلات','موبایل','ایمیل','جنسیت','تاهل','تاریخ تولد','تاریخ ازدواج'];
	private $education=['مشخص نیست','زیر دیپلم','دیپلم','فوق دیپلم','کارشناسی','کارشناسی ارشد','دکتری و بالاتر'];
	public function sampleExcel()
	{
		$messengers=TbAdminMessengers::all()->toArray();
		$msg=[];
		foreach ($messengers as $value) {
			$msg[]=$value['name'];
		}
		$excelKeys=array_merge($this->keys,$msg);
		Excel::create('file',function($excel) use($excelKeys) 
			{
				$excel->setTitle('بارگزاری کاربران به سایت');
			    // Chain the setters
			    $excel->setCreator('موسسه فرزان')
			          ->setCompany('موسسه فرزان');

			    // Call them separately
    			$excel->setDescription('مرکز مشاوره، اطلاع رسانی و خدمات کار آفرینی فرزان');
    			$excel->sheet('Sheet1', function($sheet) use($excelKeys) {

			        $head = $excelKeys;
			        $data = array($head);
			        $sheet->fromArray($data, null, 'A1', false, false);
			        $sheet->freezeFirstRow();
			        $sheet->cells('A1:K1', function($cells) {
			        	$cells->setBackground('#585858');
			        	$cells->setFontColor('#ffffff');
			        	$cells->setAlignment('center');
			        });
			        $sheet->cells('A1', function($cells) {
			        	$cells->setBackground('#ff000000');
			        });
			        $sheet->cells('B1', function($cells) {
			        	$cells->setBackground('#ff000000');
			        });
			        $sheet->cells('D1', function($cells) {
			        	$cells->setBackground('#ff000000');
			        });
			        //$sheet->setAllBorders('thin');
			        $sheet->setWidth('A', 20);
			        $sheet->setWidth('B', 20);
			        $sheet->setWidth('C', 20);
			        $sheet->setWidth('D', 20);
			        $sheet->setWidth('E', 20);
			        $sheet->setWidth('F', 7);
			        $sheet->setWidth('G', 5);
			        $sheet->setWidth('H', 20);
			        $sheet->setWidth('I', 20);
			        $sheet->setWidth('J', 7);
			        $sheet->setWidth('K', 7);
			        $sheet->setColumnFormat(array(
			        	'D' =>'@',
					    'H' => 'dd-mm-yyyy',
					    'I' => 'dd-mm-yyyy',
					    'F' => '0',
					    'G' => '0',
					    'J' => '0',
					    'K' => '0',
					));
			    });
				$excel->sheet('راهنما', function($sheet){

			        $head =array('راهنمای وارد کردن اطلاعات به فایل اکسل');
			        $data = array($head);
			        $sheet->fromArray($data, null, 'A1', false, false);
			        $sheet->freezeFirstRow();
			        $sheet->cells('A1', function($cells) {
			        	$cells->setBackground('#585858');
			        	$cells->setFontColor('#ffffff');
			        	$cells->setAlignment('center');
			        });
			        $sheet->cells('A2:A20', function($cells) {
			        	$cells->setAlignment('right');
			        });
			        $sheet->setWidth('A', 120);
			        $sheet->appendRow(2,array('برای وارد کردن اطلاعات sheet1 را انتخاب کنید'));
			        $sheet->appendRow(4,array(' ستون های قرمز رنگ اجباری است'));
			        $sheet->appendRow(6,array('در ستون جنسیت : برای زن عدد 0 و برای مرد عدد 1 را وارد کنید.'));
			        $sheet->appendRow(8,array('در ستون های  تلگرام، واتس آپ و ... : اگر کاربر شما از این نرم افزارها استفاده می کند عدد 1 در غیر این صورت عدد 0 را وارد کنید. فیلد خالی به معنی 0 است.'));
			    	$sheet->appendRow(10,array(' تاریخ تولد و تاریخ ازدواج باید به صورت سال/ماه/روز (مثلا 2/1/1394 به معنای دوم فروردین سال 1394) وارد شود'));
			    	$sheet->appendRow(12,array(' شماره موبایل ها باید با صفر شروع شوند یا +98 یا 0098'));
			    	$sheet->appendRow(14,array(' ستون ایمیل می تواند خالی باشد ولی در صورت وارد کردن آدرس ایمیل باید معتبر باشد'));
			    	$sheet->appendRow(16,array(' در ستون تاهل برای مجرد عدد 0 و برای متاهل عدد 1 را وارد کنید'));
			    	$sheet->appendRow(18,array(' ستون تحصیلات باید به صورت زیر تکمیل شود:'));
			    	$sheet->appendRow(20,array('برای حالتی که تحصیلات مشخص نیست عدد 0 را وارد کنید'));
			    	$sheet->appendRow(21,array('برای زیر دیپلم عدد 1 را وارد کنید'));
			    	$sheet->appendRow(22,array('برای دیپلم عدد 2 را وارد کنید'));
			    	$sheet->appendRow(23,array('برای فوق دیپلم عدد 3 را وارد کنید'));
			    	$sheet->appendRow(24,array('برای کارشناسی عدد 4 را وارد کنید'));
			    	$sheet->appendRow(25,array('برای کارشناسی ارشد عدد 5 را وارد کنید'));
			    	$sheet->appendRow(26,array('برای دکتری و بالاتر عدد 6 را وارد کنید'));
			    });
			})->export('xls');
		//return Response::download(storage_path('exports').'/file.xls');
	}
	public function helpExcel()
	{
		return Response::download(storage_path('exports').'/userHelp.txt');
		//Excel::create('file')->export('xls');
	}
	public function index($messages=null)
	{
		if(Session::has('messages'))
			$messages=Session::pull('messages');
		$centerid=Session::get('centerid');
		$users=TbCenterUsers::where('centerid','=',$centerid)->where('type','=',0)->get();
		return View::make('users/all')->with('users',$users)->with('messages',$messages)->with('education',$this->education);
	}
	public function indexExcel($messages=null)
	{
		//$centerid=Session::get('centerid');
		return View::make('users/allExcel')->with('messages',$messages);
	}
	public function indexGroup($messages=null)
	{
		if(Session::has('messages'))
			$messages=Session::pull('messages');
		$centerid=Session::get('centerid');
		//var_dump($centerid);
		$groups=TbCenterGroups::where('centerid','=',$centerid)->get();
		//$groups=TbCenterGroups::all();
		return View::make('users/allGroup')->with('groups',$groups)->with('messages',$messages);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$services=TbAdminServices::all();
		$centerid=Session::get('centerid');
		$center=TbAdminCenters::find($centerid);
		/*$pattern=TbAdminServicesACC::where('id','=',$center->serviceACCid)->pluck('pattern');
		$services=TbAdminServices::all();
		if($pattern)
		{
			$pattid=explode(',',$pattern);
			$srv=$services->filter(function($item) use($pattid)
				{
					if(!$item->enable)
						return false;
					if(in_array($item->id,$pattid))
						return true;
				});
		}
		else
			$srv=null;*/
		$softwares=TbAdminMessengers::all();
		return View::make('users/create')
					->with('softwares',$softwares);
					//->with('services',$srv);
	}
	public function createGroup()
	{
		return View::make('users/createGroup');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validate=$this->storeValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$user=new TbCenterUsers;
			$user->fname=Input::get('fname');
			$user->lname=Input::get('lname');
			$user->phone=$this->phoneWith98(Input::get('phone'));
			$user->email=Input::get('email');
			$user->education=Input::get('education');
			$user->sex=Input::get('sex');
			$user->single=Input::get('single');
			if(!empty(Input::get('birthdate')))
				$user->birthdate=Input::get('birthdate');
			else
				$user->birthdate=null;
			if(!empty(Input::get('marriagedate')))
				$user->marriagedate=Input::get('marriagedate');
			else
				$user->marriagedate=null;
			$user->userServiceACCid=null;
			//$user->userServiceACCid=$this->setServicePattern();
			$user->messengerid=$this->setMessengerPattern();
			$user->centerid=Session::get('centerid');
			$user->save();
			return Redirect::action('CenterUsers@index');
			//return $this->index();
		}
		else
		{
			//$services=TbAdminServices::all();
			$softwares=TbAdminMessengers::all();
			return View::make('users/create')
						->with('softwares',$softwares)
						->with('fname',Input::get('fname'))
						->with('lname',Input::get('lname'))
						->with('email',Input::get('email'))
						//->with('services',$services)
						->with('messages',$messages->getMessages());
		}
	}
	public function storeGroup()
	{
		$validate=$this->storeGroupValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$group=new TbCenterGroups;
			$group->name=Input::get('name');
			$group->desc=Input::get('desc');
			$group->centerid=Session::get('centerid');
			$group->save();
			return Redirect::action('CenterUsers@indexGroup');
			//return $this->indexGroup();
		}
		else
		{
			return View::make('users/createGroup')
						->with('name',Input::get('name'))
						->with('desc',Input::get('desc'))
						->with('messages',$messages->getMessages());
		}
	}
	public function storeExcel()
	{
		if(!Input::hasFile('file'))
			return $this->indexExcel('لطفا فایل اکسل را انتخاب کنید');

		$file=Input::file('file');
		//dd($file->getMimeType());
		if(!($file->getMimeType()=='application/vnd.ms-office'|| $file->getMimeType()=='application/vnd.ms-excel'))
			return $this->indexExcel('فایل ورودی باید یک فایل اکسل باشد');

		$name=time().'_'.$file->getClientOriginalName();
		$file->move(storage_path('exports') . '/imports',$name);

		$arr=Excel::selectSheetsByIndex(0)->load(storage_path('exports') . '/imports/'.$name);
		$excel=$arr->toArray();
		$first=$arr->first()->toArray();
		//var_dump($arr);
		//var_dump($excel);
		//dd();
		$head=array_keys($first);
		//var_dump($head);
		//dd($excel);
	//	return View::make('exceltest')->with('data',$head);
		//dd();
		//$keys=['نام','تلفن','ایمیل','جنسیت','تاهل','تاریخ تولد','تاریخ ازدواج'];
		$intsec=array_intersect($this->keys,$head);
		if(count($intsec)!=count($this->keys))
			return $this->indexExcel('فایل ورودی باید مطابق با نمونه فایل اکسل پر شود');
		
		$validate=$this->storeExcelValidation($excel,'نام','نام خانوادگی','ایمیل','موبایل','تاریخ تولد','تاریخ ازدواج');
		//var_dump($excel);
		if(!$validate)
			return $this->indexExcel('مقادیر وارد شده در فایل اکسل معتبر نمی باشد. لطفا مقادیر را با توجه به فایل راهنما پر کنید');

		$centerid=Session::get('centerid');
		$center=TbAdminCenters::find($centerid);
		/*$pattern=TbAdminServicesACC::where('id','=',$center->serviceACCid)->pluck('pattern');
		$services=TbAdminServices::all();
		if($pattern)
		{
			$pattid=explode(',',$pattern);
			$srv=$services->filter(function($item) use($pattid)
				{
					if(!$item->enable)
						return false;
					if(in_array($item->id,$pattid))
						return true;
				});
		}
		else
			$srv=null;*/
		$softwares=TbAdminMessengers::all();
		//if($srv)
		//	$srv=$srv->toArray();
		//else
		//	$srv=[];
		$soft=$softwares->toArray();
		$addusers=0;
		foreach($excel as $ex)
		{
			$user=new TbCenterUsers;
			$user->fname=$ex['نام'];
			$user->lname=$ex['نام خانوادگی'];
			$user->phone=$this->phoneWith98($ex['موبایل']);
			$user->email=$ex['ایمیل'];
			$user->sex=($ex['جنسیت']!=null)?$ex['جنسیت']:0;
			$user->education=(in_array($ex['تحصیلات'],[0,1,2,3,4,5,6]))?$ex['تحصیلات']:0;
			$user->single=($ex['تاهل']!=null)?$ex['تاهل']:0;
			if(!empty($ex['تاریخ تولد']))
				$user->birthdate=\Miladr\Jalali\jDateTime::toGregorianStr($ex['تاریخ تولد']);
			else
				$user->birthdate=null;
			if(!empty($ex['تاریخ ازدواج']))
				$user->marriagedate=\Miladr\Jalali\jDateTime::toGregorianStr($ex['تاریخ ازدواج']);
			else
				$user->marriagedate=null;
			//$user->messengerid=$this->setMessengerPattern();
			$msgid='';
			foreach($soft as $s)
			{
				if(isset($ex[$s['name']]) && $ex[$s['name']]=='1')
				{
					$msgid.=$s['id'].',';
				}
			}
			if($msgid=='')
				$user->messengerid=null;
			else
			{
				$msgid=substr($msgid,0,strlen($msgid)-1);
				$pattacc=TbAdminMessengersACC::all();
				$pattid=null;
				foreach ($pattacc as $acc) 
				{
					if($acc->pattern==$msgid)
					{
						$pattid=$acc->id;
						break;
					}
						
				}
				$user->messengerid=$pattid;
			}
			//$user->userServiceACCid=$this->setServicePattern();
			/*$srvid='';
			foreach($srv as $s)
			{
				if(isset($ex[$s['name']]) && $ex[$s['name']]=='1')
				{
					$srvid.=$s['id'].',';
				}
			}
			if($srvid=='')
				$user->userServiceACCid=null;
			else
			{
				$srvid=substr($srvid,0,strlen($srvid)-1);
				$pattacc=TbAdminServicesACC::all();
				$pattid=null;
				foreach ($pattacc as $acc) 
				{
					if($acc->pattern==$srvid)
					{
						$pattid=$acc->id;
						break;
					}
						
				}
				$user->userServiceACCid=$pattid;
			}*/
			$user->userServiceACCid=null;
			$user->centerid=Session::get('centerid');
			$user->save();
			$addusers++;
		}//foreach end users add :excel as ex
		return Redirect::action('CenterUsers@index')->with('messages','تعداد '.$addusers.' به کاربران اضافه شد.');
		//return $this->index('تعداد '.$addusers.' به کاربران اضافه شد.');

	}// function end

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user=TbCenterUsers::where('type','=',0)->find($id);
		if(count($user))
		{
			//$services=$this->activeServices($user->userServiceACCid);
			$softwares=$this->activeSoftwares($user->messengerid);
			return View::make('users/edit')->with('user',$user)
							//->with('services',$services)
							->with('softwares',$softwares);
		}
		else
		{
			return "bad request";
		}
	}
	
	public function editGroup($id)
	{
		$group=TbCenterGroups::where('centerid','=',Session::get('centerid'))->find($id);
		if(!$group)
		{
			return "bad request";
		}

		return View::make('users/editGroup')->with('group',$group);
	}

	public function editGroupUsers($id)
	{
		$centerid=Session::get('centerid');
		//var_dump($centerid);
		$groups=TbCenterGroups::where('centerid','=',$centerid)->where('id','=',$id)->get();
		if(count($groups)==0)
			return "bad request";
		else
			$group=$groups[0];
		if (Session::has('page'))
		{
		    Paginator::setCurrentPage(Session::pull('page', '1'));
		}
		
		$allUsers=TbCenterUsers::where('centerid','=',Session::get('centerid'))->where('type','=',0)->paginate($this->paginate);
		$groupUsers=TbUsersGroups::where('groupid','=',$id)->lists('userid');
		if(!is_null($groupUsers)&& !is_array($groupUsers))
			$groupUsers = array($groupUsers);

			//$groupUsers=$groupUsers->toArray();
		//var_dump($allUsers);
		//var_dump($groupUsers);
		if (Session::has('updatemessage'))
		{
		   $updatemessage=Session::pull('updatemessage', '');
		}
		else
		{
			$updatemessage=null;
		}
		return View::make('users/editGU')
					->with('allusers',$allUsers)
					->with('group',$group)
					->with('pageusers','')
					->with('updatemessage',$updatemessage)
					->with('groupusers',$groupUsers);
	}
	public function updateGroupUsers($id)
	{
		//var_dump(Input::all());
		//var_dump($page);
		$page=Input::get('page');
		$pageusers=Input::get('pageusers');
		$pageusers=explode(',',$pageusers);
		//var_dump($pageusers);
		$selecteduser=Input::get('selecteduser');
		foreach($pageusers as $userid)
		{
			if($userid=='')
				continue;
			$user=TbUsersGroups::where('groupid','=',$id)->where('userid','=',$userid)->get();
			//echo 'count === '.count($user);
			//var_dump($user);
			if(isset($selecteduser[$userid]))
			{
				if(count($user)==0)
				{
					$newuser= new TbUsersGroups;
					$newuser->groupid=$id;
					$newuser->userid=$userid;
					$newuser->save();
				}
			}
			else
			{
				if(count($user)>0)
				{
					//var_dump($user[0]);
					TbUsersGroups::destroy($user[0]->id);
				}
			}
		}
		//var_dump(Paginator::getCurrentPage());
		Session::put('page', $page);
		$updatemessages="ویرایش با موفقیت انجام شد.";
		return Redirect::to('/users/groups/'.$id.'/users')->with(['updatemessage'=>$updatemessages]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateGroup($id)
	{
		$group=TbCenterGroups::find($id);
		if(!$group)
		{
			return "bad request";
		}
		$validate=$this->updateGroupValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$group->name=Input::get('name');
			$group->desc=Input::get('desc');
			$group->save();
			return Redirect::action('CenterUsers@indexGroup');
			//return $this->indexGroup();
		}
		else
		{
			return View::make('users/editGroup')
						->with('name',Input::get('name'))
						->with('desc',Input::get('desc'))
						->with('group',$group)
						->with('messages',$messages->getMessages());
		}
	}
	public function update($id)
	{
		//var_dump(Input::all());
		$validate=$this->storeValidation();
		$messages = $this->validator->messages();
		$user=TbCenterUsers::where('type','=',0)->find(Input::get('id'));
		if(count($user)==0)
			return "bad request";
		if($validate)
		{
			//$user=new TbCenterUsers;
			$user->fname=Input::get('fname');
			$user->lname=Input::get('lname');
			$user->phone=$this->phoneWith98(Input::get('phone'));
			$user->email=Input::get('email');
			$user->education=Input::get('education');
			$user->sex=Input::get('sex');
			$user->single=Input::get('single');
			if(!empty(Input::get('birthdate')))
				$user->birthdate=Input::get('birthdate');
			else
				$user->birthdate=null;
			if(!empty(Input::get('marriagedate')))
				$user->marriagedate=Input::get('marriagedate');
			else
				$user->marriagedate=null;
			$user->userServiceACCid=null;
			//$user->userServiceACCid=$this->setServicePattern();
			$user->messengerid=$this->setMessengerPattern();
			$user->centerid=Session::get('centerid');
			$user->save();
			return Redirect::action('CenterUsers@index');
			//return $this->index();
		}
		else
		{
			//$services=TbAdminServices::all();
			//$softwares=TbAdminMessengers::all();
			$softwares=$this->activeSoftwares($user->messengerid);
			return View::make('users/edit')
						->with('softwares',$softwares)
						->with('fname',Input::get('fname'))
						->with('lname',Input::get('lname'))
						->with('email',Input::get('email'))
						->with('phone',Input::get('phone'))
						->with('user',$user)
					//	->with('services',$services)
						->with('messages',$messages->getMessages());
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroyGroup($id)
	{
		$centerid=Session::get('centerid');

		$group=TbCenterGroups::where('centerid','=',$centerid)->where('id','=',$id)->get();
		if(count($group)==0)
		{
			return "bad request";
		}
		else
		{
			$result=TbUsersGroups::where('groupid','=',$id)->delete();
			$result=TbCenterGroups::destroy($id);
			return Redirect::action('CenterUsers@indexGroup')->with('messages','حذف گروه انجام شد');
			//return $this->indexgroup('حذف گروه انجام شد');
		}
	}
	public function destroy($id)
	{
		$centerid=Session::get('centerid');

		$user=TbCenterUsers::where('centerid','=',$centerid)->where('id','=',$id)->get();
		if(count($user)==0)
		{
			return "bad request";
		}
		else
		{
			$result=TbCenterUsers::destroy($id);
			return Redirect::action('CenterUsers@index')->with('messages','کاربر با موفقیت حذف شد');
			//return $this->index('کاربر با موفقیت حذف شد');
		}
	}
	private function storeValidation()
	{
		$role=array('fname'=>'required',
					'lname'=>'required',
					'email'=>'email',
					'phone'=>'phone:IR,mobile');
		$this->validator=\Validator::make(
			array('fname'=>Input::get('fname'),
				'lname'=>Input::get('lname'),
				'email'=>Input::get('email'),
				'phone'=>Input::get('phone')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function storeGroupValidation()
	{
		$role=array('name'=>'required');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function storeExcelValidation($excel,$fname,$lname,$email,$phone,$birthdate,$marriagedate)
	{
		$role=array($fname=>'required',
					$lname=>'required',
					$email=>'email',
					$phone=>'phone:IR,mobile');
		$num=0;
		foreach($excel as $ex)
		{

			$this->validator=\Validator::make(
				array($fname=>$ex[$fname],
					$lname=>$ex[$lname],
					$email=>$ex[$email],
					$phone=>$ex[$phone]),$role);
			if($this->validator->fails())
			{
				return false;
			}
			if(!$this->jalaliValidation($ex[$birthdate]))
				return false;
			if(!$this->jalaliValidation($ex[$marriagedate]))
				return false;
			$num++;
		}
		return true;
	}
	public function jalaliValidation($jdate)
	{
		if(is_null($jdate))
			return true;
		if(empty($jdate))
			return true;
		$jarr=explode('/',$jdate);
		if(count($jarr)!=3)
			return false;
		if(strlen($jarr[0])>2 && strlen($jarr[0])<1)
			return false;
		if(strlen($jarr[1])>2 && strlen($jarr[1])<1)
			return false;
		if(strlen($jarr[2])>4 && strlen($jarr[2])<2)
			return false;
		return true;
	}
	private function updateValidation()
	{
		$role=array('fname'=>'required',
					'lname'=>'required',
					'email'=>'email',
					'phone'=>'phone:IR,mobile');
		$this->validator=\Validator::make(
			array('fname'=>Input::get('fname'),
				'lname'=>Input::get('lname'),
				'email'=>Input::get('email'),
				'phone'=>Input::get('phone')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function updateGroupValidation()
	{
		$role=array('name'=>'required');
		$this->validator=\Validator::make(array('name'=>Input::get('name')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function setServicePattern()
	{
		$services=TbAdminServices::all();
		$service=Input::get('service');
		$patt='';

		foreach ($services as $srv) 
		{
			if(isset($service[$srv->id])&&$service[$srv->id]=='on')
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminServicesACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
				{
					return $acc->id;
				}
					
			}
			return null;
		}
	}
	private function setMessengerPattern()
	{
		$messengers=TbAdminMessengers::all();
		$softwares=Input::get('software');
		$patt='';

		foreach ($messengers as $srv) 
		{
			if(isset($softwares[$srv->id])&&$softwares[$srv->id]=='on')
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminMessengersACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
				{
					return $acc->id;
				}
					
			}
			return null;
		}
	}

	private function activeServices($acc)
	{
		if($acc!=null)
		{
			$patt=DB::table('tbservicesacc')->where('id','=',$acc)->pluck('pattern');
			$patt=explode(',',$patt);
			//$srv=DB::table('tbservices')->get();

			$centerid=Session::get('centerid');
			$center=TbAdminCenters::find($centerid);
			$pattern=TbAdminServicesACC::where('id','=',$center->serviceACCid)->pluck('pattern');
			$services=TbAdminServices::all();
			//dd($pattern);
			if($pattern)
			{
				$pattid=explode(',',$pattern);
				$srv=$services->filter(function($item) use($pattid)
					{
						/*if(!$item->enable)
							return false;*/
						if(in_array($item->id,$pattid))
							return true;
					});
			}
			else
			{
				//$srv=null;
				return null;
			}
				

			$services=array();
			$i=0;
			foreach ($srv as $s) {
				$services[$i][0]=$s->id;
				$services[$i][1]=$s->name;
				if(in_array($s->id,$patt))
					$services[$i][2]=1;
				else
					$services[$i][2]=0;
				$i++;
			}
			return $services;
		}
		else
		{
			//$srv=DB::table('tbservices')->get();
			$centerid=Session::get('centerid');
			$center=TbAdminCenters::find($centerid);
			$pattern=TbAdminServicesACC::where('id','=',$center->serviceACCid)->pluck('pattern');
			$services=TbAdminServices::all();
			//dd($pattern);
			if($pattern)
			{
				$pattid=explode(',',$pattern);
				$srv=$services->filter(function($item) use($pattid)
					{
						/*if(!$item->enable)
							return false;*/
						if(in_array($item->id,$pattid))
							return true;
					});
			}
			else
			{
				//$srv=null;
				return null;
			}
				
			$services=array();
			$i=0;
			foreach ($srv as $s) {
				$services[$i][0]=$s->id;
				$services[$i][1]=$s->name;
				$services[$i][2]=0;
				$i++;
			}
			return $services;
			return null;
		}
		
	}
	private function activeSoftwares($acc)
	{
		if($acc!=null)
		{
			$patt=TbAdminMessengersACC::where('id','=',$acc)->pluck('pattern');
			$patt=explode(',',$patt);
			$srv=TbAdminMessengers::all();
			$services=array();
			$i=0;
			foreach ($srv as $s) {
				$services[$i][0]=$s->id;
				$services[$i][1]=$s->name;
				if(in_array($s->id,$patt))
					$services[$i][2]=1;
				else
					$services[$i][2]=0;
				$i++;
			}
			return $services;
		}
		else
		{
			$srv=TbAdminMessengers::all();
			$services=array();
			$i=0;
			foreach ($srv as $s) {
				$services[$i][0]=$s->id;
				$services[$i][1]=$s->name;
				$services[$i][2]=0;
				$i++;
			}
			return $services;
		}
		
	}
	public function phoneWith98($phone)
	{
		if(empty($phone))
			return "";
		return "98".substr($phone,strlen($phone)-10);
	}

}