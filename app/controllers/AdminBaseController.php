<?php

class AdminBaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->beforeFilter('authAdmin');
		$centername = DB::table('tbcenters')->where('id','=',Session::get('adminid'))->pluck('name');
        View::share('centername', $centername);
	}


	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
