@extends('userLayout')

@section('header')
  <h1> ROBOT </h1>

@stop

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم ربات <small>ایجاد ربات</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-bold"></i> ربات
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <hr></hr>
      @if(isset($successmsg))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$successmsg}}
        </div>
      @endif
      @if(isset($errormsg))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$errormsg}}
        </div>
      @endif
    {{Form::open(array('url'=>'/bot/storeTree','class'=>'form-horizontal well',  'method' => 'post'))}}
      <input type="hidden" name="botid" value="{{$bot->id}}"></input>
      <div class="row">
        <div class="col-sm-2">
          {{ Form::label('question[]', 'انتخاب سوال',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-7">
             <select class="form-control" id="question" name="question">
              <option value="{{0}}">سوالی را انتخاب کنید</option>
            @foreach($questions as $q)
              <option value="{{$q->id}}"> {{$q->text}} </option>
            @endforeach
          </select>
        </div>
      </div>
      <p></p>
      <div id="questiondiv">
      </div>
      {{------------------------------------------------------------------}}
        
      {{------------------------------------------------------------------}}
      <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-6">
        </div>
        <div class="col-sm-2">
          <a type='button' class="btn btn-success" id='addButton'>گزینه جدید</a>
        </div>
      </div>
      <p></p>
    <div class="row">
      <div class="col-sm-2"></div>
      <div class="col-sm-2">
         {{-- Form submit button. --------------------}}
         {{ Form::submit('ثبت (مرحله بعد)',array('class'=>'form-control btn btn-primary')) }}
      </div>
    </div>
  {{Form::close()}}

  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a target="_blank" href="{{url('/bot/'.$bot->id.'/show')}}" class="btn btn-success">مشاهده ساختار درختی ربات</a>
    <a href="{{url('/bot')}}" type='button' class="btn btn-success" >بازگشت به لیست ربات ها</a>
  </div>
</div>
<script type="text/javascript">
var allque={{json_encode($arrque)}};
var usedque=[];
//var usedsubque[];
var current=0;
$(document).ready(function(){
  $("#addButton").hide();
  $("#question").on("change",function()
  {
    counter=1;
    var val=$(this).val();
    if(current>0)
    {
      var xx=[];
      for(var j=0; j<usedque.length;j++)
        if(usedque[j]!=current)
        {
          xx.push(usedque[j]);
        }
      usedque=xx;

    }
    if(val<=0)
    {
      //alert(val);
      $('#questiondiv').empty();
      $("#addButton").hide(500);
      current=val;
      //return;
    }
    else
    {
      //alert(val);
      $('#questiondiv').empty();
      if(current<=0)
        $("#addButton").show(500);
      usedque.push(val);
      current=val;
    }
    
  });
    
    var counter = 1;
    $("#addButton").click(function () {
      
      var sel='<div class="row" style="background-color:rgba(0, 255, 76, 0.19);min-height: 50px;padding: 10px;"><div class="col-sm-1"><label for="option[]" class="control-label">گزینه '+counter+') </label></div><div class="col-sm-2"><input class="form-control" name="option[]" type="text" value="" id="option"></div><div class="col-sm-1"><label for="subquestion[]" class="control-label">سوال بعدی </label></div><div class="col-sm-4"><select class="form-control" id="subquestion-'+counter+'" name="subquestion[]"><option value="{{0}}">ندارد</option>';
      var select=document.createElement("SELECT");
      select.setAttribute("id", "subquestion-"+counter);
      select.setAttribute("name", "subquestion[]");
      //var selid="#subquestion-"+counter;     
      select.setAttribute("class","form-control");
      $("#subquestion1").on("change",function()
              {
                var value=$(this).val();
                alert(value);
              });
      for(var j=0; j<usedque.length;j++)
        console.log(usedque[j]);
      for(var i=0; i< allque.length ;i++)
      {
        var x= allque[i][0];
        var bool=false;
        for(var j=0; j<usedque.length;j++)
          if(usedque[j]==allque[i][0])
          {
            //console.log(j);
            bool=true;
          }
            
        if(!bool)
        {
          var id=allque[i][0];
          var q=allque[i][1];
          sel=sel+'<option value="'+id+'">'+q+'</option>';
        }
        
        //alert(sel);
      }
      sel+='</select></div>';
      sel+='<div class="col-sm-1"><label for="answer[]" class="control-label">جواب </label></div>';
      sel+='<div class="col-sm-3">'
      sel+='<input class="form-control" name="answer[]" type="text" placeholder="جواب در صورتی که گزینه سوال بعدی ندارد" id="answer-'+counter+'"></div>';
      sel+='</div><p></p>';
          $('#questiondiv').append(sel);       
          counter++;
  });
  
});
</script>


@stop