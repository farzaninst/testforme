﻿

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO migrations VALUES("2015_10_25_044719_all_table","1");
INSERT INTO migrations VALUES("2015_10_25_061319_dependency","1");
INSERT INTO migrations VALUES("2015_10_25_093948_user_table","1");
INSERT INTO migrations VALUES("2015_11_19_073332_BotMigrate","1");
INSERT INTO migrations VALUES("2015_11_30_062816_messageBank","2");
INSERT INTO migrations VALUES("2015_11_30_063423_messageBank_dependency","3");





CREATE TABLE `tbservicesacc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pattern` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbservicesacc VALUES("1","1");
INSERT INTO tbservicesacc VALUES("2","2");
INSERT INTO tbservicesacc VALUES("3","1,2");





CREATE TABLE `tbcity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbmessengers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pattern` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbmessengers VALUES("1","1");
INSERT INTO tbmessengers VALUES("2","2");
INSERT INTO tbmessengers VALUES("3","1,2");





CREATE TABLE `tbservices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbservices VALUES("1","فراما","1","");
INSERT INTO tbservices VALUES("2","فراسا","1","");





CREATE TABLE `tbcenters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_persian_ci NOT NULL,
  `username` varchar(250) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(500) COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_persian_ci DEFAULT NULL,
  `address` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `birthMessage` text COLLATE utf8_persian_ci,
  `marriageMessage` text COLLATE utf8_persian_ci,
  `nationalIdentity` varchar(10) COLLATE utf8_persian_ci DEFAULT NULL,
  `credit` int(11) NOT NULL,
  `bulk` tinyint(1) NOT NULL,
  `birthmsg` tinyint(1) NOT NULL,
  `marriagemsg` tinyint(1) NOT NULL,
  `scheduledmsg` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `matchservice` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `opinion` tinyint(1) NOT NULL,
  `serviceACCid` int(10) unsigned DEFAULT NULL,
  `bot` tinyint(1) NOT NULL DEFAULT '1',
  `logo` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `website` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `tik` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tbcenters_serviceaccid_foreign` (`serviceACCid`),
  CONSTRAINT `tbcenters_serviceaccid_foreign` FOREIGN KEY (`serviceACCid`) REFERENCES `tbservicesacc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbcenters VALUES("2","admin","admin","$2y$10$c1.vBLKbuoxP0ZMy7tQ96u8oj8R/3OQBHcOnV6ayl2DH3WW7BDy5u","admin@farzan.com","تهران، پردیس، پارک فناوری","989370078750","","","2855121415","0","1","1","1","1","1","1","1","1","3","1","/logo/1450166627_logo.gif","2015-11-30 05:43:44","2015-12-15 08:03:47","KwzQDGopYIBa7c9UmbsDN3iISVSR6dh0LF8Fupd903fimf2vVxzEJGfFTMgt","www.farzan123.com","0");
INSERT INTO tbcenters VALUES("3","test","center1","$2y$10$Q3GjpVxR69i/ycwfLJnAxOiz/XQDxJmtieWbQTYnqRE.dDiK0PboO","center1@gmail.com","","","تولد سنتر","","","0","0","0","0","0","1","0","0","0","3","1","","2015-11-30 05:43:44","2015-12-08 06:10:15","ShuYuEMkz56nB5fqKVU2xEq6YZ9RTVKIIz2jQ6EkpH4lKI914GkoYVAAIUaA","","0");
INSERT INTO tbcenters VALUES("4","test2","test2","$2y$10$eYsLQZNfBq6jz8gZp910Xuz.i.PmaQouEZYx7JrwuVM7RXN.M05QW","","","","تولد تست  #fname#  #lname#  #education# ","#gender#  #fname#  #lname#  ازدواجتان مبارک","","0","1","1","1","1","1","1","0","1","3","1","","2015-12-06 05:19:09","2015-12-07 06:16:30","meswDHz8k40J7uuxcn0ebPy7keqWX187OVVny7g1zktqZt9BQGnwAqYdNAFF","","0");
INSERT INTO tbcenters VALUES("5","test 3","test3","$2y$10$aMf5I4tomYL1DC4tAfM4D.qPyJ1xtBR/q5UJyHfHazX800jK5Wu.O","center3@gmail.com","","","","","","0","0","0","0","0","1","0","0","0","3","1","","2015-12-06 10:52:16","2015-12-06 10:52:41","","","0");
INSERT INTO tbcenters VALUES("6","فراسا","farzan123556","$2y$10$q0oJj/E2PlpNfNeWL.E00eu/fC9QKtuRQ0HWZHY/wDU.TjPuPJXsi","center1@gmail.com","","989370078750","","","3762348227","0","1","1","1","1","1","1","0","1","3","1","","2015-12-14 10:21:27","2015-12-14 10:23:34","","www.hamatest2.gigfa.com","0");





CREATE TABLE `tbmessagebank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `text` text COLLATE utf8_persian_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tbmessagebank_centerid_foreign` (`centerid`),
  CONSTRAINT `tbmessagebank_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbmessagebank VALUES("1",NULL,"msg","msg 1","2015-11-30 07:33:56","2015-11-30 07:33:56");
INSERT INTO tbmessagebank VALUES("3",NULL,"ddd"," #fname#  #lname#  #education# #gender#","2015-11-30 08:27:26","2015-11-30 08:27:26");
INSERT INTO tbmessagebank VALUES("4",NULL,"ddd"," #fname#  #lname#  #education# #gender#","2015-11-30 08:27:28","2015-11-30 08:27:28");
INSERT INTO tbmessagebank VALUES("5",NULL,"ddd"," #fname#  #lname#  #education# #gender#","2015-11-30 08:27:32","2015-11-30 08:27:32");
INSERT INTO tbmessagebank VALUES("6",NULL,"ddd"," #fname#  #lname#  #education# #gender#","2015-11-30 08:27:34","2015-11-30 08:27:34");
INSERT INTO tbmessagebank VALUES("9",NULL,"salam"," khobi","2015-12-01 05:38:16","2015-12-01 05:38:16");
INSERT INTO tbmessagebank VALUES("10",NULL,"salam"," khobi","2015-12-01 05:38:18","2015-12-01 05:38:18");
INSERT INTO tbmessagebank VALUES("11",NULL,"salam"," khobi","2015-12-01 05:38:20","2015-12-01 05:38:20");
INSERT INTO tbmessagebank VALUES("17","2","پیام خصوصی","پیام خصوصی","2015-12-01 08:07:31","2015-12-01 08:07:31");
INSERT INTO tbmessagebank VALUES("18","2","ddd","ddd","2015-12-01 09:35:28","2015-12-01 09:35:28");
INSERT INTO tbmessagebank VALUES("19","2","ddd","ddd","2015-12-01 09:35:30","2015-12-01 09:35:30");
INSERT INTO tbmessagebank VALUES("20","2","ddd","ddd","2015-12-01 09:35:32","2015-12-01 09:35:32");
INSERT INTO tbmessagebank VALUES("21","2","ddd","ddd","2015-12-01 09:35:35","2015-12-01 09:35:35");
INSERT INTO tbmessagebank VALUES("22","2","token","forget token","2015-12-06 09:23:12","2015-12-06 09:23:12");
INSERT INTO tbmessagebank VALUES("23","2","token","forget token","2015-12-06 09:23:15","2015-12-06 09:23:15");
INSERT INTO tbmessagebank VALUES("31","2","ddd","jkjkljiljljll","2015-12-06 10:04:01","2015-12-06 10:04:01");
INSERT INTO tbmessagebank VALUES("32","2","svfdg","dfgfdgfdgggggg","2015-12-06 10:35:59","2015-12-06 10:35:59");
INSERT INTO tbmessagebank VALUES("33","2","svfdg","dfgfdgfdgggggg","2015-12-06 10:36:18","2015-12-06 10:36:18");
INSERT INTO tbmessagebank VALUES("34","2","xcvxcvxcv","cvcvcbbbbb","2015-12-06 10:37:19","2015-12-06 10:37:19");
INSERT INTO tbmessagebank VALUES("35","2","xcvcvcb","bnvvnnnnn","2015-12-06 10:37:30","2015-12-06 10:37:30");
INSERT INTO tbmessagebank VALUES("36","2","xcvcvcb","bnvvnnnnn","2015-12-06 10:37:33","2015-12-06 10:37:33");
INSERT INTO tbmessagebank VALUES("38","2","پیام تست","پیام تست برای نمایش پیام موفقیت ایجاد پیام ","2015-12-07 05:35:29","2015-12-07 05:35:29");





CREATE TABLE `tbsubservices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `serviceid` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbsubservices_centerid_foreign` (`centerid`),
  KEY `tbsubservices_serviceid_foreign` (`serviceid`),
  CONSTRAINT `tbsubservices_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`),
  CONSTRAINT `tbsubservices_serviceid_foreign` FOREIGN KEY (`serviceid`) REFERENCES `tbservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbsoftwares` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbsoftwares VALUES("1","تلگرام","1","");
INSERT INTO tbsoftwares VALUES("2","واتس آپ","1","");





CREATE TABLE `tbnumbers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `json` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `number` varchar(15) COLLATE utf8_persian_ci NOT NULL,
  `softwareid` int(10) unsigned NOT NULL,
  `blocked` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbnumbers_softwareid_foreign` (`softwareid`),
  CONSTRAINT `tbnumbers_softwareid_foreign` FOREIGN KEY (`softwareid`) REFERENCES `tbsoftwares` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbnumbers VALUES("1","","17549008418","1","0");
INSERT INTO tbnumbers VALUES("3","","17549008418","1","0");
INSERT INTO tbnumbers VALUES("5","","989370078750","1","0");





CREATE TABLE `tbcentersnumbers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `numberid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbcentersnumbers_centerid_foreign` (`centerid`),
  KEY `tbcentersnumbers_numberid_foreign` (`numberid`),
  CONSTRAINT `tbcentersnumbers_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`),
  CONSTRAINT `tbcentersnumbers_numberid_foreign` FOREIGN KEY (`numberid`) REFERENCES `tbnumbers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbcentersnumbers VALUES("1","2","1");
INSERT INTO tbcentersnumbers VALUES("3","4","3");
INSERT INTO tbcentersnumbers VALUES("5","2","5");





CREATE TABLE `tbusers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `userServiceACCid` int(10) unsigned DEFAULT NULL,
  `messengerid` int(10) unsigned DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `fname` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `lname` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `education` varchar(200) COLLATE utf8_persian_ci DEFAULT NULL,
  `sex` tinyint(1) NOT NULL,
  `single` tinyint(1) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `birthdate` timestamp NULL DEFAULT NULL,
  `marriagedate` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbusers_centerid_foreign` (`centerid`),
  KEY `tbusers_messengerid_foreign` (`messengerid`),
  KEY `tbusers_userserviceaccid_foreign` (`userServiceACCid`),
  CONSTRAINT `tbusers_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`),
  CONSTRAINT `tbusers_messengerid_foreign` FOREIGN KEY (`messengerid`) REFERENCES `tbmessengers` (`id`),
  CONSTRAINT `tbusers_userserviceaccid_foreign` FOREIGN KEY (`userServiceACCid`) REFERENCES `tbservicesacc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbusers VALUES("1","2",NULL,"3","989370078750","محمد","یزدان پناه","yazdanpanah.mohamad@gmail.com","5","1","0","0","2015-12-06 00:00:00","","2015-11-29 11:33:13","2015-12-07 11:52:54","");
INSERT INTO tbusers VALUES("2","3",NULL,"3","09366727432","کاربر","فامیلی کاربر 1","","0","0","0","0","","","2015-12-01 09:48:05","2015-12-01 09:48:05","");
INSERT INTO tbusers VALUES("3","4",NULL,"3","989366727432","سعید ","سالارکیا","","6","1","0","0","2015-12-06 00:00:00","","2015-12-06 06:44:27","2015-12-07 06:11:44","2015-12-07 06:11:44");
INSERT INTO tbusers VALUES("4","4",NULL,"3","989370078750","محمد ","یزدان پناه","","2","0","0","0","2015-12-06 00:00:00","","2015-12-06 06:45:27","2015-12-07 06:11:42","2015-12-07 06:11:42");
INSERT INTO tbusers VALUES("5","2",NULL,"3","989370078750","محمد","یزدان پناه","center12@gmail.com","5","1","0","0","2015-12-06 00:00:00","","2015-12-06 07:08:20","2015-12-07 07:43:59","");
INSERT INTO tbusers VALUES("6","2",NULL,"1","989370078750","mohamad","yazdan panah","","0","1","0","0","","","2015-12-06 09:04:39","2015-12-07 11:52:47","");
INSERT INTO tbusers VALUES("7","2",NULL,"1","989370078750","mohamad","yazdan panah","","0","1","0","0","","","2015-12-06 09:06:21","2015-12-07 11:52:51","");
INSERT INTO tbusers VALUES("8","2",NULL,"1","989370078750","mohamad","yazdan panah","","0","1","0","0","","","2015-12-06 09:09:40","2015-12-07 11:52:58","");
INSERT INTO tbusers VALUES("9","2",NULL,"1","9370078750","mohamad","yazdan panah","","0","1","0","0","","","2015-12-06 09:11:49","2015-12-06 11:24:25","2015-12-06 11:24:25");
INSERT INTO tbusers VALUES("10","2",NULL,NULL,"989370078750","mohamad","yazdan panah","","1","0","0","0","","","2015-12-06 09:11:49","2015-12-07 11:53:01","");
INSERT INTO tbusers VALUES("11","2",NULL,"1","9370078750","mohamad","yazdan panah","","0","1","0","0","","","2015-12-06 09:12:02","2015-12-06 11:24:21","2015-12-06 11:24:21");
INSERT INTO tbusers VALUES("12","2",NULL,NULL,"9370078750","mohamad","yazdan panah","","1","0","0","0","","","2015-12-06 09:12:02","2015-12-06 11:24:18","2015-12-06 11:24:18");
INSERT INTO tbusers VALUES("13","2",NULL,"1","989370078750","mohamad","yazdan panah","","0","1","0","0","","","2015-12-06 09:12:11","2015-12-07 11:53:05","");
INSERT INTO tbusers VALUES("14","2",NULL,NULL,"9370078750","mohamad","yazdan panah","","1","0","0","0","","","2015-12-06 09:12:11","2015-12-06 11:24:12","2015-12-06 11:24:12");
INSERT INTO tbusers VALUES("15","2",NULL,"3","","سعید","اسکافی","saeid.s12@gmail.com","1","1","0","0","2011-12-06 00:00:00","2011-12-06 00:00:00","2015-12-06 10:27:08","2015-12-07 11:47:31","");
INSERT INTO tbusers VALUES("16","4",NULL,"3","09384006069","سعید","اسکافی","eskafi@farzaninstitute.com","6","1","0","0","2011-12-06 00:00:00","2011-12-06 00:00:00","2015-12-06 10:32:17","2015-12-07 06:11:40","2015-12-07 06:11:40");
INSERT INTO tbusers VALUES("17","4",NULL,"3","09124491801","کاربر","اول","dawenhp@yahoo.com","2","0","0","0","2011-12-06 00:00:00","2011-12-06 00:00:00","2015-12-06 11:28:50","2015-12-07 06:11:38","2015-12-07 06:11:38");
INSERT INTO tbusers VALUES("18","4",NULL,"3","09331544019","کاربر","اول","saf@f.com","3","0","0","0","2011-12-06 00:00:00","2011-12-06 00:00:00","2015-12-06 11:29:20","2015-12-07 06:11:33","2015-12-07 06:11:33");
INSERT INTO tbusers VALUES("19","4",NULL,"3","8615986757224","farzan","pc","","0","0","0","0","2015-12-07 00:00:00","2015-12-07 00:00:00","2015-12-07 06:14:50","2015-12-07 06:14:50","");
INSERT INTO tbusers VALUES("20","2",NULL,"3","989370078750","محمد","یزدان پناه","yazdanpanah.mohamad@gmail.com","0","0","1","0","2015-11-22 00:00:00","2015-12-17 00:00:00","2015-12-07 07:36:29","2015-12-07 07:36:29","");
INSERT INTO tbusers VALUES("21","2",NULL,"1","989370078750","test","testy","meysam@gmail.com","6","1","1","0","2015-12-07 00:00:00","2015-12-08 00:00:00","2015-12-07 07:37:57","2015-12-07 07:52:11","");
INSERT INTO tbusers VALUES("22","4",NULL,"3","989370078750","محمد ","یزدان پناه","","2","0","0","0","2015-12-08 00:00:00","2015-12-10 00:00:00","2015-12-07 11:31:26","2015-12-07 11:31:26","");
INSERT INTO tbusers VALUES("23","2",NULL,"3","989384006069","11111","1111","meysam@gmail.com","1","1","0","0","","","2015-12-07 11:42:11","2015-12-09 09:51:11","");
INSERT INTO tbusers VALUES("24","4",NULL,"3","989384006069","سعید","اسکافی","","1","0","0","0","2015-11-24 00:00:00","2015-12-16 00:00:00","2015-12-07 12:04:47","2015-12-08 06:17:38","2015-12-08 06:17:38");
INSERT INTO tbusers VALUES("25","4",NULL,"3","989366727432","فرهاد","طططط","","5","1","0","0","2015-12-11 00:00:00","2015-12-16 00:00:00","2015-12-07 12:05:28","2015-12-07 12:05:28","");





CREATE TABLE `tbsubservicesusers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `subserviceid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbsubservicesusers_userid_foreign` (`userid`),
  KEY `tbsubservicesusers_subserviceid_foreign` (`subserviceid`),
  CONSTRAINT `tbsubservicesusers_subserviceid_foreign` FOREIGN KEY (`subserviceid`) REFERENCES `tbsubservices` (`id`),
  CONSTRAINT `tbsubservicesusers_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `tbusers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `desc` text COLLATE utf8_persian_ci,
  PRIMARY KEY (`id`),
  KEY `tbgroups_centerid_foreign` (`centerid`),
  CONSTRAINT `tbgroups_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbgroups VALUES("1","2","کاربران","");
INSERT INTO tbgroups VALUES("2","2","برنامه نویسان","برنامه نویسان");





CREATE TABLE `tbusersgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbusersgroups_userid_foreign` (`userid`),
  KEY `tbusersgroups_groupid_foreign` (`groupid`),
  CONSTRAINT `tbusersgroups_groupid_foreign` FOREIGN KEY (`groupid`) REFERENCES `tbgroups` (`id`),
  CONSTRAINT `tbusersgroups_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `tbusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbusersgroups VALUES("1","8","1");
INSERT INTO tbusersgroups VALUES("2","10","1");
INSERT INTO tbusersgroups VALUES("3","13","1");
INSERT INTO tbusersgroups VALUES("4","15","1");
INSERT INTO tbusersgroups VALUES("5","20","1");
INSERT INTO tbusersgroups VALUES("6","21","1");
INSERT INTO tbusersgroups VALUES("7","6","2");
INSERT INTO tbusersgroups VALUES("8","15","2");
INSERT INTO tbusersgroups VALUES("9","20","2");





CREATE TABLE `tbarea` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cityid` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbarea_cityid_foreign` (`cityid`),
  CONSTRAINT `tbarea_cityid_foreign` FOREIGN KEY (`cityid`) REFERENCES `tbcity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbareanumbers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `areaid` int(10) unsigned DEFAULT NULL,
  `number` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbareanumbers_areaid_foreign` (`areaid`),
  CONSTRAINT `tbareanumbers_areaid_foreign` FOREIGN KEY (`areaid`) REFERENCES `tbarea` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbbot` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `centerid` int(10) unsigned NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `jsontree` text COLLATE utf8_persian_ci,
  `tree_completed` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbbot_centerid_foreign` (`centerid`),
  CONSTRAINT `tbbot_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbbot VALUES("15","test","4","1","{ \"class\": \"go.GraphLinksModel\",
  \"linkFromPortIdProperty\": \"fromPort\",
  \"linkToPortIdProperty\": \"toPort\",
  \"nodeDataArray\": [ 
{\"text\":\"دل درد\", \"key\":-1, \"loc\":\"-66 -162\"},
{\"text\":\"بله\", \"key\":-2, \"loc\":\"-165 -97\"},
{\"text\":\"خیر\", \"key\":-3, \"loc\":\"69 -77\"},
{\"text\":\"شما حالت خوبه\\nمرض خاصی نداری\", \"key\":-4, \"loc\":\"107 27\"},
{\"text\":\"چپ\", \"key\":-5, \"loc\":\"-298 27\"},
{\"text\":\"راست\", \"key\":-6, \"loc\":\"-77 36\"},
{\"text\":\"چپ برو دکتر\", \"key\":-7, \"loc\":\"-298 126\"},
{\"text\":\"راست ب رو دکتر\", \"key\":-8, \"loc\":\"-63 122\"},
{\"category\":\"Start\", \"text\":\"Start\", \"key\":-9, \"loc\":\"-114.39999389648438 -320.80000019073486\"}
 ],
  \"linkDataArray\": [ 
{\"from\":-2, \"to\":-6, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-165,-80.69999999999999,-165,-70.69999999999999,-165,-30.499999999999993,-77,-30.499999999999993,-77,9.700000000000003,-77,19.700000000000003]},
{\"from\":-2, \"to\":-5, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-165,-80.69999999999999,-165,-70.69999999999999,-165,-34.99999999999999,-298,-34.99999999999999,-298,0.6999999999999993,-298,10.7]},
{\"from\":-5, \"to\":-7, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-298,43.3,-298,53.3,-298,76.5,-298,76.5,-298,99.7,-298,109.7]},
{\"from\":-6, \"to\":-8, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-77,52.300000000000004,-77,62.300000000000004,-77,79,-63,79,-63,95.7,-63,105.7]},
{\"from\":-1, \"to\":-3, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-66,-145.70000000000002,-66,-135.70000000000002,-66,-119.5,69,-119.5,69,-103.3,69,-93.3]},
{\"from\":-1, \"to\":-2, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-66,-145.70000000000002,-66,-135.70000000000002,-66,-129.5,-165,-129.5,-165,-123.3,-165,-113.3]},
{\"from\":-3, \"to\":-4, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[69,-60.69999999999999,69,-50.69999999999999,69,-28.899999999999995,107,-28.899999999999995,107,-7.100000000000001,107,2.8999999999999986]},
{\"from\":-9, \"to\":-1, \"fromPort\":\"B\", \"toPort\":\"T\", \"points\":[-114.39999389648438,-295.59069786515346,-114.39999389648438,-285.59069786515346,-114.39999389648438,-236.94534893257674,-66,-236.94534893257674,-66,-188.3,-66,-178.3]}
 ]}","0","0000-00-00 00:00:00","0000-00-00 00:00:00","");





CREATE TABLE `tbbotquestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_persian_ci NOT NULL,
  `botid` int(10) unsigned NOT NULL,
  `root` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8_persian_ci,
  PRIMARY KEY (`id`),
  KEY `tbbotquestion_botid_foreign` (`botid`),
  CONSTRAINT `tbbotquestion_botid_foreign` FOREIGN KEY (`botid`) REFERENCES `tbbot` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbbotoption` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `questionid` int(10) unsigned NOT NULL,
  `answer` text COLLATE utf8_persian_ci,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbbotoption_questionid_foreign` (`questionid`),
  CONSTRAINT `tbbotoption_questionid_foreign` FOREIGN KEY (`questionid`) REFERENCES `tbbotquestion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbbotrelation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionid` int(10) unsigned NOT NULL,
  `optionid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbbotrelation_questionid_foreign` (`questionid`),
  KEY `tbbotrelation_optionid_foreign` (`optionid`),
  CONSTRAINT `tbbotrelation_optionid_foreign` FOREIGN KEY (`optionid`) REFERENCES `tbbotoption` (`id`),
  CONSTRAINT `tbbotrelation_questionid_foreign` FOREIGN KEY (`questionid`) REFERENCES `tbbotquestion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;






CREATE TABLE `tbbulk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `message` text COLLATE utf8_persian_ci NOT NULL,
  `failcount` int(11) DEFAULT NULL,
  `sendcount` int(11) DEFAULT NULL,
  `completed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbbulk_centerid_foreign` (`centerid`),
  CONSTRAINT `tbbulk_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbbulk VALUES("66","2","یب"," لالا","0","1","1");
INSERT INTO tbbulk VALUES("67","2","test","this is a test","0","1","1");
INSERT INTO tbbulk VALUES("68","4","bulk test"," msg test","","","0");
INSERT INTO tbbulk VALUES("69","4","bulk test"," msg test ","0","1","1");
INSERT INTO tbbulk VALUES("70","2","title"," salam chetori?","0","1","1");
INSERT INTO tbbulk VALUES("71","4","11111111111111111111"," 11111111111111111111","0","1","1");
INSERT INTO tbbulk VALUES("72","4","sdfsdfsd"," fsdfsdfsdfsdf","","","0");





CREATE TABLE `tbbulkqueue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bulkid` int(10) unsigned NOT NULL,
  `phone` varchar(15) COLLATE utf8_persian_ci NOT NULL,
  `send` tinyint(1) NOT NULL,
  `fail` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbbulkqueue_bulkid_foreign` (`bulkid`),
  CONSTRAINT `tbbulkqueue_bulkid_foreign` FOREIGN KEY (`bulkid`) REFERENCES `tbbulk` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbbulkqueue VALUES("97","66","989384006069","1","0");
INSERT INTO tbbulkqueue VALUES("98","67","989384006069","1","0");
INSERT INTO tbbulkqueue VALUES("99","69","989366727432","1","0");
INSERT INTO tbbulkqueue VALUES("100","70","989384006069","1","0");
INSERT INTO tbbulkqueue VALUES("101","71","989366727432","1","0");
INSERT INTO tbbulkqueue VALUES("102","72","989366727432","0","0");





CREATE TABLE `tbmatchoropinion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8_persian_ci NOT NULL,
  `answerRegex` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `startDate` timestamp NOT NULL,
  `endDate` timestamp NOT NULL,
  `desc` text COLLATE utf8_persian_ci NOT NULL,
  `matchOrOpinion` tinyint(1) NOT NULL,
  `centerid` int(10) unsigned NOT NULL,
  `serviceACCid` int(10) unsigned DEFAULT NULL,
  `messengerid` int(10) unsigned DEFAULT NULL,
  `finish` tinyint(1) NOT NULL,
  `answersJSON` text COLLATE utf8_persian_ci,
  `backMSG` text COLLATE utf8_persian_ci,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbmatchoropinion_centerid_foreign` (`centerid`),
  KEY `tbmatchoropinion_serviceaccid_foreign` (`serviceACCid`),
  KEY `tbmatchoropinion_messengerid_foreign` (`messengerid`),
  CONSTRAINT `tbmatchoropinion_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`),
  CONSTRAINT `tbmatchoropinion_messengerid_foreign` FOREIGN KEY (`messengerid`) REFERENCES `tbmessengers` (`id`),
  CONSTRAINT `tbmatchoropinion_serviceaccid_foreign` FOREIGN KEY (`serviceACCid`) REFERENCES `tbservicesacc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbmatchoropinion VALUES("25","نظرت چیه ؟","[0-1]","2015-12-09 00:00:00","2015-12-10 00:00:00","توضیح","1","4",NULL,"3","1","------","با تشکر از شرکت شما","نظر سنجی 1");
INSERT INTO tbmatchoropinion VALUES("26","کی میریه ایران یا ارژانتین ؟","[0-1]","2015-12-09 00:00:00","2015-12-10 00:00:00","توضیح مسابقه","0","4",NULL,"3","1","","مرسی امیدوارم ایران ببره","مسابقه 2");
INSERT INTO tbmatchoropinion VALUES("27","نظرت چیه ؟","[text]","2015-12-14 00:00:00","2015-12-15 00:00:00","alsk;fjlsdkafhsf","1","4",NULL,"3","0","گزینه 1---گزینه 2---گزینه 3","مرسی از شرکتت","سیبس");
INSERT INTO tbmatchoropinion VALUES("28","آیا دروغ خوب است؟","[0-1]","2015-12-15 00:00:00","2015-12-16 00:00:00","دروغ ","0","2",NULL,"3","1","---","با تشکر","مسابقه در مورد دروغ");





CREATE TABLE `tbmatchanswers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `matchid` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `answer` varchar(15) CHARACTER SET utf8 NOT NULL,
  `send` tinyint(1) NOT NULL,
  `fail` tinyint(1) NOT NULL,
  `softwareid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbmatchanswers_matchid_foreign` (`matchid`),
  KEY `tbmatchanswers_userid_foreign` (`userid`),
  KEY `tbmatchanswers_softwareid_foreign` (`softwareid`),
  CONSTRAINT `tbmatchanswers_matchid_foreign` FOREIGN KEY (`matchid`) REFERENCES `tbmatchoropinion` (`id`),
  CONSTRAINT `tbmatchanswers_softwareid_foreign` FOREIGN KEY (`softwareid`) REFERENCES `tbsoftwares` (`id`),
  CONSTRAINT `tbmatchanswers_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `tbusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbmatchanswers VALUES("81","25","19"," 5","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("82","25","22","-1","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("83","25","25","-1","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("84","26","19","-1","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("85","26","22"," 2","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("86","26","25","-1","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("87","27","19","-1","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("88","27","22","","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("89","27","25"," 1","1","0",NULL);
INSERT INTO tbmatchanswers VALUES("90","28","1","","0","0",NULL);
INSERT INTO tbmatchanswers VALUES("91","28","5","","0","0",NULL);
INSERT INTO tbmatchanswers VALUES("92","28","15","","0","0",NULL);
INSERT INTO tbmatchanswers VALUES("93","28","20","","0","0",NULL);
INSERT INTO tbmatchanswers VALUES("94","28","23","","0","0",NULL);





CREATE TABLE `tbmatchreport` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `matchid` int(10) unsigned NOT NULL,
  `serviceACCid` int(10) unsigned DEFAULT NULL,
  `answerJSON` text COLLATE utf8_persian_ci,
  `winnerJSON` text COLLATE utf8_persian_ci,
  `failcount` int(11) DEFAULT NULL,
  `sendcount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbmatchreport_matchid_foreign` (`matchid`),
  CONSTRAINT `tbmatchreport_matchid_foreign` FOREIGN KEY (`matchid`) REFERENCES `tbmatchoropinion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbmatchreport VALUES("1","28",NULL,"[]","[]","0","0");





CREATE TABLE `tbparametrizedmsg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `serviceACCid` int(10) unsigned DEFAULT NULL,
  `messengerid` int(10) unsigned DEFAULT NULL,
  `sendDate` timestamp NOT NULL,
  `msgText` text COLLATE utf8_persian_ci NOT NULL,
  `createDate` timestamp NOT NULL,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `mode` smallint(6) NOT NULL,
  `finish` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbparametrizedmsg_centerid_foreign` (`centerid`),
  KEY `tbparametrizedmsg_serviceaccid_foreign` (`serviceACCid`),
  KEY `tbparametrizedmsg_messengerid_foreign` (`messengerid`),
  CONSTRAINT `tbparametrizedmsg_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`),
  CONSTRAINT `tbparametrizedmsg_messengerid_foreign` FOREIGN KEY (`messengerid`) REFERENCES `tbmessengers` (`id`),
  CONSTRAINT `tbparametrizedmsg_serviceaccid_foreign` FOREIGN KEY (`serviceACCid`) REFERENCES `tbservicesacc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbparametrizedmsg VALUES("12","4","3","3","2015-12-09 00:00:00","سلام #gender# #fname#  #lname#  خوبی
مدرک  #education# ","2015-12-09 00:00:00","تست پارامتری 1","1","1");
INSERT INTO tbparametrizedmsg VALUES("13","4","3","3","2015-12-14 00:00:00","سلام  #fname#  #lname# #gender#","2015-12-14 00:00:00","تست پارامتری 1","1","1");
INSERT INTO tbparametrizedmsg VALUES("14","2","3","3","2015-12-14 00:00:00","param  #fname#  #lname#  #education# #gender#","2015-12-14 00:00:00","parametry","1","0");





CREATE TABLE `tbparametrizedrecievers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paramid` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned DEFAULT NULL,
  `send` tinyint(1) NOT NULL,
  `phone` varchar(15) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbparametrizedrecievers_paramid_foreign` (`paramid`),
  KEY `tbparametrizedrecievers_userid_foreign` (`userid`),
  CONSTRAINT `tbparametrizedrecievers_paramid_foreign` FOREIGN KEY (`paramid`) REFERENCES `tbparametrizedmsg` (`id`),
  CONSTRAINT `tbparametrizedrecievers_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `tbusers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

INSERT INTO tbparametrizedrecievers VALUES("69","12","19","1","8615986757224");
INSERT INTO tbparametrizedrecievers VALUES("70","12","22","1","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("71","12","25","1","989366727432");
INSERT INTO tbparametrizedrecievers VALUES("72","13","19","1","8615986757224");
INSERT INTO tbparametrizedrecievers VALUES("73","13","22","1","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("74","13","25","1","989366727432");
INSERT INTO tbparametrizedrecievers VALUES("75","14","1","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("76","14","5","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("77","14","6","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("78","14","7","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("79","14","8","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("80","14","10","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("81","14","13","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("82","14","15","0","");
INSERT INTO tbparametrizedrecievers VALUES("83","14","20","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("84","14","21","0","989370078750");
INSERT INTO tbparametrizedrecievers VALUES("85","14","23","0","989384006069");





CREATE TABLE `tbmessagearchive` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `centerid` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_persian_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tbmessagearchive_centerid_foreign` (`centerid`),
  CONSTRAINT `tbmessagearchive_centerid_foreign` FOREIGN KEY (`centerid`) REFERENCES `tbcenters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




