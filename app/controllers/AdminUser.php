<?php

class AdminUser extends \BaseController {

	protected $actionmsg='';
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('authAdmin');
	}
	public function index()
	{
		//
		//$users=new AdminUserModel;
		$data=AdminUserModel::all();
		return View::make('admin/users/all')->with('users',$data);
		//return View::make('admin/users/all')->with('users',$data);
		//return "salam ";
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('admin/users/create');
		//return View::make('admin.user.create');
	}


	
	public function store()
	{
		/*$user=new AdminUserModel;
		$user->username=Input::get('username');
		$user->email=Input::get('email');
		$user->password=Input::get('password');
		$user->status=1;
		$user->superAdmin=1;
		$user->fname=Input::get('fname');
		$user->lname=Input::get('lname');
		$user->save();
		dd($user);*/
		//
		//$data=\Input::all();
		$role=array('password'=>'required|confirmed');
		$validator=\Validator::make(array('password'=>Input::get('password'),
									'password_confirmation'=>Input::get('password_confirmation')),$role);
		if($validator->passes())
		{
			$data['username']=Input::get('username');
			$data['email']=Input::get('email');
			$data['password']=Input::get('password');
			$data['status']=1;
			$data['superAdmin']=5;
			$data['fname']=Input::get('fname');
			$data['lname']=Input::get('lname');
			$users=new AdminUserModel;
			//dd($users);
			$add=$users->add($data);
			if($add)
			{
				$users=AdminUserModel::all();
				//return 'ok!!!';
				return View::make('admin/users/all')
							->nest('createSuccess','admin/users/createSuccess',['newuser'=>Input::all()])
							->with('users',$users);
			}
			else{
				//return "no 1";
			return View::make('admin/users/create')
						->with('newuser',Input::all())
						->with('Err','yes')
						->with('messages',$users->getMessages()->getMessages());
			}
		}else{
			//return "no 2";
			return View::make('admin/users/create')
					->with('newuser',Input::all())
					->with('Err','yes')
					->with('messages',$validator->messages()->getMessages());
		}
	}


	
	public function show($id)
	{
		$users=new AdminUserModel;
		$data=null;
		$validator=\Validator::make(['id'=>$id],['id'=>'integer']);
		if($validator)
		{
			$data=$users::find($id);
		}	
		return View::make('admin/users/show')
							->with('user',$val=($data) ?:'not found');
		//return View::make('admin.user.all')->with('users',$data);	
	}


	
	public function edit($id)
	{
		$data=null;
		$users=new AdminUserModel;
		$validator=\Validator::make(['id'=>$id],['id'=>'integer']);
		if($validator)
		{
			$data=$users::find($id);
		}	
		return View::make('admin/users/edit')
							->with('id',$id)
							->with('newuser',$data);
		//return View::make('admin.user.all')->with('users',$data);	
	}


	
	public function update($id)
	{
		$users=new AdminUserModel;
		$data=null;
		$validator=\Validator::make(['id'=>$id],['id'=>'integer']);
		if($validator)
		{
			$data=$users::find($id);
		}
		if(!empty(Input::get('password')) && Input::get('password')!=Input::get('password_confirmation'))
		{
			//dd(Input::all());
			return View::make('admin/users/edit')
						->with('newuser',$data)
						->with('id',$id)
						->with('passerr','دو رمز عبور با هم مطابقت ندارند');
		}
		if($this->updateUser(Input::only('fname','lname','password'),$id))
		{
			$data=AdminUserModel::all();
			return View::make('admin/users/all')->with('users',$data)->with('actionmsg','ویرایش کاربر با موفقیت انجام شد');
		}
		else{
			return View::make('admin/users/edit')
						->with('newuser',$data)
						->with('id',$id)
						->with('actionmsg',$this->actionmsg);
		}
	}


/*	
	public function destroy($id)
	{
		$user;
		$result=null;
		$validator=\Validator::make(['id'=>$id],['id'=>'integer']);
		$users=new Users;
		if($validator)
		{
			$user=Users::find($id);
			//var_dump($user);
			//var_dump($this->superAdmin($user));
			if($user)
			{
				$data=$users::all();
				if($this->superAdmin($user))
					return View::make('admin/users/all')->with('users',$data)
							->nest('deleteview','admin/users/delete',
								['deleteduser'=>$user,
								'superadmin'=>'you can not delete Super Admin']);
				$result=Users::destroy($id);
			}
		}
		$data=$users::all();
		return View::make('admin/users/all')->with('users',$data)
							->nest('deleteview','admin/users/delete',
								['deleteduser'=>$user,
								'result'=>$val=($result) ?:'not found']);
		//return View::make('admin/users/all')->with('users',$data);	
		
	}

	protected function superAdmin($user)
	{
		if($user->user_groups_id==1)
			return true;
		return false;
	}
	

*/


	public function updateUser($usr,$id)
	{
		$user=AdminUserModel::find($id);
		if($this->updateValidation($usr)){
			if($user)
			{
				$user->lname=$usr['lname'];
				$user->fname=$usr['fname'];
				if(!empty($this->password)){
					$user->password=Hash::make($this->password);
				}
				$user->save();
				return true;
			}
			$this->actionmsg="کاربر پیدا نشد";
			return false;
		}
		else{
			$this->actionmsg="فیلدهای نام و نام کاربری نباید خالی باشند";
			return false;
		}
		
			
	}
	public function updateValidation($data){
		//roles for validation
		$superAdmin=1;
		$roles=array(
					'fname'=>'required',
					'lname'=>'required'
					);
		$validator=\Validator::make($data,$roles);
		if($validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}			
	}
}
