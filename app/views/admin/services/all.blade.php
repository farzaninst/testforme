@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت سرویس ها
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست سرویس ها
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>نام سرویس</th>
                    <th >ویرایش</th>
                </tr>
            </thead>
 
            <tbody>
                <?php $i=1; ?>
                @foreach ($services as $ctr)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{ $ctr->name }}</td>
                    <td>
                        <a href="{{url('admin/services/'.$ctr->id.'/edit')}}" class="btn btn-info pull-left hvr-radial-out-info " style="margin-right: 3px;">ویرایش</a>
                        {{ Form::open(['url' => '/admin/services/delete/' . $ctr->id, 'method' => 'POST']) }}
                        {{-- Form::submit('حذف', ['class' => 'btn btn-danger pull-right'])--}}
                        {{ Form::close() }}
                    </td>
                </tr>
                <?php $i++; ?>
                @endforeach
            </tbody>
 
        </table>
    </div>
 
    <a href="{{url('admin/services/create')}}" class="btn btn-success hvr-radial-out-success">سرویس جدید</a>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop