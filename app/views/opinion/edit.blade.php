@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    {{Form::open(array('url' => 'opinions/view','class'=>'form-horizontal well'))}}
      <div class="form-group">
        <div class="col-lg-3">
          <label class="control-label col-sm-5" for=""> نام نظرسنجی : </label>
        </div>
        <div class="col-lg-3">
          <select class="form-control" id="selectOpinion" name="selectOpinion">
            <option>لطفا نظر سنجی مورد نظر را انتخاب کنید ...</option>
            @foreach ($id as $opinionid)
              <option value="{{$opinionid}}">{{$title[$opinionid]}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-lg-6"></div>
      </div>

      <div class="form-group">
        <div class="col-lg-7">
        </div>
        <div class="col-lg-3">
          <button type="submit" name ="submit" class="btn btn-primary">ویرایش</button>
        </div>
       </div>
      {{Form::close()}}

      
      {{Form::open(array('url' => '/opinions/store','class'=>'form-horizontal well'))}}
    
        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label" for=""> انتخاب سرویس ها : </label>
          </div>
        
          <div class="col-lg-3">
            <?php
              $services = str_split(str_replace(',','', Session::get('servicepattern')));
            ?>
          
            @foreach ($services as $service)
              <lable> {{$servicename[$service]}} <input tabindex="1" type="checkbox" name="service[]" id="{{$service}}" value="{{$service}}" checked="true"></lable>
            @endforeach
          </div>
        </div>    

        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label" for=""> ارسال به کاربران :  </label>
          </div>
          <div class="col-lg-3">
            @for ($i = 1; $i <= count($software); $i++)
              <lable> {{$software[$i]}} <input tabindex="1" type="checkbox" name="software[]" id="{{$i}}" value="{{$i}}" checked="true"></lable>
            @endfor
          </div>
        </div>

        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label" for=""> عنوان نظرسنجی : </label>
          </div>
          <div class="col-lg-3">
            <input type="text" class="form-control" id="opinion_title" name="opinion_title" placeholder="عنوان نظرسنجی را وارد کنید ...">
          </div>
          <div class="col-lg-7">
            @if ($errors->has('opinion_title')) <p class="alert alert-danger">{{ $errors->first('opinion_title') }}</p> @endif
          </div>
        </div>

        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label" for="">توضیحات :</label>
          </div>
          <div class="col-lg-3">
            <textarea rows="3" class="form-control" id="opinion_desc" name="opinion_desc" placeholder="توضیحات نظرسنجی را وارد کنید"></textarea>
          </div>
          <div class="col-lg-7">
            @if ($errors->has('opinion_desc')) <p class="alert alert-danger">{{ $errors->first('opinion_desc') }}</p> @endif
          </div>
        </div>

        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label " for="">پرسش : </label>
          </div>
          <div class="col-lg-3">
            <input type="text" class="form-control" id="opinion_que" name="opinion_que" placeholder="پرسش را وارد کنید ...">
          </div>
          <div class="col-lg-7">
            @if ($errors->has('opinion_que')) <p class="alert alert-danger">{{ $errors->first('opinion_que') }}</p> @endif
          </div>
        </div>

        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label" for="">پاسخ : </label>
          </div>
          <div class="radio col-lg-3" >
            <label> بلی - خیر<input type="radio" name = "radioans" value = "[0-1]" checked ="true"> </label><br>
            <label>بازه صفر تا ده<input type="radio" name = "radioans" value = "[1-10]">  </label>
          </div>
          <div class="col-lg-7"></div>
        </div>

        <div class="form-group">
          <div class="col-lg-2">
            <label class="control-label " for="">  تاریخ : </label>
          </div>
          <div class="col-lg-3">
            <div class="input-group input-daterange">
              <input type="text" class="pdate" id="opinion_start" name="opinion_start" value="">
              <input type="hidden" id="extraStart" name="extraStart" class="pdate wide">
              <span class="input-group-addon">تا</span>
              <input type="text" class="pdate" id="opinion_end" name="opinion_end" value="">
              <input type="hidden" id="extraEnd" name="extraEnd" class="pdate wide">
            </div>
          </div>
          <div class="col-lg-7"></div>
        </div>

        <div class="form-group">        
          <div class="col-sm-7"></div>
          <div class="col-sm-3">
            <button type="submit" name ="submit" class="btn btn-primary">ثبت اطلاعات</button>
          </div>
        </div>

    {{Form::close()}}
  </div>
</div>
@stop