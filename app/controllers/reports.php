<?php

class reports extends \BaseController {

	private $pageNum = 3;

	public function main()
	{
		return View::make('reports.main');
	}

	public function bulk()
	{
		//Paginator::setPageName('main_page');
		$bulks = TBbulk::
					where ('centerid','=',Session::get('centerid'))->
					paginate($this->pageNum);
		$data = array(
    		'bulks' => $bulks
		);
		return View::make('reports.bulk')->with($data);
	}

	public function opinion()
	{
		$opinions = TBopinion::
			where ('centerid','=',Session::get('centerid'))->
			where ('matchOrOpinion','=',1)->
			paginate($this->pageNum);
		$data = array(
    		'opinions' => $opinions
		);
		return View::make('reports.opinion')->with($data);
	}

	public function match()
	{	
		$matches = TBopinion::
			where ('centerid','=',Session::get('centerid'))->
			where ('matchOrOpinion','=', 0)->
			paginate($this->pageNum);
		$data = array(
    		'matches' => $matches
		);
		return View::make('reports.match')->with($data);
	}

	public function parametrize()
	{
		$parametrize = TbParametrizedMSG::
			where ('centerid','=',Session::get('centerid'))->
			paginate(3);
		$data = array(
    		'parametrize' => $parametrize
		);
		return View::make('reports.parametrize')->with($data);
	}

	public function bulkreport()
	{	
		Paginator::setPageName('report_page');
		$bulkid = Input::get('submit');
		$bulklog = TBbulk::
			where ('id','=', $bulkid )->get();
		$bulkqueue = TBbulkQueue::
			where ('bulkid','=', $bulkid )->
			paginate($this->pageNum);
		if ($bulklog) {
			$bulklog = $bulklog->toArray();
		}
		$data = array(
    		'bulklog' => $bulklog,
    		'bulkqueue' => $bulkqueue
		);
		return View::make('reports.bulkReport')->with($data);
	}

	public function opinionreport()
	{	
		$opinionid = Input::get('submit');

		$software = TBsoftwares::get();
		$opinionAns = TBopinionans::
			where('matchid','=', $opinionid )->
			get();
		$opinion = TBopinion::
			where('id','=', $opinionid)->
			where('matchOrOpinion','=',1)->
			get();
		$users = TbCentersUsers::
			where('centerid','=',Session::get('centerid'))->
			get();
		$answerRegex = TBopinion::
			where('id','=',$opinionid)->
			lists('answerRegex');
		if ($opinionAns) {
			$opinionAns = $opinionAns->toArray();
		}
		if ($opinion) {
			$opinion = $opinion->toArray();
		}
		if ($users) {
			$users = $users->toArray();
		}
		if ($software) {
			$software = $software->toArray();
		}
		
		$data = array(
    		'opinionans' => $opinionAns,
    		'opinion' => $opinion,
    		'software' => $software,
    		'answerRegex' => $answerRegex,
    		'users' => $users
		);
		return View::make('reports.opinionReport')->with($data);
	}

	public function matchreport()
	{	
		$matchid = Input::get('submit');

		$software = TBsoftwares::
			lists('name','id');
		$answerRegex = TBopinion::
			where('id','=',$matchid)->
			lists('answerRegex');
		$finish = TBopinion::
			where ('id','=', $matchid )->
			pluck('finish');
		$users = TbCentersUsers::
			where('centerid','=',Session::get('centerid'))->
			get();
		if ($users) {
			$users = $users->toArray();
		}
		if (!$finish) {
			$matchAns = TBopinionans::
				where ('matchid','=', $matchid )->
				get();
			if ($matchAns) {
				$matchAns = $matchAns->toArray();
			}
			$data = array(
    			'matchans' => $matchAns,
    			'software' => $software,
    			'answerRegex' => $answerRegex,
    			'users' => $users
			);
			return View::make('reports.matchReport')->with($data);
		} else {
			$matchRep = TBmatchreport::
				where ('matchid','=', $matchid )->
				get();
			if ($matchRep) {
				$matchRep = $matchRep->toArray();
			}
			$data = array(
    			'matchrep' => $matchRep,
    			'software' => $software,
    			'answerRegex' => $answerRegex,
    			'users' => $users
			);
			return View::make('reports.matchReport')->with($data);
		}
	}

	public function parametrizereport()
	{
		$paramid = Input::get('submit');

		$software = TBsoftwares::get();
		$paramlog = TbParametrizedRecievers::
			where('paramid','=', $paramid)->
			get();
		$users = TbCentersUsers::
			where('centerid','=',Session::get('centerid'))->
			get();
		$parametrize = TbParametrizedMSG::
			where ('centerid','=',Session::get('centerid'))->
			where ('id','=',$paramid)->
			get();
		if ($paramlog) {
			$paramlog = $paramlog->toArray();
		}
		if ($users) {
			$users = $users->toArray();
		}
		if ($parametrize) {
			$parametrize = $parametrize->toArray();
		}
		if ($software) {
			$software = $software->toArray();
		}
		$data = array(
    		'paramlog' => $paramlog,
    		'users'=> $users,
    		'software' => $software,
    		'parametrize'=> $parametrize
		);
		return View::make('reports.parametrizeReport')->with($data);
	}

	public function bulksearch() {
		Paginator::setPageName('search_page');
		$title = Input::get('search-title');
		$msg = Input::get('search-message');
		$complete = Input::get('submit');

		if ($complete == 'search0' || $complete == 'search1') {
			switch ($complete) {
				case 'search0':
					$complete = 0;
					$completemsg = 'نا تمام' ;
					break;
				case 'search1':
					$complete = 1;
					$completemsg = 'اتمام شده' ;
					break;
			}
			$bulksearch = TBbulk::
				where ('centerid','=',Session::get('centerid'))->
				where ('completed','=',$complete)->
				paginate($this->pageNum);
			$data = array(
				'bulksearch' => $bulksearch,
				'complete' => $completemsg
				);
			return View::make('reports.bulkSearch')->with($data);
		}else {
			$bulksearch = TBbulk::
				where ('centerid','=',Session::get('centerid'))->
				where ('title','like',"%".$title."%")->
				where ('message','like',"%".$msg."%")->
				paginate($this->pageNum);

			$data = array(
				'bulksearch' => $bulksearch,
				'title' => $title,
				'msg' => $msg
				);
			return View::make('reports.bulkSearch')->with($data);	
		}
		//$view = View::make('reports.bulkSearch')->with('bulksearch', $bulksearch);
		//$cookie = Cookie::make('search','asdsad', 30);
		//return Response::make($view)->withCookie($cookie);
	}

	public function opinionsearch() {
		Paginator::setPageName('search_page');
		$title = Input::get('search-title');
		$desc = Input::get('search-desc');
		$que = Input::get('search-que');
		$start = Input::get('extraStart');
		$end = Input::get('extraEnd');
		$finish = Input::get('submit');
		$finishmsg = 0;
		if ($finish == 'search0' || $finish == 'search1') {
			switch ($finish) {
				case 'search0':
					$finish = 0;
					$finishmsg = 'در حال برگزاری' ;
					break;
				case 'search1':
					$finish = 1;
					$finishmsg = 'اتمام شده' ;
					break;
			}
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('finish','=' , $finish)->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'finish' => $finishmsg
				);
			return View::make('reports.opinionSearch')->with($data);
		} elseif ($start)  {
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('startDate','like' , $start)->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'startDate' => $start
				);
			return View::make('reports.opinionSearch')->with($data);
		} elseif ($end){
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('endDate','like' , $end)->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'title' => $title,
				'desc' => $desc,
				'question' => $que,
				'startDate' => $start,
				'endDate' => $end,
				);
			return View::make('reports.opinionSearch')->with($data);
		} else {
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('title','like' , "%".$title."%")->
				where ('desc','like' , "%".$desc."%")->
				where ('question','like' , "%".$que."%")->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'title' => $title,
				'desc' => $desc,
				'question' => $que,
				'startDate' => $start,
				'endDate' => $end,
				);
			return View::make('reports.opinionSearch')->with($data);
		}
		//$view = View::make('reports.bulkSearch')->with('bulksearch', $bulksearch);
		//$cookie = Cookie::make('search','asdsad', 30);
		//return Response::make($view)->withCookie($cookie);
	}

	public function matchsearch() {
		Paginator::setPageName('search_page');
		$title = Input::get('search-title');
		$desc = Input::get('search-desc');
		$que = Input::get('search-que');
		$start = Input::get('extraStart');
		$end = Input::get('extraEnd');
		$finish = Input::get('submit');
		$finishmsg = 0;
		if ($finish == 'search0' || $finish == 'search1') {
			switch ($finish) {
				case 'search0':
					$finish = 0;
					$finishmsg = 'در حال برگزاری' ;
					break;
				case 'search1':
					$finish = 1;
					$finishmsg = 'اتمام شده' ;
					break;
			}
			$matchsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 0)->
				where ('finish','=' , $finish)->
				paginate($this->pageNum);
			$data = array(
				'matchsearch' => $matchsearch,
				'finish' => $finishmsg
				);
			return View::make('reports.matchSearch')->with($data);
		} elseif ($start)  {
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('startDate','like' , $start)->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'startDate' => $start
				);
			return View::make('reports.opinionSearch')->with($data);
		} elseif ($end){
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('endDate','like' , $end)->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'title' => $title,
				'desc' => $desc,
				'question' => $que,
				'startDate' => $start,
				'endDate' => $end,
				);
			return View::make('reports.opinionSearch')->with($data);
		} else {
			$opinionsearch = TBopinion::
				where ('centerid','=' , Session::get('centerid'))->
				where ('matchOrOpinion','=' , 1)->
				where ('title','like' , "%".$title."%")->
				where ('desc','like' , "%".$desc."%")->
				where ('question','like' , "%".$que."%")->
				paginate($this->pageNum);
			$data = array(
				'opinionsearch' => $opinionsearch,
				'title' => $title,
				'desc' => $desc,
				'question' => $que,
				'startDate' => $start,
				'endDate' => $end,
				);
			return View::make('reports.opinionSearch')->with($data);
		}
		//$view = View::make('reports.bulkSearch')->with('bulksearch', $bulksearch);
		//$cookie = Cookie::make('search','asdsad', 30);
		//return Response::make($view)->withCookie($cookie);
	}

	public function parametrizesearch() {
		Paginator::setPageName('search_page');
		$title = Input::get('search-title');
		$msg = Input::get('search-message');
		
		$finish_mode = Input::get('submit');
		$finishmsg = 0;
		if ($finish_mode == 'search0' || $finish_mode == 'search01') {
			switch ($finish_mode) {
				case 'search0':
					$finish_mode = 0;
					$finishmsg = 'در حال برگزاری' ;
					break;
				case 'search01':
					$finish_mode = 1;
					$finishmsg = 'اتمام شده' ;
					break;
			}
			$parsearch = TbParametrizedMSG::
				where ('centerid','=' , Session::get('centerid'))->
				where ('finish','=' , $finish_mode)->
				paginate($this->pageNum);
			$data = array(
				'parsearch' => $parsearch,
				'finish' => $finishmsg
				);
			return View::make('reports.parametrizeSearch')->with($data);
		}elseif($finish_mode == 'search1' || $finish_mode == 'search2' || $finish_mode == 'search3' || $finish_mode == 'search4') {
			switch ($finish_mode) {
				case 'search1':
					$finish_mode = 1;
					$finishmsg = 'یکبار' ;
					break;
				case 'search2':
					$finish_mode = 2;
					$finishmsg = 'هفتگی' ;
					break;
				case 'search3':
					$finish_mode = 3;
					$finishmsg = 'ماهانه' ;
					break;
				case 'search4':
					$finish_mode = 4;
					$finishmsg = 'سالانه' ;
					break;
			}
			$parsearch = TbParametrizedMSG::
				where ('centerid','=' , Session::get('centerid'))->
				where ('mode','=' , $finish_mode)->
				paginate($this->pageNum);
			$data = array(
				'parsearch' => $parsearch,
				'finish' => $finishmsg
				);
			return View::make('reports.parametrizeSearch')->with($data);
		} else {
			$parsearch = TbParametrizedMSG::
				where ('centerid','=' , Session::get('centerid'))->
				where ('title','like' , "%".$title."%")->
				where ('msgText','like' , "%".$msg."%")->
				paginate($this->pageNum);
			$data = array(
				'parsearch' => $parsearch,
				'title' => $title,
				'msg' => $msg
				);
			return View::make('reports.parametrizeSearch')->with($data);
		}
		//$view = View::make('reports.bulkSearch')->with('bulksearch', $bulksearch);
		//$cookie = Cookie::make('search','asdsad', 30);
		//return Response::make($view)->withCookie($cookie);
	}
}
