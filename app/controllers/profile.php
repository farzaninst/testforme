<?php

class profile extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $validator;
	public function index()
	{
		if(Session::has('successmsg'))
			$successmsg=Session::pull('successmsg');
		else
			$successmsg=null;
		$center=TbAdminCenters::find(Session::get('centerid'));
		if(!$center)
			return "bad request";
		return View::make('profile.all')->with('center',$center)->with('successmsg',$successmsg);
	}
	public function edit($id)
	{
		/*if(Session::has('successmsg'))
			$successmsg=Session::pull('successmsg');
		else*/
			$successmsg=null;
		if(Session::has('errormsg'))
			$errormsg=Session::pull('errormsg');
		else
			$errormsg=null;
		$center=TbAdminCenters::find(Session::get('centerid'));
		if(!$center)
			return "bad request";
		return View::make('profile.edit')->with('center',$center)->with('successmsg',$successmsg)->with('errormsg',$errormsg);
	}
	public function update($id)
	{
		$center=TbAdminCenters::find(Input::get('id'));
		if(!$center)
		{
			return "Bad request";
		}
		$validate=$this->updateValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			if($center){
				$center->name=Input::get('name');
				$center->email=Input::get('email');
				$center->nationalIdentity=Input::get('nationalIdentity');
				$center->phone=$this->phoneWith98(Input::get('phone'));
				$center->address=Input::get('address');
				$center->website=Input::get('website');
				$center->tik=Input::get('tik');
				//$center->birthMessage=Input::get('birthMessage');
				//$center->marriageMessage=Input::get('marriageMessage');
				$center->save();
				return Redirect::to('profile')->with('successmsg','ویرایش با موفقیت انجام شد');
				//return View::make('admin/centers/all')
						//	->with('centers',$ctr);
			}
			else
			{
				return Redirect::to('login');
			}
		}//end if validate
		else
		{
			return View::make('profile/edit')			
					->with('center',$center)
					->with('name',Input::get('name'))
					->with('username',Input::get('username'))
					->with('email',Input::get('email'))
					->with('address',Input::get('address'))
					->with('nationalIdentity',Input::get('nationalIdentity'))
					->with('phone',Input::get('phone'))
					->with('website',Input::get('website'))
					->with('tik',Input::get('tik'))
					->with( 'messages',$messages->getMessages());

		}
	}
	public function storePassword()
	{
		$role=array('password'=>'required|min:5|confirmed',
					'password_confirmation' => 'required|min:5');
		$this->validator=\Validator::make(
			array('password'=>Input::get('password'),
				'password_confirmation'=>Input::get('password_confirmation')),$role);
		$messages = $this->validator->messages();
		if($this->validator->passes())
		{
			$center=TbAdminCenters::find(Session::get('centerid'));
			$center->password=Hash::make(Input::get('password'));
			$center->save();
			return Redirect::to('profile')->with('successmsg','رمز عبور با موفقیت تغییر یافت');
			//return View::make('profile.all')->with('successmsg','رمز عبور با موفقیت تغییر یافت');
		}
		else
		{
			return View::make('profile.edit')->with('messages',$messages->getMessages());
		}

	}
	public function storeLogo()
	{
		$center=TbAdminCenters::find(Session::get('centerid'));
		if(!$center)
		{
			return Redirect::to('login');
		}
		if(!Input::hasFile('file'))
			return Redirect::to('profile/'.$center->id.'/edit')->with('errormsg','لطفا عکس را انتخاب کنید');
		$file=Input::file('file');
		//dd($file->getMimeType());
		//dd(Input::file('file')->getSize());
		if(!($file->getMimeType()=='image/jpeg' || $file->getMimeType()=='image/gif'))
			return Redirect::to('profile/'.$center->id.'/edit')->with('errormsg','فایل ورودی باید یک فایل تصویری با فرمت jpg باشد');
		if($file->getSize()>204800)
			return Redirect::to('profile/'.$center->id.'/edit')->with('errormsg','حجم فایل باید کمتر از 200 کیلوبایت باشد');
		$name=time().'_'.$file->getClientOriginalName();

		$file->move(public_path().'/logo',$name);
		if(file_exists ($center->logo))
			unlink($center->logo);
		$center->logo='/logo/'.$name;
		$center->save();
		return Redirect::to('profile')->with('successmsg','لوگو با موفقیت بارگزاری شد');
	}
	public function phoneWith98($phone)
	{
		if(empty($phone))
			return "";
		return "98".substr($phone,strlen($phone)-10);
	}
	private function updateValidation()
	{
		$role=array('name'=>'required',
					'email'=>'email',
					'phone'=>'phone:IR,mobile');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name'),
				'email'=>Input::get('email'),
				'phone'=>Input::get('phone')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
