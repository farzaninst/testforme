<?php

class AdminCenters extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $validator;
	private $paginate=10;
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('authAdmin');
	}
	public function index()
	{
		//$data=TbAdminCenters::paginate($this->paginate);
		$data=TbAdminCenters::all();
		return View::make('admin/centers/all')->with('centers',$data);
	}
	public function access($id)
	{
		$center=TbAdminCenters::find($id);
		if($center)
		{
			Session::put('centerid',$id);
			Session::put('centername', $center->name);
			//return View::make('panel');
			return Redirect::to('/panel');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$services=DB::table('tbservices')->get();
		return View::make('admin/centers/create')->with('services',$services);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//var_dump(Input::all());
		$validate=$this->storeValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$center=new TbAdminCenters;
			$center->name=Input::get('name');
			$center->username=Input::get('username');
			$center->email=Input::get('email');
			$center->nationalIdentity=Input::get('nationalIdentity');
			// phone should be begin with 98
			$center->phone=$this->phoneWith98(Input::get('phone'));
			$center->address=Input::get('address');
			$center->birthMessage=Input::get('birthMessage');
			$center->marriageMessage=Input::get('marriageMessage');
			$center->password=Hash::make(Input::get('password'));
			$center->bulk=Input::get('bulk');
			$center->birthmsg=Input::get('birthmsg');
			$center->marriagemsg=Input::get('marriagemsg');
			$center->scheduledmsg=Input::get('schedulemsg');
			$center->enabled=Input::get('enable');
			$center->matchservice=Input::get('matchservice');
			$center->opinion=Input::get('opinion');
			$center->serviceACCid=$this->setServicePattern();
			$center->website=Input::get('website');
			$center->save();
			$ctr=TbAdminCenters::all();
			return Redirect::to('/admin/centers');
			//return View::make('admin/centers/all')
				//		->with('centers',$ctr);
		}
		else
		{
			$services=DB::table('tbservices')->get();
			return View::make('admin/centers/create')
						->with('name',Input::get('name'))
						->with('services',$services)
						->with('username',Input::get('username'))
						->with('email',Input::get('email'))
						->with('address',Input::get('address'))
						->with('phone',Input::get('phone'))
						->with('nationalIdentity',Input::get('nationalIdentity'))
						->with('birthMessage',Input::get('birthMessage'))
						->with('marriageMessage',Input::get('marriageMessage'))
						->with('website',Input::get('website'))
						->with( 'messages',$messages->getMessages());
			//var_dump($messages);
			//return "no";
		}
	}

	public function phoneWith98($phone)
	{
		if(empty($phone))
			return "";
		return "98".substr($phone,strlen($phone)-10);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data=TbAdminCenters::find($id);
		if(count($data)>0)
		{
			$services=$this->activeServices($data->serviceACCid);
			return View::make('admin/centers/edit')
						->with('services',$services)
						->with('center',$data);
		}
		else
		{
			return "bad request";
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$center=TbAdminCenters::find(Input::get('id'));
		if(!$center)
		{
			return "Bad request";
		}
		$inpassword=Input::get('password');
		if(empty($inpassword))
		{
			$validate=$this->updateValidation();
			$messages = $this->validator->messages();
			if($validate)
			{
				if($center){
					$center->name=Input::get('name');
					$center->email=Input::get('email');
					$center->nationalIdentity=Input::get('nationalIdentity');
					$center->phone=$this->phoneWith98(Input::get('phone'));
					$center->address=Input::get('address');
					$center->birthMessage=Input::get('birthMessage');
					$center->marriageMessage=Input::get('marriageMessage');
					$center->bulk=Input::get('bulk');
					$center->birthmsg=Input::get('birthmsg');
					$center->marriagemsg=Input::get('marriagemsg');
					$center->scheduledmsg=Input::get('schedulemsg');
					$center->enabled=Input::get('enable');
					$center->matchservice=Input::get('matchservice');
					$center->opinion=Input::get('opinion');
					$center->serviceACCid=$this->setServicePattern();
					$center->website=Input::get('website');
					$center->save();
					$ctr=TbAdminCenters::all();
					return Redirect::to('admin/centers');
					//return View::make('admin/centers/all')
							//	->with('centers',$ctr);
				}
				else
				{
					$ctr=TbAdminCenters::all();
					return View::make('admin/centers/all')
								->with('centers',$ctr);
				}
			}//end if validate
			else
			{
				$services=$this->activeServices($center->serviceACCid);
				return View::make('admin/centers/edit')			
						->with('services',$services)
						->with('center',$center)
						->with('name',Input::get('name'))
						->with('username',Input::get('username'))
						->with('email',Input::get('email'))
						->with('address',Input::get('address'))
						->with('nationalIdentity',Input::get('nationalIdentity'))
						->with('birthMessage',Input::get('birthMessage'))
						->with('marriageMessage',Input::get('marriageMessage'))
						->with('phone',Input::get('phone'))
						->with('bulk',Input::get('bulk'))
						->with('birthmsg',Input::get('birthmsg'))
						->with('marriagemsg',Input::get('marriagemsg'))
						->with('schedulemsg',Input::get('schedulemsg'))
						->with('matchservice',Input::get('matchservice'))
						->with('opinion',Input::get('opinion'))
						->with('enable',Input::get('enable'))
						->with('website',Input::get('website'))
						->with( 'messages',$messages->getMessages());

			}

		}//end if with condition empty(Input::get('password'))


		else
		{
			$validate=$this->updateWithPasswordValidation();
			$messages = $this->validator->messages();
			if($validate)
			{
				if($center){
					$center->name=Input::get('name');
					$center->email=Input::get('email');
					$center->nationalIdentity=Input::get('nationalIdentity');
					$center->phone=$this->phoneWith98(Input::get('phone'));
					$center->address=Input::get('address');
					$center->birthMessage=Input::get('birthMessage');
					$center->marriageMessage=Input::get('marriageMessage');
					$center->password=Hash::make(Input::get('password'));
					$center->bulk=Input::get('bulk');
					$center->birthmsg=Input::get('birthmsg');
					$center->marriagemsg=Input::get('marriagemsg');
					$center->scheduledmsg=Input::get('schedulemsg');
					$center->enabled=Input::get('enable');
					$center->matchservice=Input::get('matchservice');
					$center->opinion=Input::get('opinion');
					$center->serviceACCid=$this->setServicePattern();
					$center->website=Input::get('website');
					$center->save();
					$ctr=TbAdminCenters::all();
					return Redirect::to('admin/centers');
					return View::make('admin/centers/all')
								->with('centers',$ctr);
				}
				else
				{
					$ctr=TbAdminCenters::all();
					return View::make('admin/centers/all')
								->with('centers',$ctr);
				}
			}//end if validate
			else
			{
				$services=$this->activeServices($center->serviceACCid);
				return View::make('admin/centers/edit')			
						->with('services',$services)
						->with('center',$center)
						->with('name',Input::get('name'))
						->with('username',Input::get('username'))
						->with('email',Input::get('email'))
						->with('address',Input::get('address'))
						->with('nationalIdentity',Input::get('nationalIdentity'))
						->with('birthMessage',Input::get('birthMessage'))
						->with('marriageMessage',Input::get('marriageMessage'))
						->with('phone',Input::get('phone'))
						->with('bulk',Input::get('bulk'))
						->with('birthmsg',Input::get('birthmsg'))
						->with('marriagemsg',Input::get('marriagemsg'))
						->with('schedulemsg',Input::get('schedulemsg'))
						->with('matchservice',Input::get('matchservice'))
						->with('opinion',Input::get('opinion'))
						->with('enable',Input::get('enable'))
						->with('website',Input::get('website'))
						->with( 'messages',$messages->getMessages());
			}
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$center=TbAdminCenters::find($id);
		if($center)
		{
			$result=TbAdminCenters::destroy($id);
			return Redirect::to('admin/centers');
			//return $this->index();
		}
		else
		{
			return "bad request";
		}

	}

	private function storeValidation()
	{
		$role=array('name'=>'required',
					'username'=>'required|unique:tbcenters',
					'email'=>'email',
					'phone'=>'phone:IR,mobile',
					'password'=>'required|min:5|confirmed',
					'password_confirmation' => 'required|min:5');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name'),
				'username'=>Input::get('username'),
				'email'=>Input::get('email'),
				'phone'=>Input::get('phone'),
				'password'=>Input::get('password'),
				'password_confirmation'=>Input::get('password_confirmation')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function updateValidation()
	{
		$role=array('name'=>'required',
					'email'=>'email',
					'phone'=>'phone:IR,mobile');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name'),
				'email'=>Input::get('email'),
				'phone'=>Input::get('phone')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function updateWithPasswordValidation()
	{
		$role=array('name'=>'required',
					'email'=>'email',
					'phone'=>'phone:IR,mobile',
					'password'=>'required|min:5|confirmed',
					'password_confirmation' => 'required|min:5');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name'),
				'email'=>Input::get('email'),
				'phone'=>Input::get('phone'),
				'password'=>Input::get('password'),
				'password_confirmation'=>Input::get('password_confirmation')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private function setServicePattern()
	{
		$services=TbAdminServices::all();
		$patt='';

		foreach ($services as $srv) 
		{
			if(Input::has($srv->id)&&Input::get($srv->id)==1)
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminServicesACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
					return $acc->id;
			}
			return null;
		}
	}
	private function activeServices($acc)
	{
		if($acc!=null)
		{
			$patt=DB::table('tbservicesacc')->where('id','=',$acc)->pluck('pattern');
			$patt=explode(',',$patt);
			$srv=DB::table('tbservices')->get();
			$services=array();
			$i=0;
			foreach ($srv as $s) {
				$services[$i][0]=$s->id;
				$services[$i][1]=$s->name;
				if(in_array($s->id,$patt))
					$services[$i][2]=1;
				else
					$services[$i][2]=0;
				$i++;
			}
			return $services;
		}
		else
		{
			$srv=DB::table('tbservices')->get();
			$services=array();
			$i=0;
			foreach ($srv as $s) {
				$services[$i][0]=$s->id;
				$services[$i][1]=$s->name;
				$services[$i][2]=0;
				$i++;
			}
			return $services;
		}
		
	}
}
