
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ویرایش مرکز</h1>
			<hr></hr>
			 {{ Form::open(array('url' => '/admin/services/'.$service->id.'/update','class'=>'form-horizontal well')) }}
			 	<input name="id" type="hidden" value="{{$service->id}}">
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('name', 'نام سرویس*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('name',isset($name)?$name:$service->name,array('class'=>'form-control','required' => 'required')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['name']))
				{{$messages['name'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('enable', 'فعال',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('enable',array('0'=>'نه','1'=>'بله'),$service->enable,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>

	<a href="{{url('admin/services')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
@stop