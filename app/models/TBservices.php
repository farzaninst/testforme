<?php


class TBservices extends Eloquent  {

	protected $table = 'tbservices';
	public $timestamps=false;

}

class TBsubservices extends Eloquent  {

	protected $table = 'tbsubservices';
	public $timestamps=false;

}

class TBsoftwares extends Eloquent  {

	protected $table = 'tbsoftwares';
	public $timestamps=false;

}

class TBmessengers extends Eloquent  {

	protected $table = 'tbmessengers';
	public $timestamps=false;

}

?>