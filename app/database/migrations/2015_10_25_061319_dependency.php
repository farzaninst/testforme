<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dependency extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbcenters', function($table)
		 {
			 $table->foreign('serviceACCid')->references('id')->on('tbservicesacc');
		 });
		Schema::table('tbusers', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
			 $table->foreign('messengerid')->references('id')->on('tbmessengers');
			 $table->foreign('userServiceACCid')->references('id')->on('tbservicesACC');
		 });
		Schema::table('tbnumbers', function($table)
		 {
			 //$table->foreign('centerid')->references('id')->on('tbcenters');
			 $table->foreign('softwareid')->references('id')->on('tbsoftwares');
		 });
		Schema::table('tbmatchAnswers', function($table)
		 {
			 $table->foreign('matchid')->references('id')->on('tbmatchOrOpinion');
			 $table->foreign('userid')->references('id')->on('tbusers');
			 $table->foreign('softwareid')->references('id')->on('tbsoftwares');
		 });
		/*Schema::table('tbparametrizedMessages', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
		 });*/


		Schema::table('tbparametrizedMSG', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
			 $table->foreign('serviceACCid')->references('id')->on('tbservicesACC');
			 $table->foreign('messengerid')->references('id')->on('tbmessengers');
			 //$table->foreign('numberid')->references('id')->on('tbnumbers');
		 });


		Schema::table('tbparametrizedRecievers', function($table)
		 {
			 $table->foreign('paramid')->references('id')->on('tbparametrizedMSG');
			 $table->foreign('userid')->references('id')->on('tbusers');
		 });

		Schema::table('tbbulk', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
			// $table->foreign('serviceACCid')->references('id')->on('tbservicesACC');
			// $table->foreign('messengerid')->references('id')->on('tbmessengers');
			// $table->foreign('numberid')->references('id')->on('tbnumbers');
		 });
		Schema::table('tbmessageArchive', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
		 });
		/*Schema::table('tbbulkLog', function($table)
		 {
			 $table->foreign('queueid')->references('id')->on('tbqueue');
		 });*/

		Schema::table('tbbulkQueue', function($table)
		 {
			 $table->foreign('bulkid')->references('id')->on('tbbulk');
		 });
		Schema::table('tbmatchOrOpinion', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
			 $table->foreign('serviceACCid')->references('id')->on('tbservicesACC');
			 $table->foreign('messengerid')->references('id')->on('tbmessengers');
		 });
		Schema::table('tbcentersnumbers', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
			 $table->foreign('numberid')->references('id')->on('tbnumbers');
		 });
		/*Schema::table('', function($table)
		 {
			 $table->foreign('')->references('id')->on('');
		 });*/
		Schema::table('tbusersgroups', function($table)
		 {
			 $table->foreign('userid')->references('id')->on('tbusers');
			 $table->foreign('groupid')->references('id')->on('tbgroups');
		 });
		Schema::table('tbgroups', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
		 });
		Schema::table('tbsubservices', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
			  $table->foreign('serviceid')->references('id')->on('tbservices');
		 });
		Schema::table('tbsubservicesUsers', function($table)
		 {
			 $table->foreign('userid')->references('id')->on('tbusers');
			  $table->foreign('subserviceid')->references('id')->on('tbsubservices');
		 });
		Schema::table('tbarea', function($table)
		 {
			 $table->foreign('cityid')->references('id')->on('tbcity');
		 });
		Schema::table('tbareaNumbers', function($table)
		 {
			 $table->foreign('areaid')->references('id')->on('tbarea');
		 });
		Schema::table('tbmatchReport', function($table)
		 {
			 $table->foreign('matchid')->references('id')->on('tbmatchOrOpinion');
		 });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbcenters', function($table)
		 {
			 $table->dropForeign('tbcenters_serviceACCid_foreign');
		 });
		Schema::table('tbusers', function($table)
		 {
			 $table->dropForeign('tbusers_centerid_foreign');
			 $table->dropForeign('tbusers_messengerid_foreign');
			 $table->dropForeign('tbusers_userServiceACCid_foreign');
		 });
		Schema::table('tbnumbers', function($table)
		 {
			 //$table->dropForeign('tbnumbers_centerid_foreign');
			 $table->dropForeign('tbnumbers_softwareid_foreign');
		 });
		Schema::table('tbmatchAnswers', function($table)
		 {
			 $table->dropForeign('tbmatchAnswers_matchid_foreign');
			 $table->dropForeign('tbmatchAnswers_userid_foreign');
			 $table->dropForeign('tbmatchAnswers_softwareid_foreign');
		 });
		/*Schema::table('tbparametrizedMessages', function($table)
		 {
			 $table->dropForeign('tbparametrizedMessages_centerid_foreign');
		 });*/
		Schema::table('tbparametrizedMSG', function($table)
		 {
			 $table->dropForeign('tbparametrizedMSG_centerid_foreign');
			  $table->dropForeign('tbparametrizedMSG_serviceACCid_foreign');
			   $table->dropForeign('tbparametrizedMSG_messengerid_foreign');
		 });
		Schema::table('tbparametrizedRecievers', function($table)
		 {
			 $table->dropForeign('tbparametrizedRecievers_paramid_foreign');
			 $table->dropForeign('tbparametrizedRecievers_userid_foreign');

		 });
		Schema::table('tbbulk', function($table)
		 {
			 $table->dropForeign('tbbulk_centerid_foreign');
			 $table->dropForeign('tbbulk_serviceACCid_foreign');
			 $table->dropForeign('tbbulk_messengerid_foreign');
		 });
		Schema::table('tbmessageArchive', function($table)
		 {
			 $table->dropForeign('tbmessageArchive_centerid_foreign');
		 });
	/*	Schema::table('tbbulkLog', function($table)
		 {
			 $table->dropForeign('tbbulkLog_queueid_foreign');
		 });*/

		Schema::table('tbbulkQueue', function($table)
		 {
			 $table->dropForeign('tbbulkQueue_bulkid_foreign');
		 });
		Schema::table('tbmatchOrOpinion', function($table)
		 {
			 $table->dropForeign('tbmatchOrOpinion_centerid_foreign');
			 $table->dropForeign('tbmatchOrOpinion_seviceACCid_foreign');
			 $table->dropForeign('tbmatchOrOpinion_messengerid_foreign');
		 });
		Schema::table('tbcentersnumbers', function($table)
		 {
			 $table->dropForeign('tbcentersnumbers_centerid_foreign');
			 $table->dropForeign('tbcentersnumbers_numberid_foreign');
		 });
		Schema::table('tbusersgroups', function($table)
		 {
			 $table->dropForeign('tbusersgroups_userid_foreign');
			 $table->dropForeign('tbusersgroups_groupid_foreign');
		 });
		Schema::table('tbgroups', function($table)
		 {
			 $table->dropForeign('tbgroups_centerid_foreign');
		 });
	}

}
