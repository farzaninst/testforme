<html>
<head>
  <link href="{{asset('css/JSTreeGraph.css')}}" rel="stylesheet">
<style>
      .ontop {
        position:absolute;
            top:100px;
            left:50px;
       /* padding-left: 50px;
        margin-right: 500px;
        display: block;
        position: absolute;  */     
        background-color: #225587;
        color: #000;
       /*opacity: .4;
        filter: alpha(opacity = 50);*/
      }
      body
      {
        background-color: #f5fa92;
      }
</style>
</head>
<body >
   <div><select id="dlLayout" onchange="ChangeLayout()"><option value="Horizontal">افقی</option>
        <option value="Vertical">عمودی</option></select> </div>
{{--json_encode($tree)--}}
<script src="{{asset('js/JSTreeGraph.min.js')}}"></script>
 <script type="text/javascript">

 	@if(isset($tree))

 		function findLocation(id,data)
 		{
 			for(var m=0;m<data.length;m++)
 			{
 				if(data[m].id==id)
 					return m;
 			}
 			return -1;
 		}
 		function createTree(rn2,data)
 		{
          var x=findLocation(rn2.ID,data);
          rn=data[x];
          rn2.Nodes=new Array();
          if(data[x].option!=null)
          {
            rn2.Nodes=new Array();
            //opt=rn.option;
            var j=0;
            for(var i=0;i<data[x].option.length;i++)
            {
              if(data[x].option[i].next!=null)
              {
                var index=findLocation(data[x].option[i].next,data);
                if(index>=0)
                {
                  var btxt2='<div style="display:none;background-color:#235478; width:300px;padding-bottom:10px;color:white;"><p>';
                  btxt2+='سوال: '+ data[index].text;
                  btxt2+='</p><hr></hr><p>مربوط به گزینه: '+data[x].option[i].title+'</p><p>';
                  btxt2+='</p></div>';
                  rn2.Nodes[j]={Content:"Q"+btxt2,ID:data[index].id};
                  createTree(rn2.Nodes[j],data);
                  j++;
                }
              }//if next!=null
              else
              {
                var btxt2='<div style="display:none;background-color:#235478; width:300px;padding-bottom:10px;color:white;"><p>';
                  btxt2+='جواب: '+ data[x].option[i].answer;
                  btxt2+='</p><hr></hr><p>مربوط به گزینه: '+data[x].option[i].title+'</p><p>';
                  btxt2+='</p></div>';
                rn2.Nodes[j]={Content:"A"+btxt2,ID:null};
                j++;
              }
            }//end for
          }
 		}
	 	var data={{json_encode($tree)}}
	    <!--
        var rootNode = new Object();
        var btxt='<div name="body" style="display:none;background-color:#235478; width:300px;padding-bottom:10px;color:white;"><p><h3>ریشه درخت ربات</h3></p><hr></hr><p>';
        btxt+='سوال: '+data[0].text;
        btxt+='</p></div>';
        rootNode.Content = "Q"+btxt;
       // rootNode.ToolTip=data[0].text;
        //alert(data[0].text);
        rootNode.ID=data[0].id;
        createTree(rootNode,data);
        // Get the reference to a container layer
      //  var container = document.getElementById("dvTreeContainer");
        var container = document.createElement("DIV");        // Create a <button> element                                // Append the text to <button>
        // container.setAttribute("id", "popDiv");
         container.setAttribute("class", "ontop");
        // Build tree with options
       /* DrawTree({ 
            Container: container,
            RootNode: rootNode,
            Layout: "Horizontal"
        });*/
        DrawTree({ Container: container,
                  RootNode: rootNode,
                  Layout: document.getElementById("dlLayout").value});
        document.body.appendChild(container);

        function RefreshTree() {
          DrawTree({ Container: container,
                  RootNode: rootNode,
                  Layout: document.getElementById("dlLayout").value});
        }

        function ChangeLayout() {
            RefreshTree();
        }
    @endif
    
    -->
    </script>
</body>
</html>