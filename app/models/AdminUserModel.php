<?php
use \Eloquent;
use \Exception;
use \Hash;
/*
//field in DB
id              int
username        varchar 255
email           //      //
password        text
status          int
user_groups_id  int
fname,lname     varchar 255 null
extras          text        null
remember_token  varchar 100 null

create_at,update_at =>timestamp
*/
class AdminUserModel extends Eloquent {

	//use UserTrait, RemindableTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public $table = 'adminusers';

	protected $messages=array();

	protected $errors=array(); 

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	//usr must be an array
	public function add($usr)
	{
		//$this->insertValidation($usr)
		if($this->insertValidation($usr))
		{
			$key=array_keys($usr);
			for($i=0;$i<count($usr);$i++)
			{
				$this->$key[$i]=$usr[$key[$i]];
			}
			$this->password=\Hash::make($this->password);
			$this->save();
			return true;
		}
		return false;	
	}

	public function insertValidation($data){
		//roles for validation
		$superAdmin=1;
		$roles=array(
					'username'=>'required|alpha_dash|min:3|max:255|unique:adminusers,username',
					'password'=>'required|min:5',
					'email'=>'required|max:255|email|unique:adminusers,email',
					'status'=>'integer',
					'superAdmin'=>'integer|not_in:'.$superAdmin
					);
		$validator=\Validator::make($data,$roles);
		if($validator->passes())
		{
			return true;
		}
		else
		{
			//var_dump($validator->messages());
			$this->messages=$validator->messages();
			//var_dump($this->messages);
			return false;
		}			
	}

	public function updateValidation($data){
		//roles for validation
		$superAdmin=1;
		$roles=array(
					'username'=>'required|alpha_dash|min:3|max:255',
					'password'=>'min:5',
					'email'=>'required|max:255|email',
					'status'=>'integer',
					'user_groups_id'=>'integer|not_in:'.$superAdmin,
					'extras'=>'string,array'
					);
		$validator=\Validator::make($data,$roles);
		if($validator->passes())
		{
			return true;
		}
		else
		{
			//var_dump($validator->messages());
			$this->messages=$validator->messages();
			//var_dump($this->messages);
			return false;
		}			
	}
	public function updateUser($usr,$id)
	{
		$user=$this->find($id);
		if($user)
		{
			
			/*$key=array_keys($usr);
			for($i=0;$i<count($usr);$i++)
			{
				$user->$key[$i]=$usr[$key[$i]];
			}*/
			$user->lname=$usr['lname'];
			$user->fname=$usr['fname'];
			if(!empty($this->password)){
				$user->password=Hash::make($this->password);
			}
			$user->save();
			return true;
		}
		$this->messages['notFound']='not found user with id='.$id;
		return false;	
	}
	public function araseRememberToken()
	{
		$this->remember_token=null;
		$this->save();
	}

	public function getErrors(){
		return $this->errors;
	}

	public function getMessages(){
		return $this->messages;
	}
}
