<?php

class DbExporter extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $tables=['migrations','tbservicesacc','tbcity','tbmessengers','tbservices','tbcenters','tbmessagebank','tbsubservices','tbsoftwares','tbnumbers','tbcentersnumbers','tbusers','tbsubservicesusers','tbgroups','tbusersgroups','tbarea','tbareanumbers','tbbot','tbbotquestion','tbbotoption','tbbotrelation','tbbulk','tbbulkqueue','tbmatchoropinion','tbmatchanswers','tbmatchreport','tbparametrizedmsg','tbparametrizedrecievers','tbmessagearchive'];
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('authAdmin');
	}
	public function index()
	{
		return View::make('admin.dbexporter');
	}
	public function backup()
	{
		$host=Config::get('database.connections.mysql.host');
		$username=Config::get('database.connections.mysql.username');
		$password=Config::get('database.connections.mysql.password');
		$db=Config::get('database.connections.mysql.database');
		$return=$this->backup_tables($host,$username,$password,$db,$this->tables);
		//return View::make('dbexporter')->with('data',$return);
		$file = $return;
        //$headers = array('Content-Type'=>'application/octet-stream');
      /*  ob_start();
        ob_clean();
        ob_end_clean();
        header("Content-Type: application/force-download");
	    header("Content-Length: " .(string)(filesize($file)));

	    echo file_get_contents($file);
	    die();*/
      //  return Response::download($file, 'backup.sql',$headers);
		return Response::download($return);
	}

	/* backup the db OR just a table */
	private function backup_tables($host,$user,$pass,$name,$tables = '*')
	{
		
		//$link = mysql_connect($host,$user,$pass);
		$link=mysqli_connect($host, $user, $pass, $name);
		mysqli_set_charset($link, "utf8");
		//mysql_select_db($name,$link);
		$return="";
		//get all of the tables
		if($tables == '*')
		{
			$tables = array();
			$result = mysqli_query($link,'SHOW TABLES');
			while($row = mysqli_fetch_row($result))
			{
				$tables[] = $row[0];
			}
		}
		else
		{
			$tables = is_array($tables) ? $tables : explode(',',$tables);
		}
		//dd($tables);

		//cycle through
		foreach($tables as $table)
		{

			$result = mysqli_query($link,'SELECT * FROM '.$table);
			$num_fields = mysqli_num_fields($result);
			$tbFieldName=mysqli_fetch_fields($result);
			$tbFN=[];
			foreach($tbFieldName as $tb)
			{
				$tbFN[]=$tb->name;
			}
			//$foreign=mysqli_stmt_result_metadata();
			//var_dump($foreign);
			//$return.= 'DROP TABLE '.$table.';';
			$row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
			$return.= "\n\n".$row2[1].";\n\n";
			
			for ($i = 0; $i < $num_fields; $i++) 
			{
				while($row = mysqli_fetch_row($result))
				{
					$return.= 'INSERT INTO '.$table.' VALUES(';
					for($j=0; $j < $num_fields; $j++) 
					{
						$row[$j] = addslashes($row[$j]);
						//$row[$j] = preg_replace("\n","\\n",$row[$j]);
						if ($row[$j]!=null) { 
							$return.= '"'. $row[$j].'"' ; 
						} 
						else { 
							$q=$tbFN[$j];
							$q=substr($q,strlen($q)-2);
							//var_dump($q);
							if($q=='id')
							$return.= 'NULL';
							else
							$return.='""';
						}
						if ($j < ($num_fields-1)) { $return.= ','; }
					}
					$return.= ");\n";
				}
			}
			$return.="\n\n\n";
		}
		
		//save file
		$handle = fopen(storage_path().'/db-backup___'.time().'___'.(md5(implode(',',$tables))).'.sql','w+');
		fwrite($handle, pack("CCC",0xef,0xbb,0xbf)); 
		fwrite($handle,$return);
		fclose($handle);
		return storage_path().'/db-backup___'.time().'___'.(md5(implode(',',$tables))).'.sql';
	}

}
