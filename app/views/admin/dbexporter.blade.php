@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     پشتیبان گیری
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> ایجاد پشتیبان
      </li>
    </ol>
  </div>
</div>
<div class="row"> 
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-10">
    	<a href="{{url('exporter/backup')}}" class="btn btn-success hvr-radial-out-success">ایجاد پشتیبان از دیتابیس و دانلود فایل sql</a>
    </div>
</div>
@stop