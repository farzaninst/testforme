@extends('userLayout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('css/search.css')}}">
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت بانک پیام
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست پیام ها
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12">
    @if(isset($successmsg))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$successmsg}}
        </div>
      @endif
      @if(isset($errormsg))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$errormsg}}
        </div>
      @endif
    <div class="row">
       <div id="custom-search-input">
            {{Form::open(['url' => '/messagebank/local/search', 'method' => 'POST'])}}
            <div class="input-group col-md-6">
                <input name="search" type="text" class="  search-query form-control" placeholder="جست و جو در متن و عنوان پیام" />
                <span class="input-group-btn">
                    <button class="btn btn-success" type="submit">
                        <span class=" glyphicon glyphicon-search"></span>
                    </button>
                </span>
                {{ Form::close()}}
            </div>
        </div>
    </div>
    <p></p>
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>عنوان</th>
                    <th>متن پیام</th>
                    <th style="color:transparent;">........................</th>
                </tr>
            </thead>
 
            <tbody>
                <?php $i=1; ?>
                @foreach ($data as $ctr)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{ $ctr->title }}</td>
                    <td>{{ $ctr->text }}</td>
                    <td>
                        <a href="{{url('messagebank/local/'.$ctr->id.'/edit')}}" class="btn btn-info pull-left" style="margin-right: 3px;">ویرایش</a>
                        {{Form::open(['url' => '/messagebank/local/delete/' . $ctr->id, 'method' => 'POST'])}}
                        {{Form::submit('حذف', ['class' => 'btn btn-danger pull-right'])}}
                        {{ Form::close()}}
                    </td>
                </tr>
                <?php $i++; ?>
                @endforeach
            </tbody>
 
        </table>
    </div>
    @if(is_callable(array($data, 'links')))
    <div class="row">
        <div class="col-lg-12">
            {{$data->links()}}
        </div>
    </div>
    @else
        <a href="{{url('messagebank/local')}}" class="btn btn-success">بازگشت به لیست پیام ها</a>
    @endif
    <a href="{{url('messagebank/local/create')}}" class="btn btn-success">پیام جدید</a>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop