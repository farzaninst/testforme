
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<h3>ویرایش پیام های تولد و ازدواج</h3>
	<div class="alert alert-info">
        <p>
        	در صورتی که پیام های تولد و ازدواج خالی باشند، به معنای ارسال نشدن پیام تبریک به کاربران شما است.
        </p>
    </div>
    @if(isset($msg))
    <div class="alert alert-success">
        <p>
        	{{$msg}}
        </p>
    </div>
    @endif
	{{ Form::open(array('url' => '/messages/store/birth','class'=>'form-horizontal well')) }}
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('birthMessage', 'متن پیام تبریک تولد',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::textarea('birthMessage',isset($birthMessage)?$birthMessage:'',array('class'=>'form-control','id'=>'birthMessage')) }}
			  <p>می توانید از عبارات با قاعده زیر استفاده کنید. مثلا به جای عبارت با قاعده #fname# نام کاربر قرار می گیرد</p>
	         <a id="firstname" class="btn btn-default"> نام</a>
	         <a id="family" class="btn btn-default">نام خانوادگی</a>
	         <a id="education" class="btn btn-default"> تحصیلات</a>
	         <a id="gender" class="btn btn-default"> جنسیت</a>
		</div>
		<div class="col-sm-5">
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	{{ Form::open(array('url' => '/messages/store/marriage','class'=>'form-horizontal well')) }}
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('marriageMessage', 'متن پیام تبریک ازدواج',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::textarea('marriageMessage',isset($marriageMessage)?$marriageMessage:'',array('class'=>'form-control','id'=>'marriageMessagemarriageMessage')) }}
			 <p>می توانید از عبارات با قاعده زیر استفاده کنید. مثلا به جای عبارت با قاعده #fname# نام کاربر قرار می گیرد</p>
	         <a id="firstname2" class="btn btn-default"> نام</a>
	         <a id="family2" class="btn btn-default">نام خانوادگی</a>
	         <a id="education2" class="btn btn-default"> تحصیلات</a>
	         <a id="gender2" class="btn btn-default"> جنسیت</a>
		</div>
		<div class="col-sm-5">
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
   function inyectarTexto(elemento,valor){
     var elemento_dom=document.getElementsByName(elemento)[0];
     if(document.selection){
      elemento_dom.focus();
      sel=document.selection.createRange();
      sel.text=valor;
      return;
     }if(elemento_dom.selectionStart||elemento_dom.selectionStart=="0"){
      var t_start=elemento_dom.selectionStart;
      var t_end=elemento_dom.selectionEnd;
      var val_start=elemento_dom.value.substring(0,t_start);
      var val_end=elemento_dom.value.substring(t_end,elemento_dom.value.length);
      elemento_dom.value=val_start+valor+val_end;
     }else{
      elemento_dom.value+=valor;
     }
    }               
                
$(document).ready(function(){

    $("#gender").click(function(){
     inyectarTexto('birthMessage','#gender#');
     $("#birthMessage").focus();
    });
    $("#firstname").click(function(){
     inyectarTexto('birthMessage',' #fname# ');
     $("#birthMessage").focus();
    });
    $("#family").click(function(){
     inyectarTexto('birthMessage',' #lname# ');
     $("#birthMessage").focus();
    });
    $("#education").click(function(){
     inyectarTexto('birthMessage',' #education# ');
     $("#birthMessage").focus();
    });

    $("#gender2").click(function(){
     inyectarTexto('marriageMessage','#gender#');
     $("#marriageMessage").focus();
    });
    $("#firstname2").click(function(){
     inyectarTexto('marriageMessage',' #fname# ');
     $("#marriageMessage").focus();
    });
    $("#family2").click(function(){
     inyectarTexto('marriageMessage',' #lname# ');
     $("#marriageMessage").focus();
    });
    $("#education2").click(function(){
     inyectarTexto('marriageMessage',' #education# ');
     $("#marriageMessage").focus();
    });

});
</script>
@stop