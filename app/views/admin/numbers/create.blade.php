@extends('userLayout')

@section('content')

<div class="col-lg-12">
    @if (Session::get('Msg'))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{Session::get('Msg')}} </strong> 
      </div>
    @endif
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>شماره</th>
                    <th >مسنجر</th>
                    <th >مسدود شده</th>
                    <th >مرکز</th>
                    <th >ویرایش</th>
                    <th >حذف</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1; ?>
                @foreach ($numbers as $number)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{ $number['number'] }}</td>
                    <td>
                    	@foreach ($softwares as $software)
							@if ($software['id'] == $number['softwareid'])
								{{ $software['name'] }}
							@endif
						@endforeach
                    </td>
                    <td>
                        @if ($number['blocked'] == 0)
                        	{{ "آزاد" }}
                        @endif
                        @if ($number['blocked'] == 1)
                        	{{ "مسدود" }}
                        @endif
                    </td>
                    <td>
                    	@foreach ($centersnumbers as $centernum)
                    		@if ($number['id'] == $centernum['numberid'])
	                    		@foreach ($centers as $center)
									@if ($center['id'] == $centernum['centerid'])
										{{ $center['name'] }}
									@endif
								@endforeach
							@endif
						@endforeach
                    </td>
                    {{Form::open(array('url' => '/admin/numbers/edit','class'=>'form-horizontal well','method'=>'get'))}}
                    <td>
                        <button type="submit" name="numberid" class="btn btn-primary" value="{{$number['id']}}">ویرایش</button>
                    </td>
                    {{Form::close()}}
                    {{Form::open(array('url' => '/admin/numbers/delete','class'=>'form-horizontal well','method'=>'get'))}}
                    <td>
                        <button type="submit" name="numberid" class="btn btn-danger" value="{{$number['id']}}">حذف</button>
                    </td>
                    {{Form::close()}}
                </tr>	
                <?php $i++; ?>
                @endforeach
            </tbody> 
        </table>
    </div>
    <a href="{{url('/admin/numbers/add')}}" class="btn btn-success">افزودن شماره جدید</a>
</div>
@stop