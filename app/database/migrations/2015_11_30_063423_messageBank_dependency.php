<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessageBankDependency extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbmessagebank', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
		 });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbmessagebank', function($table)
		 {
			 $table->dropForeign('tbusers_centerid_foreign');
		 });
	}

}
