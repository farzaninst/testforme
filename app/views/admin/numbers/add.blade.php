@extends('userLayout')

@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
       <small></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="fa fa-fw fa-share"></i>
      </li>
    </ol>
  </div>
</div>
{{Form::open(array('url' => '/admin/numbers/save','class'=>'form-horizontal well','method'=>'get'))}}
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @if(isset($successmsg))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
                {{$successmsg}}
            </div>
        @endif
        @if(isset($errormsg))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
                {{$errormsg}}
            </div>
        @endif
        <div class="form-group">
            <div class="col-lg-2">
                <label class="control-label" for=""> شماره : </label>
            </div>
            <div class="col-lg-5">
                <input type = "text" class="form-control" id="number" name="number" placeholder="شماره را وارد کنید..."> </input>
            </div>
            <div class="col-lg-5">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-2">
                <label class="control-label" for=""> مسنجر : </label>
            </div>
            <div class="col-lg-5">
              <select id ="softwares" name ="softwares" class="form-control">
                @foreach($softwares as $software)
                    <option value="{{$software['id']}}">{{$software['name']}}</option>
                @endforeach
              </select>  
            </div>
            <div class="col-lg-5">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-2">
                <label class="control-label" for=""> مرکز : </label>
            </div>
            <div class="col-lg-5">
                <select id ="centers" name ="centers" class="form-control">
                @foreach($centers as $center)
                    <option value="{{$center['id']}}">{{$center['name']}}</option>
                @endforeach
              </select>  
            </div>
            <div class="col-lg-5">
            </div>
        </div>
        <a href="{{url('/admin/numbers')}}" class="btn btn-success">بازگشت</a>
        <button type="submit" class="btn btn-primary" >ذخیره</button>
    </div>
</div>
{{Form::close()}}
@stop