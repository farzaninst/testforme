@extends('userLayout')
@section('header')
<h1 class="">مدیریت کاربران: مشخصات کاربر</h1>
@stop
@section('content')
@if(isset($user))
	<div class="col-lg-10 col-lg-offset-1 "> 
	    <div style="direction:rtl;">
	    	<br>
	    	<h4>نام: <span >{{$user->fname}}</span></h4>
	    	<br>
	    	<h4>نام خانوادگی: <span>{{$user->lname}}</span></h4>
	    	<br>
	    	<h4>نام کاربری: <span>{{$user->username}}</span></h4>
	    	<br>
	    	<h4>ایمیل: <span>{{$user->email}}</span></h4>
	    	<br>
	    	<h4>وضعیت: <span>{{($user->status==0)?'غیرفعال':'فعال'}}</span></h4>
  		</div>
	</div>

@endif
@stop