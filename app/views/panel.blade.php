{{--@extends('userLayout')--}}

@section('header')
    
@stop


@section('content')

<?php
    //match or opinion 
    $count = 0;
    $finish = 0;
    foreach ($match_opinion as $matchopinion) {
        $count++;
        if ($matchopinion['finish'] == 1)
            $finish++;
    }
    $notfinish = $count - $finish;
    echo "<input type='hidden' value='".$count."' id='opinion-count'>";
    echo "<input type='hidden' value='".$notfinish."' id='opinion-notfinish'>";
    echo "<input type='hidden' value='".$finish."' id='opinion-finish'>";
    //bulk
    $sendcount = 0;
    $failcount = 0;
    foreach ($bulks as $bulk) {
        $sendcount = $sendcount + $bulk['sendcount'];
        $failcount = $failcount + $bulk['failcount'];
    }
    echo "<input type='hidden' value='".$sendcount."' id='bulk-send'>";
    echo "<input type='hidden' value='".$failcount."' id='bulk-fail'>";
    $bulkcount = count($bulks);
    //parametrized
    $paramsendcount = 0;
    $paramfailcount = 0;
    foreach ($param as $param) {
        if ($param['send'] == 1) $paramsendcount ++;
        if ($param['send'] == 0) $paramfailcount ++;
    }
    echo "<input type='hidden' value='".$paramsendcount."' id='param-send'>";
    echo "<input type='hidden' value='".$paramfailcount."' id='param-fail'>";
?>
@if (Session::get('accError'))
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{Session::pull('accError' )}} </strong> 
    </div>
@endif
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      صفحه اصلی <small>خلاصه ای از وضعیت حساب شما</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="fa fa-dashboard"></i> خلاصه وضعیت
      </li>
    </ol>
  </div>
</div>
<div class="row">
  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> ارسال انبوه</h3>
      </div>
      <div class="panel-body">
        <div id="bulk-chart"></div>
        <div class="text-center"><h3> مجموع : <strong>{{$bulkcount}}</strong></h3></div>
        <div class="text-right">
          <a href={{url('/reports/bulk')}}>مشاهده جزئیات <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel panel-green">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> مسابقه و نظرسنجی</h3>
        </div>
        <div class="panel-body">
          <div id="opinion-chart"></div>
          <div class="text-center"><h3>.<strong></strong></h3></div>
          <div class="text-right">
            <a href={{url('/reports/opinion')}}>مشاهده جزئیات <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> ارسال پارامتری</h3>
      </div>
      <div class="panel-body">
        <div id="parametrize-chart"></div>
        <div class="text-center"><h3>.<strong></strong></h3></div>
        <div class="text-right">
          <a href={{url('/reports/parametrize')}}>مشاهده جزئیات <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  
    var send = $('#bulk-send').val();
    var fail = $('#bulk-fail').val();
    Morris.Donut({
        element: 'bulk-chart',
        data: [{
            label: "مجموع ارسالی موفق",
            value: send
        }, {
            label: "مجموع ارسالی ناموفق",
            value: fail
        }],
        resize: true
    });

    var count = $('#opinion-count').val();
    var notfinish = $('#opinion-notfinish').val();
    var finish = $('#opinion-finish').val();
    Morris.Donut({
        element: 'opinion-chart',
        data: [{
            label: "تعداد",
            value: count
        }, {
            label: "درحال برگذاری",
            value: notfinish
        }, {
            label: "اتمام شده",
            value: finish
        }],
        resize: true
    });

    var send = $('#param-send').val();
    var fail = $('#param-fail').val();
    Morris.Donut({
        element: 'parametrize-chart',
        data: [{
            label: "ارسال موفق",
            value: send
        }, {
            label: "ارسال ناموفق",
            value: fail
        }],
        resize: true
    });
</script>
@stop