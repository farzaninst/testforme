<?php

class bulk extends \BaseController {

	public function __construct(){
    	parent::__construct();
	}
	//private $keys=['نام','نام خانوادگی','تحصیلات','موبایل','ایمیل','جنسیت','تاهل','تاریخ تولد','تاریخ ازدواج'];
	private $keys=['موبایل'];
	public function sampleExcel()
	{
		/*$messengers=TbAdminMessengers::all()->toArray();
		$msg=[];
		foreach ($messengers as $value) {
			$msg[]=$value['name'];
		}
		$excelKeys=array_merge($this->keys,$msg);*/
		$excelKeys=$this->keys;
		Excel::create('bulk',function($excel) use($excelKeys) 
			{
				$excel->setTitle('بارگزاری کاربران به سایت');
			    // Chain the setters
			    $excel->setCreator('موسسه فرزان')
			          ->setCompany('موسسه فرزان');

			    // Call them separately
    			$excel->setDescription('مرکز مشاوره، اطلاع رسانی و خدمات کار آفرینی فرزان');
    			$excel->sheet('Sheet1', function($sheet) use($excelKeys) {

			        $head = $excelKeys;
			        $data = array($head);
			        $sheet->fromArray($data, null, 'A1', false, false);
			        $sheet->freezeFirstRow();
			        $sheet->cells('A1', function($cells) {
			        	$cells->setBackground('#585858');
			        	$cells->setFontColor('#ffffff');
			        	$cells->setAlignment('center');
			        });
			        $sheet->cells('A1', function($cells) {
			        	$cells->setBackground('#ff000000');
			        });
			        //$sheet->setAllBorders('thin');
			        $sheet->setWidth('A', 30);
			        $sheet->setColumnFormat(array(
			        	'A' =>'@',
					));
			    });
				$excel->sheet('راهنما', function($sheet){

			        $head =array('راهنمای وارد کردن اطلاعات به فایل اکسل');
			        $data = array($head);
			        $sheet->fromArray($data, null, 'A1', false, false);
			        $sheet->freezeFirstRow();
			        $sheet->cells('A1', function($cells) {
			        	$cells->setBackground('#585858');
			        	$cells->setFontColor('#ffffff');
			        	$cells->setAlignment('center');
			        });
			        $sheet->cells('A2:A20', function($cells) {
			        	$cells->setAlignment('right');
			        });
			        $sheet->setWidth('A', 120);
			        $sheet->appendRow(2,array('برای وارد کردن اطلاعات sheet1 را انتخاب کنید'));
			        $sheet->appendRow(4,array(' ستون های قرمز رنگ اجباری است'));
			    	$sheet->appendRow(12,array(' شماره موبایل ها باید با صفر شروع شوند یا 98+ یا 0098'));
			    });
			})->export('xls');
		//return Response::download(storage_path('exports').'/file.xls');
	}
	public function createExcel($failmessage=null,$successmessage=null)
	{
		if(Session::has('successmessage'))
			$successmessage=Session::pull('successmessage');
		if(Session::has('bulk_title'))
			$bulk_title=Session::pull('bulk_title');
		else
			$bulk_title=null;
		if(Session::has('bulk_msg'))
			$bulk_msg=Session::pull('bulk_msg');
		else
			$bulk_msg=null;
		if(Session::has('failmessage'))
			$failmessage=Session::pull('failmessage');
		$software = TBsoftwares::get(['name','id'])->toArray();
        $pattern = Session::get('servicepattern');
        $services=TbAdminServices::all();
		if($pattern)
		{
			$pattid=explode(',',$pattern);
			$srv=$services->filter(function($item) use($pattid)
				{
					if(!$item->enable)
						return false;
					if(in_array($item->id,$pattid))
						return true;
				})->toArray();
		}
		else
			$srv=null;
		$data = array(
  		  	'servicename'  => $srv,
    		'software'   => $software
		);
		return View::make('bulk.createExcel')->with($data)
					->with('bulk_msg',$bulk_msg)
					->with('bulk_title',$bulk_title)
					->with('failmessage',$failmessage)
					->with('successmessage',$successmessage);
	}
	public function storeExcelValidation($excel,$phone)
	{
		$role=array($phone=>'phone:IR,mobile');
		$num=0;
		foreach($excel as $ex)
		{
			$this->validator=\Validator::make(
				array($phone=>$ex[$phone]),$role);
			if($this->validator->fails())
			{
				return false;
			}
			$num++;
		}
		return true;
	}
	public function storeExcel()
	{
		if(!Input::hasFile('file'))
			return Redirect::to('bulk/excel/create')->with('failmessage','لطفا فایل اکسل را انتخاب کنید')->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));
		$file=Input::file('file');
		//dd($file->getMimeType());
		if(!($file->getMimeType()=='application/vnd.ms-office'|| $file->getMimeType()=='application/vnd.ms-excel'))
			return Redirect::to('bulk/excel/create')->with('failmessage','فایل ورودی باید یک فایل اکسل باشد')->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));

		$name=time().'_'.$file->getClientOriginalName();
		$file->move(storage_path('exports') . '/imports',$name);

		$arr=Excel::selectSheetsByIndex(0)->load(storage_path('exports') . '/imports/'.$name);
		$excel=$arr->toArray();
		$first=$arr->first()->toArray();
		if(count($excel)<2)
			Redirect::to('bulk/excel/create')->with('failmessage','فایل اکسل خالی است')->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));
		$head=array_keys($first);
		//$keys=['نام','تلفن','ایمیل','جنسیت','تاهل','تاریخ تولد','تاریخ ازدواج'];
		$intsec=array_intersect($this->keys,$head);
		if(count($intsec)!=count($this->keys))
			Redirect::to('bulk/excel/create')->with('failmessage','فایل ورودی باید مطابق با نمونه فایل اکسل پر شود')->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));
		
		$validate=$this->storeExcelValidation($excel,'موبایل');
		//dd($excel);
		if(!$validate)
			Redirect::to('bulk/excel/create')->with('failmessage','مقادیر وارد شده در فایل اکسل معتبر نمی باشد. لطفا مقادیر را با توجه به فایل راهنما پر کنید')->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));
		$centerid=Session::get('centerid');
		$center=TbAdminCenters::find($centerid);
		if(!$center)
			return Redirect::to('login');
		$blk=new TBbulk;
		$blk->centerid=Session::get('centerid');
		$blk->message=Input::get('bulk_msg');
		$blk->title=Input::get('bulk_title');
		$blk->save();
		$addusers=0;
		foreach($excel as $ex)
		{
			$user=new TBbulkQueue;
			$user->phone=$this->phoneWith98($ex['موبایل']);
			$user->bulkid=$blk->id;
			$user->save();
			$addusers++;
		}//foreach end users add :excel as ex
		return Redirect::action('bulk@createExcel')->with('successmessage','پیام با موفقیت ثبت شد و به تعداد '.$addusers.' شماره موبایل ارسال شد');
		//return $this->createExcel(null,'پیام با موفقیت ثبت شد و به تعداد '.$addusers.' شماره موبایل ارسال شد');
	}
	private function setServicePattern($input)
	{
		//dd($input);
		$services=TbAdminServices::all();
		$patt='';
		foreach ($services as $srv) 
		{
			if(in_array($srv->id,$input))
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminServicesACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
					return $acc->id;
			}
			return null;
		}
	}
	private function setSoftwarePattern($input)
	{
		//dd($input);
		$services=TbAdminMessengers::all();
		$patt='';
		foreach ($services as $srv) 
		{
			if(in_array($srv->id,$input))
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminMessengersACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
					return $acc->id;
			}
			return null;
		}
	}
	public function create()
	{
		$bulkACC = TbAdminCenters::where('id','=',Session::get('centerid'))->pluck('bulk');
		if($bulkACC == 1) {
			$software = TBsoftwares::lists('name','id');
			$area = DB::table('tbarea')->lists('id','name');
	        $services = str_split(str_replace(',','', Session::get('servicepattern')));
	        $servicename = array();
	        $msg = Input::get('msg');

	        foreach ($services as $key => $value) {
	        	$name = tbservices::where('id', '=', $value)->
	        		where('enable','=','1')-> pluck('name');
	        	array_push($servicename , $name);
	        }
	        if(Session::has('bulk_title'))
			$bulk_title=Session::pull('bulk_title');
			else
				$bulk_title='';
			if(Session::has('bulk_msg'))
				$bulk_msg=Session::pull('bulk_msg');
			else
				$bulk_msg='';
			$data = array(
	    		'software'   => $software,
	    		'msg' => $msg,
	    		'area' => $area,
	    		'bulk_title'=>$bulk_title,
	    		'bulk_msg'=>$bulk_msg
			);
			return View::make('bulk.create')->with($data);
		} else {
			return Redirect::to('/panel')->with('accError','شما مجاز به استفاده از سرویس ارسال انبوه نمی باشید !');
		}
	}

	public function store()
	{	
		if(empty(Input::get('areaid')))
			$areaID=null;
		else
			$areaID = explode(',', Input::get('areaID'));
		if(empty(Input::get('numbers')))
			$numbers=null;
		else
			$numbers = explode(',', Input::get('numbers'));
		$bulkID = TBbulk::insertGetId(array(
			'centerid' => Session::get('centerid'),
			'message' => Input::get('bulk_msg'),
			'title' => Input::get('bulk_title')
		));
		//dd($numbers);
		if ($numbers) {
			$count = 0;
			$bulkQueue = 0;
			foreach ($numbers as $number) {
				$number =  str_replace('"', '', $number);
				$number =  str_replace('[', '', $number);
				$number =  str_replace(']', '', $number);
				if(strlen($number)<10)
					continue;
				if(!$this->phoneValidation($number))
					continue;
				$number=$this->phoneWith98($number);
				$bulkQueue = DB::table('tbbulkqueue')->insertGetId(array(
					'bulkid' => $bulkID,
					'phone' => $number
				));
				$count ++ ;
			}
			if ($bulkQueue) {
				return Redirect::to('/bulk/create')->with('Msg', 'پیام با موفقیت ثبت و به '.$count.' شماره ارسال شد');
			} else {
				return Redirect::to('/bulk/create')->with('Error','شماره ای وارد نکرده اید یا شماره های وارد شده صحیح نمی باشد!')
					->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));
			}
	
		} elseif ($areaID) {
			$count = 0;
			foreach ($areaID as $id) {
				$numbers = DB::table('tbareanumbers')->
					where('areaid','=',$id)->
					pluck('number');
				if ($numbers) {
					foreach ($numbers as $number ) {
					$count ++ ;
					$bulkQueue = TBbulkqueue::insertGetId(array(
						'bulkid' => $bulkID,
						'phone' => $number
					));
				}
			}
			}
			
			if ($numbers) {
				return Redirect::to('/bulk/create')->with('Msg','پیام با موفقیت ثبت و به '.$count.' شماره ارسال شد');	
			} else {
				return Redirect::to('/bulk/create')->with('Error','لیست شماره های منطقه خالی است !')
				->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));
			}
		}else
		{
			return Redirect::to('/bulk/create')
				->with('Error','شماره ای برای ارسال وجود ندارد. یا شماره ای انتخاب کنید یا از لیست مناطق، منطقه ای را انتخاب کنید.')
				->with('bulk_msg',Input::get('bulk_msg'))->with('bulk_title',Input::get('bulk_title'));

		}
	}

	public function phoneWith98($phone)
	{
		if(empty($phone))
			return "";
		return "98".substr($phone,strlen($phone)-10);
	}
	private function phoneValidation($number)
	{
		$role=array('phone'=>'phone:IR,mobile');
		$this->validator=\Validator::make(array('phone'=>$number),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
