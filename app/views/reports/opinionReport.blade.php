@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم گزارش گیری <small>انتخاب گزارش از :</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-list-alt"></i> گزارش گیری
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    @if (Session::get('msg'))
        <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{session::get('msg')}} </strong> 
      </div>
    @endif

    @if ($errors->has('bulk_msg')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('bulk_msg') }} </strong> 
      </div>
    @endif

  <h3>تهیه گزارش از :</h3>
  <ul class="nav nav-tabs nav-justified">
    <li><a href={{url('/reports/bulk')}}>ارسال انبوه</a></li>
    <li class="active"><a href={{url('/reports/opinion')}}>نظرسنجی</a></li>
    <li><a href={{url('/reports/match')}}>مسابقه</a></li>
    <li><a href={{url('/reports/parametrize')}}>ارسال پارامتری</a></li>
  </ul>

  {{Form::open(array('url' => '/reports/opinion','class'=>'form-horizontal well','method' => 'get'))}}
  <div class="form-group">
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse1" class="fa fa-long-arrow-right"> مشاهده مشخصات نظرسنجی </a>
          </h3>
        </div>
        <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
          @foreach ($opinion as $opinion)
            <h3> عنوان نظرسنجی : <small> {{$opinion['title']}} </small></h3>
            <h3> توضیح نظرسنجی : <small> {{$opinion['desc']}} </small></h3>
            <h3> سوال نظرسنجی : <small> {{$opinion['question']}} </small></h3>
          @endforeach  
        </div>
      </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse2" class="fa fa-long-arrow-right"> مشاهده لیست ارسالی ها  </a>
          </h3>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>نام کاربر</th>
                  <th>وضعیت ارسال</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($opinionans as $ans) {
                    echo "<tr>";
                    foreach ($users as $user) {
                      if ($user['id'] == $ans['userid']) {
                        echo "<th>".$user['fname']." ". $user['lname']."</th>";
                      }
                    }
                    if ($ans['send'] ==   1) {
                      echo "<th> ارسال موفق </th></tr>";
                    } elseif ($ans['fail'] == 1) {
                      echo "<th> ارسال ناموفق </th></tr>";
                    } else {
                      echo "<th> در حال ارسال </th></tr>";
                    }
                  }
                ?>
              </tbody>
            </table>
        </div>
      </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-4">
      <label> انتخاب پیام رسان : </label>
      <select class="form-control" id="selectSoftware" name="selectSoftware">
          <option value = "0">کلیه پیام رسان ها</option>
          <?php
            foreach ($software as $software) {
              echo "<option  class='option' value='" .$software['id']."'>".$software['name']."</option>";
             }
          ?>
        </select>
    </div>
    <div class="col-lg-4">
    </div>  
    <div class="col-lg-4">
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> آمار ارسال شده ها </h3>
        </div>
        <div class="panel-body">
          <div id="opinion-progress-chart"></div>
          <div class="text-right">
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> آمار پاسخ دهندگان </h3>
        </div>
        <div class="panel-body">
          <div id="opinion-ans-chart"></div>
          <div class="text-right">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> آمار گزینه ها </h3>
        </div>
        <div class="panel-body">
          <div id="opinion-answers-chart"></div>
          <div class="text-right">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-10">
      <button type="submit" name ="submit" class="btn btn-primary" value="">بازگشت</button>
    </div>
    <div class="col-lg-2">
      
    </div>
  </div>
{{Form::close()}}
</div>
</div>

<?php 
  //chart sendfail & answers  first variable
  $inprogress = 0;
  $sended = 0;
  $faild = 0;
  $answers = 0;
  foreach ($opinionans as $ans) {
    if (!$ans['send'] && !$ans['fail']) $inprogress += 1;
    if ($ans['send'] && $ans['fail'] == 0) $sended +=1;
    if ($ans['fail'] && $ans['send'] == 0) $faild += 1;

    if ( $ans['answer'] != null ) $answers += 1;
  }
  $notanswer = count($opinionans) - $answers ;

  echo "<input type='hidden' value='".$inprogress."' id='opinion-inprogress'>";
  echo "<input type='hidden' value='".$sended."' id='opinion-sended'>";
  echo "<input type='hidden' value='".$faild."' id='opinion-faild'>";
  
  echo "<input type='hidden' value='".$answers."' id='opinion-answer'>";
  echo "<input type='hidden' value='".$notanswer."' id='opinion-notanswer'>";
?>
<?php
  //chart answers  first variable
  foreach ($answerRegex as  $regex) {
    if ($regex == '[0-1]') {
      $yes = 0;
      $no = 0;
      $notanswer = 0;
      foreach ($opinionans as $id => $ans) {
        if ($ans['answer'] == null) {
          $notanswer +=1;
        }elseif ($ans['answer'] == 1) {
          $yes += 1;
        }elseif ($ans['answer'] == 0 ){
          $no += 1;
        }
      }
      echo "<input type='hidden' value='".$yes."' id='opinion-yesanswer'>";
      echo "<input type='hidden' value='".$no."' id='opinion-noanswer'>";
      echo "<input type='hidden' value='".$notanswer."' id='opinion-notanswer'>";
      echo "<input type='hidden' value='0-1' id='opinion-answertype'>";
    } elseif($regex == '[1-10]') {
      $answer = array(0,0,0,0,0,0,0,0,0,0,0);
      foreach ($opinionans as $ans) {
        for ($i = 1; $i <= 10; $i++) { 
          if ($ans['answer'] == $i) {
            $answer[$i] = $answer[$i] + 1 ;
          }
        }
      }
      echo "<input type='hidden' value='".json_encode($answer)."' id='answer'>";
      echo "<input type='hidden' value='1-10' id='opinion-answertype'>";
    } elseif($regex == '[text]') {
      $answer = null;
      $optionnumber = 0;
      $options = explode("---", $opinion['answersJSON']);
      foreach ($options as $key => $option) {
        $optionnumber = $key + 1 ;
        echo "گزینه ".$optionnumber." -> ".$option."<br>";
      }
      foreach ($opinionans as $ans) {
        for ($i = 1; $i <= count($options); $i++) { 
          if ($ans['answer'] == $i) {
            $answer[$i] = $answer[$i] + 1 ;
          }
        }
      }
      echo "<input type='hidden' value='".json_encode($answer)."' id='answer'>";
      echo "<input type='hidden' value='".$optionnumber."' id='answercount'>";
      echo "<input type='hidden' value='text' id='opinion-answertype'>";
    }
  }
?>

<script type="text/javascript">
  //chart send/fail
  var opinion_inprogress = $('#opinion-inprogress').val();
  var opinion_sended = $('#opinion-sended').val();
  var opinion_faild = $('#opinion-faild').val();
  var sendfail_data = [{
        progresslog:"در حال ارسال",
        count: opinion_inprogress 
      }, {
        progresslog:"ارسال شده",
        count: opinion_sended
      }, {
        progresslog:"ارسال نشده",
        count: opinion_faild
      }];
  var opinionsendfailchart = Morris.Bar({
      element: 'opinion-progress-chart',
      data: sendfail_data,
      xkey: 'progresslog',
      ykeys: ['count'],
      labels: ['تعداد'],
      barRatio: 1,
      xLabelAngle: 35,
      hideHover: 'auto',
      resize: true
  });
  //chart answers count
  var opinion_answer = $('#opinion-answer').val();
  var opinion_notanswer = $('#opinion-notanswer').val();
  var answers_data = [{
          useranswer: 'پاسخ داده شده',
          count: opinion_answer
      }, {
          useranswer: 'پاسخ نداده',
          count: opinion_notanswer
      }];
  var opinionanswerschart = Morris.Bar({
      element: 'opinion-ans-chart',
      data: answers_data,
      xkey: 'useranswer',
      ykeys: ['count'],
      labels: ['تعداد'],
      barRatio: 1,
      xLabelAngle: 35,
      hideHover: 'auto',
      resize: true
  });
  //chart answers
  var opinion_answertype = $('#opinion-answertype').val();
  if (opinion_answertype == '0-1') {
    var opinion_yesanswer = $('#opinion-yesanswer').val();
    var opinion_noanswer = $('#opinion-noanswer').val();
    var opinion_notanswer = $('#opinion-notanswer').val();
    var answer_data = [{
            answer: 'بله',
            count: opinion_yesanswer
        }, {
            answer: 'خیر',
            count: opinion_yesanswer
        }, {
            answer: 'بدون پاسخ',
            count: opinion_notanswer
        }];
    var opinionanswers = Morris.Bar({
      element: 'opinion-answers-chart',
      data: answer_data,
      xkey: 'answer',
      ykeys: ['count'],
      labels: ['تعداد پاسخ'],
      barRatio: 1,
      xLabelAngle: 35,
      hideHover: 'auto',
      resize: true
    });
  }
  if (opinion_answertype == '1-10'){
    var answer = JSON.parse($('#answer').val());
    var answer_data = [];
    for (var i = 1; i <= 10; i++) {
      answer_data.push({
          answer: 'پاسخ شماره '+ i,
          count: answer[i]})
    }
    var opinionanswers = Morris.Bar({
      element: 'opinion-answers-chart',
      data: answer_data,
      xkey: 'answer',
      ykeys: ['count'],
      labels: ['تعداد پاسخ'],
      barRatio: 1,
      xLabelAngle: 35,
      hideHover: 'auto',
      resize: true
    });  
  }
  if (opinion_answertype == 'text') {
    var answers = JSON.parse($('#answer').val());
    var answercount = $('#answercount').val();
    var answer_data = [];
    if (answers) {
      for (var i = 1; i <= answercount; i++) {
        answer_data.push({
            answer: 'گزینه شماره '+ i,
            count: answers[i]})
      }
    }
    var opinionanswers = Morris.Bar({
      element: 'opinion-answers-chart',
      data: answer_data,
      xkey: 'answer',
      ykeys: ['count'],
      labels: ['تعداد پاسخ'],
      barRatio: 2,
      xLabelAngle: 35,
      hideHover: 'auto',
      resize: true
    });
  }

$('#selectSoftware').on('change', function() {
  var value = $(this).val();
  //send/fail update
  inprogress = 0;
  sended = 0;
  faild = 0;
  answers = 0;
  notanswer = 0;
  @foreach ($opinionans as $ans)
    if (value == 0) {
      //send/fail update
      if ({{!$ans['send']}} && {{!$ans['fail']}}){
        inprogress += 1;
      }
      if ({{$ans['send']}} && {{$ans['fail']}} == 0) {
        sended +=1;
      }
      if ({{$ans['fail']}} && {{$ans['send']}} == 0) {
        faild += 1;
      }
      //answers update
      if ("{{$ans['answer']}}" != null ) {
         answers += 1;
      } 
    } else {
      if (value == "{{$ans['softwareid']}}") {
        //send/fail update
        if ({{!$ans['send']}} && {{!$ans['fail']}}){
          inprogress += 1;
        }
        if ({{$ans['send']}} && {{$ans['fail']}} == 0) {
          sended +=1;
        }
        if ({{$ans['fail']}} && {{$ans['send']}} == 0) {
          faild += 1;
        }
        //answers update
        if ("{{$ans['answer']}}" != null ) {
          answers += 1;
        } 
      }
    }
  @endforeach
  sendfail_data = [];
  sendfail_data.push({
        progresslog:"در حال ارسال",
        count: inprogress 
      } , {
        progresslog:"ارسال شده",
        count: sended
      } , {
        progresslog:"ارسال نشده",
        count: faild
      });
  opinionsendfailchart.setData(sendfail_data);
  //answers update
  if (value == 0) {
    notanswer = {{count($opinionans)}} - answers ;  
  } else {
    notanswer = 0;
  }
  answers_data = [];
  answers_data.push({
          useranswer: 'پاسخ داده شده',
          count: answers
      }, {
          useranswer: 'پاسخ نداده',
          count: notanswer
      })
  opinionanswerschart.setData(answers_data);
});
</script>

@stop
