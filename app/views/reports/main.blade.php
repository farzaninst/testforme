@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم گزارش گیری <small>انتخاب گزارش از :</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-list-alt"></i> گزارش گیری
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <ul class="nav nav-tabs nav-justified">
    
    <li><a href={{url('/reports/bulk')}}>ارسال انبوه</a></li>
    <li><a href={{url('/reports/opinion')}}>نظرسنجی</a></li>
    <li><a href={{url('/reports/match')}}>مسابقه</a></li>
    <li><a href={{url('/reports/parametrize')}}>ارسال پارامتری</a></li>
  </ul>

    {{Form::open(array('url' => '/bulk/send','class'=>'form-horizontal well'))}}

 	  <div class="form-group">
        <div class="col-lg-2">
 		 	   
 	  	  </div>
 		    <div class="col-lg-3">
          
        </div>
    </div>    
    {{Form::close()}}
  </div>
</div>
@stop