@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم گزارش گیری <small>انتخاب گزارش از :</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-list-alt"></i> گزارش گیری
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    @if (Session::get('msg'))
        <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{session::get('msg')}} </strong> 
      </div>
    @endif

    @if ($errors->has('bulk_msg')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('bulk_msg') }} </strong> 
      </div>
    @endif

  <h3>تهیه گزارش از :</h3>
  <ul class="nav nav-tabs nav-justified">
    <li><a href={{url('/reports/bulk')}}>ارسال انبوه</a></li>
    <li><a href={{url('/reports/opinion')}}>نظرسنجی</a></li>
    <li><a href={{url('/reports/match')}}>مسابقه</a></li>
    <li class="active"><a href={{url('/reports/parametrize')}}>ارسال پارامتری</a></li>
  </ul>

  {{Form::open(array('url' => '/reports/parametrize','class'=>'form-horizontal well','method' => 'get'))}}
    <div class="form-group">
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse1" class="fa fa-long-arrow-right"> مشاهده مشخصات پیام </a>
          </h3>
        </div>
        <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
          @foreach ($parametrize as $parametrize)
            <h3> عنوان پیام : <small> {{$parametrize['title']}} </small></h3>
            <h3> متن پیام : <small> {{$parametrize['msgText']}} </small></h3>
            <h3> تاریخ ایجاد : <small> {{$parametrize['createDate']}} </small></h3>
            <h3> تاریخ ارسال : <small> {{$parametrize['sendDate']}} </small></h3>
            <h3> نوع ارسال : 
              <small>
              @if($parametrize['mode'] == 1)
                روزانه
              @endif
              @if($parametrize['mode'] == 2)
                هفتگی
              @endif
              @if($parametrize['mode'] == 3)
                ماهانه
              @endif
              @if($parametrize['mode'] == 4)
                سالانه
              @endif
              </small>
            </h3>
          @endforeach  
        </div>
      </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse2" class="fa fa-long-arrow-right"> مشاهده لیست ارسالی ها  </a>
          </h3>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>نام کاربر</th>
                  <th>شماره</th>
                  <th>وضعیت ارسال</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($paramlog as $param)
              <tr>
              <th> 
                @foreach ($users as $user)
                  @if($user['id'] == $param['userid'])
                    {{$user['fname']}}{{" "}}{{$user['lname']}}
                  @endif
                @endforeach
              </th>
              <th> {{$param['phone']}} </th>
              <th>
                @if ($param['send'] == 1)
                  ارسال شده
                @endif
                @if ($param['send'] == 0)
                  ارسال نشده
                @endif
              </th>
              @endforeach
              </tbody>
            </table>
        </div>
      </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-4">
      <label> انتخاب پیام رسان : </label>
      <select class="form-control" id="selectSoftware" name="selectSoftware">
          <option value = "0">کلیه پیام رسان ها</option>
          <?php
            foreach ($software as $software) {
              echo "<option  class='option' value='" .$software['id']."'>".$software['name']."</option>";
             }
          ?>
        </select>
    </div>
    <div class="col-lg-4">
    </div>  
    <div class="col-lg-4">
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> آمار ارسال شده ها </h3>
        </div>
        <div class="panel-body">
          <div id="par-progress-chart"></div>
          <div class="text-right">
            <button type="submit" name ="submit" class="btn btn-primary" value="">بازگشت</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="form-group">
    <div class="col-lg-10">
    </div>
    <div class="col-lg-2">
      
    </div>
  </div>
{{Form::close()}}
</div>
</div>
<?php
  $send = 0;
  $notsend = 0;
  $inprogress = 0;
  foreach ($paramlog as $id => $param) {
    if ($param['send'] == null) {
      $inprogress += 1 ; 
    }elseif ($param['send'] == 0) {
      $notsend +=1 ;
    }elseif ($param['send'] == 1) {
      $send +=1;
    }
  }
  echo "<input type='hidden' value='".$inprogress."' id='par-inprogress'>";
  echo "<input type='hidden' value='".$send."' id='par-send'>";
  echo "<input type='hidden' value='".$notsend."' id='par-notsend'>";
?>
<script type="text/javascript">
  var par_inprogress = $('#par-inprogress').val();
  var par_send = $('#par-send').val();
  var par_notsend = $('#par-notsend').val();
  var progress_data = [{
          progresslog: 'در صف ارسال',
          count: par_inprogress
      }, {
          progresslog: 'ارسال شده',
          count: par_send
      }, {
          progresslog: 'ارسال نشده',
          count: par_notsend
      }];
  var par = Morris.Bar({
      element: 'par-progress-chart',
      data: progress_data,
      xkey: 'progresslog',
      ykeys: ['count'],
      labels: ['تعداد'],
      barRatio: 1,
      xLabelAngle: 35,
      hideHover: 'auto',
      resize: true
  });
  $('#selectSoftware').on('change', function() {
    var value = $(this).val();
    send = 0;
    notsend = 0;
    inprogress = 0;
    @foreach ($paramlog as $id => $param)
      if (value == 0) {
        if ("{{$param['send']}}" == null) {
          inprogress += 1 ;
        }
        if ({{$param['send']}} == 0) {
          notsend += 1;
        }
        if ({{$param['send']}} == 1) {
          send += 1;
        }  
      } else {
          if ("{{$param['send']}}" == null) {
            inprogress += 1 ;
          }
          if ({{$param['send']}} == 0) {
            notsend += 1;
          }
          if ({{$param['send']}} == 1) {
            send += 1;
          } 
      }
    @endforeach
    progress_data = [];
    progress_data.push({
          progresslog: 'در صف ارسال',
          count: par_inprogress
      }, {
          progresslog: 'ارسال شده',
          count: par_send
      }, {
          progresslog: 'ارسال نشده',
          count: par_notsend
      });
    par.setData(progress_data);
  });
</script>
@stop
