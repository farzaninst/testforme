<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbservices', function($table)
		 {
			 $table->increments('id');
			 $table->string('name');
			 $table->boolean('enable');
			 $table->softDeletes();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbcenters', function($table)
		 {
			 $table->increments('id');
			 $table->string('name',250);
			 $table->string('username',250);
			 $table->string('password',500);
			 $table->string('email',250)->nullable();
			 $table->string('address',500)->nullable();
			 $table->string('phone',15)->nullable();
			 $table->text('birthMessage')->nullable();
			 $table->text('marriageMessage')->nullable();
			 $table->string('nationalIdentity',10)->nullable();
			 $table->string('website')->nullable();
			 $table->integer('credit');
			 $table->boolean('bulk');
			 $table->boolean('birthmsg');
			 $table->boolean('marriagemsg');
			 $table->boolean('scheduledmsg');
			 $table->boolean('enabled');
			 $table->boolean('matchservice');
			 $table->boolean('admin');
			 $table->boolean('opinion');
			 $table->integer('serviceACCid')->unsigned()->nullable();
			 $table->timestamps();
			 $table->rememberToken();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbsubservices', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 $table->integer('serviceid')->unsigned();
			 $table->string('name');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbcity', function($table)
		 {
			 $table->increments('id');
			 $table->string('name');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbarea', function($table)
		 {
			 $table->increments('id');
			 $table->integer('cityid')->unsigned()->nullable();
			 $table->string('name');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbareaNumbers', function($table)
		 {
			 $table->increments('id');
			 $table->integer('areaid')->unsigned()->nullable();
			 $table->string('number',15)->nullable();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbsubservicesUsers', function($table)
		 {
			 $table->increments('id');
			 $table->integer('userid')->unsigned();
			 $table->integer('subserviceid')->unsigned();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbservicesACC', function($table)
		 {
			 $table->increments('id');
			 $table->string('pattern');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbusers', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 $table->integer('userServiceACCid')->unsigned()->nullable();
			 $table->integer('messengerid')->unsigned()->nullable();
			 $table->string('phone',15)->nullable();
			 $table->string('fname',100)->nullable();
			 $table->string('lname',100)->nullable();
			 $table->string('email',200)->nullable();
			 $table->string('education',200)->nullable();
			 $table->boolean('sex');
			 $table->boolean('single');
			 $table->boolean('type');
			 $table->date('birthdate')->nullable()->nullable();
			 $table->date('marriagedate')->nullable();	
			 $table->timestamps();
			 $table->softDeletes();
			 $table->engine='InnoDB';		 
		 });
		Schema::create('tbnumbers', function($table)
		 {
			 $table->increments('id');
			 $table->string('json');
			 $table->string('number',15);
			 $table->integer('softwareid')->unsigned();
			// $table->integer('centerid')->unsigned();
			 $table->boolean('blocked');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbsoftwares', function($table)
		 {
			 $table->increments('id');
			 $table->string('name');
			 $table->boolean('enable');
			 $table->softDeletes();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbmessengers', function($table)
		 {
			 $table->increments('id');
			 $table->string('pattern');
			 $table->engine='InnoDB';
		 });
		/*Schema::create('tbuserServices', function($table)
		 {
			 $table->increments('id');
			 $table->string('pattern');
			 $table->engine='InnoDB';
		 });*/
		Schema::create('tbmatchAnswers', function($table)
		 {
			 $table->increments('id');
			 $table->integer('matchid')->unsigned();
			 $table->integer('userid')->unsigned();
			 $table->string('answer',15);
			 $table->boolean('send');
			 $table->boolean('fail');
			 $table->integer('softwareid')->unsigned()->nullable();
			 $table->engine='InnoDB';		 
		 });
		/*Schema::create('tbparametrizedMessages', function($table)
		 {
			 $table->increments('id');
			 $table->text('message');
			 $table->integer('centerid');
			 $table->smallInteger('mode');		 
		 });*/
		Schema::create('tbparametrizedMSG', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 $table->integer('serviceACCid')->unsigned()->nullable();
			 $table->integer('messengerid')->unsigned()->nullable();
			 //$table->integer('numberid')->unsigned();
			 $table->date('sendDate');
			 $table->text('msgText');
			 $table->date('createDate');
			 $table->string('title');
			 $table->smallInteger('mode');
			 $table->boolean('finish');	
			 $table->engine='InnoDB'; 
		 });
		Schema::create('tbparametrizedRecievers', function($table)
		 {
			 $table->increments('id');
			 $table->integer('paramid')->unsigned();
			 $table->integer('userid')->unsigned()->nullable();
			 $table->boolean('send');
			 $table->string('phone',15); 
			 $table->engine='InnoDB';
		 });
		Schema::create('tbbulk', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 //$table->integer('serviceACCid')->unsigned()->nullable();
			// $table->integer('numberid')->unsigned();
			 $table->string('title');
			 $table->text('message');
			// $table->integer('messengerid')->unsigned()->nullable();
			 $table->integer('failcount')->nullable();
			 $table->integer('sendcount')->nullable();
			 $table->boolean('completed');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbmatchReport', function($table)
		 {
			 $table->increments('id');
			 $table->integer('matchid')->unsigned();
			 $table->integer('serviceACCid')->unsigned()->nullable();
			// $table->integer('numberid')->unsigned();
			 $table->text('answerJSON')->nullable();
			 $table->text('winnerJSON')->nullable();
			 $table->integer('failcount')->nullable();
			 $table->integer('sendcount')->nullable();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbmessageArchive', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 $table->text('message');
			 $table->string('title');
			 $table->engine='InnoDB';
		 });
		/*Schema::create('tbbulkLog', function($table)
		 {
			 $table->increments('id');
			 $table->integer('queueid')->unsigned();
			 $table->boolean('status');
			 $table->engine='InnoDB';
		 });*/
		Schema::create('tbbulkQueue', function($table)
		 {
			 $table->increments('id');
			 $table->integer('bulkid')->unsigned();
			 $table->string('phone',15);
			 $table->boolean('send');
			 $table->boolean('fail');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbmatchOrOpinion', function($table)
		 {
			 $table->increments('id');
			 $table->text('question');
			 $table->string('answerRegex');
			 $table->date('startDate');
			 $table->date('endDate');
			 $table->text('desc');
			 $table->boolean('matchOrOpinion');	
			 $table->integer('centerid')->unsigned();
			 $table->integer('serviceACCid')->unsigned()->nullable();
			 $table->integer('messengerid')->unsigned()->nullable();
			 $table->boolean('finish');
			 $table->text('answersJSON')->nullable();
			 $table->text('backMSG')->nullable();
			// $table->boolean('match');
			 $table->string('title');	
			 $table->engine='InnoDB';	 	 
		 });
		Schema::create('tbcentersnumbers', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 $table->integer('numberid')->unsigned();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbgroups', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned();
			 $table->string('name',200);
			 $table->text('desc')->nullable();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbusersgroups', function($table)
		 {
			 $table->increments('id');
			 $table->integer('userid')->unsigned();
			 $table->integer('groupid')->unsigned();
			 $table->engine='InnoDB';
		 });
	/*	Schema::create('tbbot', function($table)
		 {
			 $table->increments('id');
			 $table->string('name'); 
			 $table->integer('centerid');
		 });
		Schema::create('tbbotQuestion', function($table)
		 {
			 $table->increments('id');
			 $table->increments('parentid');
			 $table->increments('childid');
			 $table->increments('botid');
			 $table->text('question'); 
		 });
		Schema::create('tbbotAnswer', function($table)
		 {
			 $table->increments('id');
			 $table->increments('parentid');
			 $table->increments('childid');
			 $table->increments('botid');
			 $table->text('question'); 
		 });*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbservices');
		Schema::drop('tbcenters');
		Schema::drop('tbservicesACC');
		Schema::drop('tbusers');
		Schema::drop('tbnumbers');
		Schema::drop('tbsoftwares');
		Schema::drop('tbmessengers');
		//Schema::drop('tbuserServices');
		Schema::drop('tbmatchAnswers');
		//Schema::drop('tbparametrizedMessages');
		Schema::drop('tbparametrizedMSG');
		Schema::drop('tbparametrizedRecievers');
		Schema::drop('tbbulk');
		Schema::drop('tbmessageArchive');
		//Schema::drop('tbbulkLog');
		Schema::drop('tbbulkQueue');
		Schema::drop('tbmatchOrOpinion');
		Schema::drop('tbcentersnumbers');
	}

}
