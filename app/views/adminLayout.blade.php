<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>مدیریت سایت</title>
		<meta name="description" content="بخش مدیریت سایت مدیریت کاربران مدیریت مطالب مدیریت بخش ها" />
		<meta name="keywords" content="" />
		<meta name="author" content="yazdanpanah" />
		

		<script src="{{asset('js/js-persian-cal.min.js')}}"></script>
      	<link rel='stylesheet' href="{{asset('css/js-persian-cal.css')}}">


		<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
		<link rel="shortcut icon" href="{{asset('favicon.ico')}}">
		<link rel='stylesheet' href="{{asset('css/bootstrap.rtl.min.css')}}">    

	</head>
	<body>
	<div class="well">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-lg-offset-2">
					<h4>logo here!</h4>	
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-2">
					<h1>Sitename here</h1>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
					<div class="dropdown">
					  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">نام کاربر
					  <span class="caret"></span></button>
					  <ul class="dropdown-menu">
					    <li><a href="#">پروفایل</a></li>
					    <li><a href="#">خروج</a></li>
					  </ul>
					</div>
				<br>	
    			<?php 
     	 			$date = jDate::forge()->format(' %d , %B , %Y');
      				print_r( $date);
    			?>
				</div>
			</div>
		</div>	
	 </div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12" style="border-left:1px solid #aaaaaa;">
					<ul class="nav nav-pills nav-stacked bordered" style="direction:rtl;">
						<li class="active"><a href="#">صفحه اصلی</a></li>
						<li><a href={{url('/admin/centers')}}>مدیریت مراکز</a></li>
						<li><a href="#">ارسال انبوه</a></li>
						<li class="dropdown">
						    <a class="dropdown-toggle" data-toggle="dropdown" href="#" >نظر سنجی
						    <span class="caret"></span></a>
						    <ul class="dropdown-menu" style="direction:ltr;">
						      <li><a href={{url('/opinions/create')}}>ایجاد</a></li>
						      <li><a href={{url('/opinions/edit')}}>ویرایش</a></li>
						    </ul>
						  </li>
						<li><a href={{url('/bot/create')}}>ربات</a></li>
						<li><a href="#">گزارش گیری</a></li>
						<li><a href="#">مسابقه</a></li>
					</ul>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

				@yield('content')

			</div>
			
		</div>
	</div>
	<div class="well">
		<div class="container-fluid">
			<div class="row">
				copy right &footer here
			</div>
		</div>	
	 </div>	
		<script src="{{asset('js/bootstrap.min.js')}}"></script>
	</body>
</html>