<?php

class Bot extends \BaseController {

	public function index($successmsg=null)
	{
		$allbots=Tbbot::where('centerid','=',Session::get('centerid'))->get();
		return View::make('bot.all')->with('bots',$allbots)->with('successmsg',$successmsg);	
	}

	public function create()
	{
		//return $this->getTree(7);
		return View::make('bot.create');
	}

	public function store()
	{
		$jsonbot = Input::get('jsonbot');
		$namebot = Input::get('botName');
		$botid = Tbbot::insertGetId(array(
			'name' => $namebot,
			'centerid' => Session::get('centerid'),
			'enable' => true,
			'jsontree' => $jsonbot
			));
		if ($botid) {
			return Redirect::to('/bot/create')->with('Msg','ربات شما با موفقیت در سیستم ذخیره شد.');
		}else{
			return Redirect::to('/bot/create')->with('Error','در حین ذخیره ربات خطایی رخ داده است ! لطفا دوباره تلاش کنید .');
		}
	}

	public function edit()
	{
		$bots = Tbbot::
			where('centerid','=',Session::get('centerid'))->
			get();
		if ($bots) {
			$bots = $bots->toArray();
		}
		return View::make('bot/edit')->with('bots',$bots);
	}
}
