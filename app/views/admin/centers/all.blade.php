@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت مراکز
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست مراکز
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>نام مرکز</th>
                    <th>نام کاربری</th>
                    <th>ایمیل</th>
                    <th>موبایل</th>
                    <th style="color:transparent;">........................</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($centers as $ctr)
                <tr>
                    <td>{{ $ctr->name }}</td>
                    <td>{{ $ctr->username }}</td>
                    <td>{{ $ctr->email}}</td>
                    <td>{{ $ctr->phone}}</td>
                    <td>
                        <a href="{{url('admin/centers/'.$ctr->id.'/edit')}}" class="btn btn-info hvr-radial-out-info" style="margin-right: 3px;">ویرایش</a>
                        <a href="{{url('admin/centers/'.$ctr->id.'/access')}}" class="btn btn-success hvr-radial-out-success" style="margin-right: 3px;">دسترسی به پنل</a>
                        {{-- Form::open(['url' => '/admin/centers/delete/' . $ctr->id, 'method' => 'POST']) --}}
                        {{-- Form::submit('حذف', ['class' => 'btn btn-danger pull-right hvr-bounce-to-right-danger'])--}}
                        {{-- Form::close()--}}
                    </td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {{--$centers->links()--}}
        </div>
    </div>
    <a href="{{url('admin/centers/create')}}" class="btn btn-success hvr-radial-out-success">مرکز جدید</a>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop