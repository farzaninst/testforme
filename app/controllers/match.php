<?php
use \View;
use \Input;

class match extends \BaseController {

	public function __construct(){
    	parent::__construct();
	}

	public function create()
	{
		$matchACC = TbAdminCenters::where('id','=',Session::get('centerid'))->pluck('matchservice');
		if($matchACC == 1) {
			$software = TBsoftwares::lists('name','id');
	        $servicePath = str_split( Session::get('servicepattern') );
	        $services = array();
	        
	        foreach ($servicePath as $serviceID) {
	        	if ($serviceID != ','){
	        		$name = TBservices::
	        			where('id', '=', $serviceID)->
	        			where('enable','=','1')-> pluck('name');
	        		$services[$serviceID] = $name ; 
	        	}
	        }

	        $centerGroups = DB::table('tbgroups')->where ('centerid','=',Session::get('centerid'))->get();
	        $subService = TBsubservices::where ('centerid','=',Session::get('centerid'))->get();
	        $servicename = array();
	        $msg = Input::get('msg');

	        if ($subService) {
	        	$subService = $subService->toArray();
	        }
	        

			$data = array(
	  		  	'services'  => $services,
	    		'software'   => $software,
	    		'Msg' => $msg,
	    		'centerGroups' => $centerGroups,
	    		'subService' => $subService

			);
			$admindata=array();
			if(Session::has('admin') && Session::get('admin')==1 )
			{
				$adcenters=TbAdminCenters::all();
				$adservices=TbAdminServices::all();
				$adsub=TBsubservices::all();
				if(count($adservices)==0)
					$adservices=null;
				if(count($adcenters)>0)
				{
					$i=0;
					foreach ($adcenters as $ac) {
						$admindata[$i]=array();
						$admindata[$i]['id']=$ac->id;
						$admindata[$i]['name']=$ac->name;
						$admindata[$i]['username']=$ac->username;
						$acc=$ac->serviceACCid;
						if(!$acc || count($adservices)==0)
						{
							$admindata[$i]['service']=null;
							$i++;
							continue;
						}
						$patt=TbAdminServicesACC::where('id','=',$acc)->pluck('pattern');
						$patt=explode(',',$patt);
					//	var_dump($patt);
						$srv=$adservices->filter(function($item) use($patt)
							{
								if(in_array($item->id, $patt))
									return true;
							});
					//	var_dump($srv->toArray());
						if(count($srv)==0)
						{
							$admindata[$i]['service']=null;
							$i++;
							continue;
						}
						//dd($srv->toArray());
						$j=0;
						foreach ($srv as $s) {
							$admindata[$i]['service'][$j]['id']=$s->id;
							$admindata[$i]['service'][$j]['name']=$s->name;
							$sub=$adsub->filter(function($item) use($ac,$s)
										{
											if($item->centerid==$ac->id && $item->serviceid==$s->id)
												return true;
										});
							$k=0;
							//var_dump($sub->toArray());
							foreach ($sub as $key) {
								$admindata[$i]['service'][$j]['sub'][$k]['name']=$key->name;
								$admindata[$i]['service'][$j]['sub'][$k]['id']=$key->id;
								$k++;
							}
							$j++;
						}
						$i++;
					}
				}
			}
			$servicesAd=TbAdminServices::all();
			return View::make('match.create')->with($data)->with('allServices',$servicesAd)->with('admindata',$admindata);
		} else {
			return Redirect::to('/panel')->with('accError','شما مجاز به استفاده از سرویس مسابقه نمی باشید !');
		}
	}


	public function store()
	{
		$service_checked = Input::get('service');
		$software_checked = Input::get('software');
		$sendType = Input::get('radiousers');
		$users = array();
		static $service_choose;
		$options = array();
		$options = Input::get('option');
		$options = implode("---", $options);

		if(is_array($service_checked)) {			
			foreach ($service_checked as $key => $value) {
				if (count($service_checked) == $key+1) {
					$service_choose = $service_choose . $value ;	
					break;
				}
				$service_choose = $service_choose . $value . ","; 
			}
		}
		
		static $software_choose;
		if(is_array($software_checked)) {
			foreach ($software_checked as $key => $value) {
				if (count($software_checked) == $key+1) {
					$software_choose = $software_choose . $value ;	
					break;
				}
				$software_choose = $software_choose . $value . ",";	
			}
		}

		$serviceaccid = DB::table('tbservicesacc')->where ('pattern','=',$service_choose)->pluck('id');
		$messengerid = DB::table('tbmessengers')->where ('pattern','=',$software_choose)->pluck('id');

		if ($sendType == "allSite"){  //All Site User 
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TbCentersUsers::
				where ('centerid','=',Session::get('centerid'))->
				where ('messengerid','=',$messengerid)->
				where ('type','=',0)->
				lists('id');
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به کلیه کاربران سایت (تعداد'.count($usersID).'کاربر) ارسال شد.');
			} else {
				TBopinion::
					where('id','=', $opinionID)->
					delete();
				return Redirect::to('/match/create')->with('Error','مرکز شما فاقد کاربر می باشد!');
			}
		}

		if ($sendType == "allService"){
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TbCentersUsers::
				where ('centerid','=',Session::get('centerid'))->
				where ('messengerid','=',$messengerid)->
				where ('userServiceACCid','=',$serviceaccid)->
				lists('id');
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به کاربران سرویس (تعداد'.count($usersID).'کاربر) ارسال شد.');
			} else {
				TBopinion::
					where('id','=', $opinionID)->
					delete();
				return Redirect::to('/match/create')->with('Error','مرکز شما فاقد کاربر با سرویس انتخابی می باشد!');
			}
		}

		if ($sendType == "subService"){
			$opinionID = TBopinion::insertGetId(array(
				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TBsubservicesusers::
				where ('subserviceid','=',Input::get('selectSubService'))->
				lists('userid');
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به کاربران زیرسرویس (تعداد'.count($usersID).'کاربر) ارسال شد.');
			} else {
				TBopinion::
					where('id','=', $opinionID)->
					delete();
				return Redirect::to('/match/create')->with('Error','مرکز شما فاقد کاربر با زیر سرویس انتخابی می باشد!');
			}
		}

		if ($sendType == "userFilter"){
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TBsubservicesusers::
				where ('subserviceid','=',Input::get('selectSubService'))->
				lists('userid');
			$usersID = array();
			$usersFilter = Input::get('usersfilter');
			foreach ($usersFilter as $filter) {
				if ($filter == "name") {
					$userID = TbCentersUsers::where('name','=', Input::get('usersFilterName'))->where ('centerid','=',Session::get('centerid'))->lists('id');
					if ($userID) {
						array_push($usersID , $userID);
					}
				}
				if ($filter == "age") {
					$date = getdate(date("U"));
					$curentdate =  "$date[year]";
					$birthdate = TbCentersUsers::where('centerid','=',Session::get('centerid'))->lists('birthdate','id');
					$startAge = Input::get('usersFiltersStartAge') ;
					$endAge = Input::get('usersFiltersEndAge') ;
					$age = array();
					foreach ($birthdate as $key => $value) {
						$age = $curentdate - substr($value, 0 , 4) ;
						if ($startAge <= $age && $endAge >= $age ) {
								array_push($userID, $key);
						}
					}
				}
				if ($filter == "sex") {
					$userID = TbCentersUsers::where('sex','=', Input::get('usersFilterSex'))->where ('centerid','=',Session::get('centerid'))->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "marriage") {
					$userID = TbCentersUsers::where('single','=', Input::get('usersFilterMarriage'))->where ('centerid','=',Session::get('centerid'))->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "phone") {
					$userID = TbCentersUsers::where ('centerid','=',Session::get('centerid'))->lists('id');
			    	foreach ($userID as $key => $value) {
						if (strpos($value,Input::get('usersFilterPhone') ) !== false) {
 						   array_push($usersID, $value);
						}
			    	}
				}
				if ($filter == "birthdate") {
					$userID = TbCentersUsers::
			    		where('birthdate','=',Input::get('extraBirthdate'))->
			    		where ('centerid','=',Session::get('centerid'))->lists('id');
			    	if ($phones) {
			        	array_push($usersID , $userID);
			        }
				}
				if ($filter == "marriagedate") {
					$userID = TbCentersUsers::
			    		where('marriagedate','=',Input::get('extraMarriagedate'))->
			    		where ('centerid','=',Session::get('centerid'))->lists('id');
			    	if ($userID) {
			        	array_push($usersID ,$userID);
			        }
				}
			}
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به کاربران منتخب (تعداد'.count($usersID).'کاربر) ارسال شد.');
			} else {
				TBopinion::
					where('id','=', $opinionID)->
					delete();
				return Redirect::to('/match/create')->with('Error','کاربران با مشخصات وارد شده موجود نمی باشد!');
			}
		}
		if ($sendType == "userFilterService"){
			$usersID = array();
			$usersFilter = Input::get('userservicefilter');
			$serviceID = Input::get('filterservice');
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TBsubservicesusers::
				where ('subserviceid','=',Input::get('selectSubService'))->
				lists('userid');
			foreach ($usersFilter as $filter) {
				if ($filter == "name") {
					$userID = TbCentersUsers::
						where('name','=', Input::get('userserviceFilterName'))->
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
					if ($userID) {
						array_push($usersID , $userID);
					}
				}
				if ($filter == "age") {
					$date = getdate(date("U"));
					$curentdate =  "$date[year]";
					$birthdate = TbCentersUsers::
						where('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->
						lists('birthdate','id');
					$startAge = Input::get('userserviceFiltersStartAge') ;
					$endAge = Input::get('userserviceFiltersEndAge') ;
					$age = array();
					foreach ($birthdate as $key => $value) {
						$age = $curentdate - substr($value, 0 , 4) ;
						if ($startAge <= $age && $endAge >= $age ) {
								array_push($userID, $key);
						}
					}
				}
				if ($filter == "sex") {
					$userID = TbCentersUsers::
						where('sex','=', Input::get('userserviceFilterSex'))->
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "marriage") {
					$userID = TbCentersUsers::
						where('single','=', Input::get('userserviceFilterMarriage'))->
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "phone") {
					$userID = TbCentersUsers::
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
			    	foreach ($userID as $key => $value) {
						if (strpos($value,Input::get('userserviceFilterPhone') ) !== false) {
 						   array_push($usersID, $value);
						}
			    	}
				}
				if ($filter == "birthdate") {
					$userID = TbCentersUsers::
			    		where('birthdate','=',Input::get('extraBirthdate'))->
			    		where ('centerid','=',Session::get('centerid'))->
			    		where ('userServiceACCid','=',$serviceID)->lists('id');
			    	if ($phones) {
			        	array_push($usersID , $userID);
			        }
				}
				if ($filter == "marriagedate") {
					$userID = TbCentersUsers::
			    		where('marriagedate','=',Input::get('extraMarriagedate'))->
			    		where ('centerid','=',Session::get('centerid'))->
			    		where ('userServiceACCid','=',$serviceID)->lists('id');
			    	if ($userID) {
			        	array_push($usersID ,$userID);
			        }
				}
			}
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به کاربران منتخب (تعداد'.count($usersID).'کاربر) ارسال شد.');
			} else {
				TBopinion::
					where('id','=', $opinionID)->
					delete();
				return Redirect::to('/match/create')->with('Error','کاربران با مشخصات وارد شده موجود نمی باشد!');
			}
		}

		if ($sendType == "group"){ 

			$groupSelectId = json_decode(Input::get('groupSelectId'));
			return $groupSelectId;

			//$serviceID = 
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}

			$groupUserId = null;
			$matchAnsId = null;
			foreach ($groupSelectId as $groupid) {
				$groupUserId = DB::table('tbusersgroups')->where('groupid','=',$groupid)->lists('userid');
				foreach ($groupUserId as $key => $id) {
					$matchAnsId = DB::table('tbmatchanswers')->insertGetId(array(
						'matchid' => $matchID,
						'userid' => $id
					));
				}
			}

			if ($groupUserId) {
				return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به کاربران گروه (تعداد'.count($matchAnsId).'کاربر) ارسال شد.');
			} else {
				TBopinion::
					where('id','=', $opinionID)->
					delete();
				return Redirect::to('/match/create')->with('Error','گروه انتخوابی فاقد کاربر می باشد!');
			}
		}
		if($sendType == 'center')
		{
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('match_title'),
				'question' => Input::get('match_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('match_desc'),
				'matchOrOpinion' => '0',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			if(!$this->fillCenterFilter($opinionID))
				return Redirect::to('/match/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			$count=TBopinionans::where('matchid','=',$opinionID)->get();
			$count=count($count);
			return Redirect::to('/match/create')->with('Msg','مسابقه با موفقیت ثبت و به تعداد '.$count.' ارسال شد');

		}
	}
	private function fillCenterFilter($param)
	{
		$centerid=Input::get('ad-center');

		$center=TbAdminCenters::find($centerid);
		if(!$center)
			return false;
		$serviceid=Input::get('ad-service');
		$service=TbAdminServices::find($serviceid);
		if(!$service)
			return false;
		$sub=Input::get('ad-subservice');
		//$subId=TBsubservices::whereIn('id',$sub)->lists('id');
		//dd($subId);
		$list=TBsubservicesusers::whereIn('subserviceid',$sub)->lists('userid');
		//dd($users);
		if(count($list)==0)
			return false;
		$users=TbCentersUsers::whereIn('id',$list)->get();
		if(count($users)==0)
			return false;
		//$param->save();
		foreach ($users as $usr) {
			$matchAnsId = DB::table('tbmatchanswers')->insertGetId(array(
						'matchid' => $param,
						'userid' => $usr->id
					));
		}
		return true;
	}
	public function edit()
	{
		
	}

	public function view()
	{
		$selectOpinion = Input::get('selectOpinion');
		return $selectOpinion;
	}

	public function end()
	{
		$matchans = array();
		$matchs = TBopinion::
        	where('centerid','=',Session::get('centerid'))->
        	where('matchOrOpinion','=', 0 )->
        	where('finish','=', 0 )->
        	get();
        if($matchs) {
        	$matchs = $matchs->toArray();
        }
        foreach ($matchs as $match) {
        	$ans = TBopinionans::
        		where('matchid','=',$match['id'])->
        		get();
        	if ($ans) {
        		$ans = $ans->toArray();
        		array_push($matchans, $ans);
        	}
        }
        $data = array(
        	'matchs' => $matchs,
        	'matchans' => $matchans
        	); 
		return View::make('match.end')->with($data);
	}

	public function finish()
	{
		$matchFinishID = Input::get('finishbtn');
		$selectMatchAns = Input::get('correctAnswer');
		if ($matchFinishID) {
			$sendcount = 0 ;
			$failcount = 0 ;
			$wronganswer = 0;
			$userid = array();
			$winners = array();
			$answers = array();

			$matchanswers = TBopinionans::
				where('matchid','=', $matchFinishID)->
				get();
			if ($matchanswers) {
				$matchanswers = $matchanswers->toArray();
			}
			foreach ($matchanswers as $answer) {
				if ($answer['send'] == "1") $sendcount ++;
				if ($answer['fail'] == "1") $failcount ++;
				if ($answer['answer'] == "0") $no ++;
				if ($answer['answer'] == "1") $yes ++;

				if ($answer['answer'] == $selectMatchAns) {
					array_push($userid, $answer['userid']);
				} else {
					$wronganswer ++;
				}
			}
			if ($userid != null) {
				if (count($userid) < 3)  {
					for ( $i=count($userid); $i < 3; $i++) { 
						array_push($userid, $userid[0]);
					}
				}
				$rand_keys = array_rand($userid, 3);
				array_push($winners, $userid[$rand_keys[0]]);
				array_push($winners, $userid[$rand_keys[1]]);
				array_push($winners, $userid[$rand_keys[2]]);	
			}
			
			TBopinion::where ('id','=',$matchFinishID)->update(array (
				'finish' => 1 
				)
			);
			$winners = json_encode($winners);
			$answers = json_encode($answers);
			$report = TBmatchreport::insertGetId( array(
				'matchid' => $matchFinishID,
				'answerJSON' => $answers,
				'winnerJSON' => $winners,
				'failcount' => $failcount,
				'sendcount' => $sendcount,
				)
			);
			return Redirect::to('/match/end')->with('Msg','اتمام نظرسنجی با موفقیت انجام شد.');
		}
	}
}
