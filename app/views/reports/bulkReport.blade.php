@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم گزارش گیری <small>انتخاب گزارش از :</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-list-alt"></i> گزارش گیری
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    @if (Session::get('msg'))
        <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{session::get('msg')}} </strong> 
      </div>
    @endif

    @if ($errors->has('bulk_msg')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('bulk_msg') }} </strong> 
      </div>
    @endif

  <h3>تهیه گزارش از :</h3>
  <ul class="nav nav-tabs nav-justified">
    <li class="active"><a href={{url('/reports/bulk')}}>ارسال انبوه</a></li>
    <li><a href={{url('/reports/opinion')}}>نظرسنجی</a></li>
    <li><a href={{url('/reports/match')}}>مسابقه</a></li>
    <li><a href={{url('/reports/parametrize')}}>ارسال پارامتری</a></li>
  </ul>

  {{Form::open(array('url' => '/reports/bulk','class'=>'form-horizontal well','method' => 'get'))}}
 <div class="form-group">
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse1" class="fa fa-long-arrow-right"> مشاهده مشخصات پیام </a>
          </h3>
        </div>
        <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">
          @foreach ($bulklog as $bulk)
            <h3> عنوان پیام : <small> {{$bulk['title']}} </small></h3>
            <h3> متن پیام : <small> {{$bulk['message']}} </small></h3>
            <h3> وضعیت : <small> 
              <?php
                if ($bulk['completed']==true) {
                  echo "اتمام";
                } else{
                  echo "در حال ارسال";
                }
              ?>
            </small></h3>
          @endforeach  
        </div>
      </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse2" class="fa fa-long-arrow-right"> مشاهده لیست ارسالی ها  </a>
          </h3>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>شماره ارسال</th>
                <th>وضعیت ارسال</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($bulkqueue as $bulk)
                  <tr>
                  <th>{{$bulk->phone}}</th>
                  @if ($bulk->send ==  1)
                    <th> {{ "ارسال موفق" }} </th></tr>
                  @endif
                  @if ($bulk->fail == 1)
                    <th> {{ "ارسال ناموفق" }} </th></tr>
                  @else
                    <th> {{ "در حال ارسال" }} </th>
                  @endif
                  </tr>
                @endforeach
              
            </tbody>
          </table>
          <div class="row">
            <?php
              Paginator::setPageName('report_page');
              echo $bulkqueue->appends(array_except(Request::query(), 'report_page'))->links();
            ?>           
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-12">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> نمودار وضعیت </h3>
        </div>
        <div class="panel-body">
          <div id="bulk-chart"></div>
          <div class="text-right">
            <button type="submit" name ="submit" class="btn btn-primary" value="">بازگشت</button>
          </div>
        </div>
      </div>
      <?php
        foreach ($bulklog as $bulk) {
          $count = $bulk['sendcount'] + $bulk['failcount'];
          echo "<input type='hidden' value='".$count."' id='bulk-count'>";
          echo "<input type='hidden' value='".$bulk['sendcount']."' id='bulk-sendcount'>";
          echo "<input type='hidden' value='".$bulk['failcount']."' id='bulk-failcount'>";
         } 
      ?>
    </div>
  </div>
  {{Form::close()}}
  </div>
</div>

<script type="text/javascript">
  $(function() {
    var bulk_count = $('#bulk-count').val();
    var bulk_send = $('#bulk-sendcount').val();
    var bulk_fail = $('#bulk-failcount').val();
    Morris.Bar({
        element: 'bulk-chart',
        data: [{
            bulklog: 'مجموع ارسالی ها',
            count: bulk_count
        }, {
            bulklog: 'ارسالی های موفق',
            count: bulk_send
        }, {
            bulklog: 'ارسالی های نا موفق',
            count: bulk_fail
        }],
        xkey: 'bulklog',
        ykeys: ['count'],
        labels: ['تعداد'],
        barRatio: 0.7,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true 
    });
  });
</script>
@stop
