<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BotMigrate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbbot', function($table)
		 {
			 $table->increments('id');
			 $table->string('name');
			 $table->integer('centerid')->unsigned();
			 $table->boolean('enable');
			 $table->boolean('tree_completed');
			 $table->timestamps();
			 $table->softDeletes();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbbotquestion', function($table)
		 {
			 $table->increments('id');
			 $table->text('text');
			 $table->integer('botid')->unsigned();
			 $table->boolean('root');
			 $table->timestamps();
			 $table->softDeletes();
			 $table->text('description')->nullable();
			 $table->engine='InnoDB';
		 });
		Schema::create('tbbotoption', function($table)
		 {
			 $table->increments('id');
			 $table->string('title');
			 $table->integer('questionid')->unsigned();
			 $table->text('answer')->nullable();
			 $table->integer('order');
			 $table->engine='InnoDB';
		 });
		Schema::create('tbbotrelation', function($table)
		 {
			 $table->increments('id');
			 $table->integer('questionid')->unsigned();
			 $table->integer('optionid')->unsigned();
			 $table->engine='InnoDB';
		 });

		Schema::table('tbbot', function($table)
		 {
			 $table->foreign('centerid')->references('id')->on('tbcenters');
		 });
		Schema::table('tbbotquestion', function($table)
		 {
			 $table->foreign('botid')->references('id')->on('tbbot');
		 });

		Schema::table('tbbotoption', function($table)
		{
			 $table->foreign('questionid')->references('id')->on('tbbotquestion');
		 }); 
		
		Schema::table('tbbotrelation', function($table)
		{
			 $table->foreign('questionid')->references('id')->on('tbbotquestion');
			 $table->foreign('optionid')->references('id')->on('tbbotoption');
		 }); 

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
