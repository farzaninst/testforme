@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop

@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم گزارش گیری <small>انتخاب گزارش از :</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-list-alt"></i> گزارش گیری
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    @if (Session::get('msg'))
        <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{session::get('msg')}} </strong> 
      </div>
    @endif

    @if ($errors->has('bulk_msg')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('bulk_msg') }} </strong> 
      </div>
    @endif

  <h3>تهیه گزارش از :</h3>
  <ul class="nav nav-tabs nav-justified">
    <li><a href={{url('/reports/bulk')}}>ارسال انبوه</a></li>
    <li><a href={{url('/reports/opinion')}}>نظرسنجی</a></li>
    <li class="active"><a href={{url('/reports/match')}}>مسابقه</a></li>
    <li><a href={{url('/reports/parametrize')}}>ارسال پارامتری</a></li>
  </ul>

  {{Form::open(array('url' => '/reports/match','class'=>'form-horizontal well','method' => 'get'))}}
          
  <div class="form-group">
    <div class="col-lg-2">
    </div>
    <div class="col-lg-10">
      <div class="well well-lg">
        <h4>نفرات برتر</h4>
        @if (isset($matchans)) 
          <h5>برای نمایش نفرات برتر تا اتمام مسابقه صبر و یا از منوی مسابقه گزینه اتمام را انتخاب کنید.</h5>
        @endif
        @if (isset($matchrep))
          <table class="table table-bordered" >
          <thead>
            <tr>
              <th>نفر اول</th>
              <th>نفر دوم</th>
              <th>نفر سوم</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <?php
              $winner =0;
              foreach ($matchrep as $report) {
                $winner = $report['winnerJSON'];
              }
              $winner = json_decode($winner);
              if ($winner) {
                foreach ($users as $user) {
                  if ($winner[0] == $user['id']) echo "<th>".$user['fname']." ".$user['lname']."</th>";
                }
                foreach ($users as $user) {
                  if ($winner[1] == $user['id']) echo "<th>".$user['fname']." ".$user['lname']."</th>";
                }
                foreach ($users as $user) {
                  if ($winner[2] == $user['id']) echo "<th>".$user['fname']." ".$user['lname']."</th>";
                }
              }
            ?>
          </tr>    
          </tbody>
        </table>
        @endif
        
      </div>

      <div class="well well-lg">
        <h4>آمار گزینه ها</h4>
        <table class="table table-bordered" >
          <thead>
            <?php
              foreach ($answerRegex as  $regex) {
                if ($regex == '[0-1]') {
                  echo '<th>بله</th>';
                  echo '<th>خیر</th>';
                  echo '<th>بدون پاسخ</th>';
                }
                if ($regex == '[text]') {
                  //$options = explode("---", $opinion['answersJSON']);
                  //echo '<th>'.$options[0].'</th>';
                  //echo '<th>'.$options[1].'</th>';
                  //echo '<th>'.$options[2].'</th>';
                }
              }
            ?>
          </thead>
          <tbody>
            <tr>
            <?php
            if (isset($matchans)) {
              foreach ($answerRegex as  $regex) {
                if ($regex == '[0-1]') {
                  $yes = 0;
                  $no = 0;
                  $noanswer = 0;
                  foreach ($matchans as $ans) {
                    switch ($ans['answer']) {
                      case "0":
                        $no += 1;
                        break;
                      case "1":
                        $yes += 1;
                        break;
                      default:
                        $noanswer +=1;
                        break;
                    }
                  }
                  echo '<th>'.$yes.'</th>';
                  echo '<th>'.$no.'</th>';
                  echo '<th>'.$noanswer.'</th>';
                }
              }
            } elseif (isset($matchrep)) {
              $answers = 0;
              foreach ($matchrep as $report) {
                $answers = $report['answerJSON'];
              }
              $answers = json_decode($answers,true);
              if ($answers) {
                echo '<th>'.$answers[1].'</th>';
                echo '<th>'.$answers[0].'</th>';
                echo '<th>'.$answers[2].'</th>';
              }
            }
            ?>
          </tr>    
          </tbody>
        </table>
      </div>

      <div id = "divgroup">
        @if (isset($matchans))
        <table class="table table-bordered" id="grouptable" name="grouptable" >
          <thead>
            <tr>
              <th>نام کاربر</th>
              <th>وضعیت ارسال</th>
              <th>نوع پیام رسان</th>
              <th>پاسخ</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($matchans as $ans)
              <tr>
              <th>
              <?php
                foreach ($users as $user) {
                  if ($user['id'] == $ans['userid']) echo $user['fname']." ".$user['lname'];
                }
              ?> 
              </th>
              <th>
                <?php
                  if (!$ans['send'] && !$ans['fail']) echo "در صف ارسال";
                  if ($ans['send'] && $ans['fail'] == 0) echo "ارسال موفق";
                  if ($ans['fail'] && $ans['send'] == 0) echo "ارسال ناموفق";
                ?>
              </th>
              <th>
                @foreach ($software as $id => $name)
                  @if ($id == $ans['softwareid'])
                    {{$name}}
                  @endif
                @endforeach
              </th>
              <th>
                {{$ans['answer']}}
              </th>
              </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-lg-10">
      <button type="submit" name ="submit" class="btn btn-primary" value="">بازگشت</button>
    </div>
    <div class="col-lg-2">
      
    </div>
  </div>
{{Form::close()}}
</div>
</div>
@stop
