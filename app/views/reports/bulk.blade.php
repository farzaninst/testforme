
@extends('userLayout')

@section('content')


<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم گزارش گیری <small>انتخاب گزارش از :</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-list-alt"></i> گزارش گیری
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    @if (isset($msg))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{$msg}} </strong> 
      </div>
    @endif

    @if ($errors->has('bulk_msg')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> ا{{ $errors->first('bulk_msg') }} </strong> 
      </div>
    @endif

    @if (Session::get('Msg'))
        <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{Session::get('Msg')}} </strong> 
      </div>
    @endif
  <ul class="nav nav-tabs nav-justified">
    <li class="active"><a href={{url('/reports/bulk')}}>ارسال انبوه</a></li>
    <li><a href={{url('/reports/opinion')}}>نظرسنجی</a></li>
    <li><a href={{url('/reports/match')}}>مسابقه</a></li>
    <li><a href={{url('/reports/parametrize')}}>ارسال پارامتری</a></li>
  </ul>
{{Form::open(array('url' => '/reports/bulk','class'=>'form-horizontal well','method'=>'get'))}}
<div class="form-group">
  <div class="col-lg-12">
    <div class="navbar navbar-default">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
          data-target=".navbar-collapse">
          <span class="icon-bar"></span> 
          <span class="icon-bar"></span> 
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">فیلتر لیست براساس:</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav pull-right">
          <li class="divider-vertical"></li>
          <li class="dropdown">
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">عنوان پیام <strong class="caret"></strong></a>
            <div class="dropdown-menu" style="padding: 15px; padding-bottom: 0px;">
                <label>قسمت یا تمام متن مورد جست و جو را وارد کنید</lable><br>
                <div class="divider"></div>
                <input id="search-title" name="search-title" style="margin-bottom: 15px;" type="text" size="30" />
                <button type="submit" name ="submit" class="btn btn-primary" value="search" style="clear: left; width: 100%; height: 32px; font-size: 13px;">جست و جو</button>
            </div>
          </li>
        </ul>
        <ul class="nav pull-right">
          <li class="divider-vertical"></li>
          <li class="dropdown">
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">متن پیام <strong class="caret"></strong></a>
            <div class="dropdown-menu" style="padding: 15px; padding-bottom: 0px;">
                <label>قسمت یا تمام متن مورد جست و جو را وارد کنید</lable><br>
                <div class="divider"></div>
                <input id="search-message" name="search-message" style="margin-bottom: 15px;" type="text" size="30" />
                <button type="submit" name ="submit" class="btn btn-primary" value="search" style="clear: left; width: 100%; height: 32px; font-size: 13px;">جست و جو</button> 
            </div>
          </li>
        </ul>
        <ul class="nav pull-right">
          <li class="divider-vertical"></li>
          <li class="dropdown">
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">وضعیت ارسال <strong class="caret"></strong></a>
            <div class="dropdown-menu" style="padding: 15px; padding-bottom: 10px;">
                <button type="submit" name ="submit" class="btn btn-primary" value="search1" style="clear: left; width: 100%; height: 32px; font-size: 13px;">اتمام شده</button>
                <div class="divider"></div>
                <button type="submit" name ="submit" class="btn btn-primary" value="search0" style="clear: left; width: 100%; height: 32px; font-size: 13px;">نا تمام</button>
            </div>
          </li>
        </ul>
    </div>
  </div>
  <div class="table-responsive clear back-white col-lg-12">
  <table id ="tbl-bulk"class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>عنوان پیام</th>
        <th>پیغام</th>
        <th>وضعیت ارسال</th>
        <th>حذف</th>
        <th>گزارش گیری</th>
      </tr>
    </thead> 
    <tbody id="tbl-body">
      @foreach ($bulks as $bulk)
      <tr>
        <td>{{$bulk->title}}</td>
        <td>{{$bulk->message}}</td>
        <td>
          <?php 
            $completed = $bulk->completed;
            if ($completed) {
              echo "اتمام شده";
            } else {
              echo "نا تمام";
            }
          ?>
        </td>
        <td>
          <?php
            echo "<input type='checkbox' id='deleteCheck' name='deleteCheck[]' value ='".$bulk->id."'></input>";
          ?>
        </td>
        <td>
          <?php
            echo "<button type='submit' name ='submit' class='btn btn-primary' value='".$bulk->id."'>گزارش</button>";
          ?>
        </td>
      </tr>
      @endforeach
    </tbody> 
  </table>
  <div class="row">
    
    <?php
      echo $bulks->links();
    ?>

  </div>
</div>

<div class="form-group">
  <div class="col-lg-10">

  </div>
  <div class="col-lg-2">
    <button type="submit" name ="submit" class="btn btn-danger" value="delete">حذف</button>
  </div>
</div>    
{{Form::close()}} 
</div>


</div>
</div>

<script type='text/javascript'>
$(function() {
  $('.dropdown-toggle').dropdown();
  $('.dropdown input, .dropdown label').click(function(e) {
    e.stopPropagation();
  });
});
</script>

@stop
