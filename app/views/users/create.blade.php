
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ثبت کاربر جدید</h1>
			<hr></hr>
			 {{ Form::open(array('url' => '/users/store','class'=>'form-horizontal well')) }}
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('fname', 'نام',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('fname',isset($fname)?$fname:'',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['fname']))
				{{$messages['fname'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('lname', 'نام خانوادگی',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('lname',isset($lname)?$lname:'',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['lname']))
				{{$messages['lname'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('education', 'تحصیلات',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{-- Form::text('education',isset($education)?$education:'',array('class'=>'form-control')) --}}
			 {{ Form::select('education',array('0'=>'مشخص نیست','1'=>'زیر دیپلم','2'=>'دیپلم','3'=>'فوق دیپلم','4'=>'کارشناسی','5'=>'کارشناسی ارشد','6'=>'دکتری و بالاتر'),'0',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['education']))
				{{$messages['education'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('email', 'ایمیل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('email',isset($email)?$email:'',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['email']))
				{{$messages['email'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('phone', 'موبایل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('phone',isset($phone)?$phone:'',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['phone']))
				{{$messages['phone'][0]}}
			@endif
		</div>
	</div>
	
	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('sex', 'جنسیت',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('sex',array('0'=>'زن','1'=>'مرد'),'0',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>

	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('single', 'وضعیت تاهل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('single',array('0'=>'مجرد','1'=>'متاهل'),'0',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@if(isset($services))
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('services', 'سرویس های کاربر',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   @foreach ($services as $service)
	            <lable> {{$service->name}} <input tabindex="1" type="checkbox" name="service[{{$service->id}}]" id="{{$service->id}}" checked="true"></lable>
	          @endforeach
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@endif
	@if(isset($softwares))
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('softwares', 'پیام گیر های کاربر',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   @foreach ($softwares as $software)
	            <lable> {{$software->name}} <input tabindex="1" type="checkbox" name="software[{{$software->id}}]" id="{{$software->id}}" checked="true"></lable>
	          @endforeach
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@endif
	<p></p> 
   <div class="row">
    <div class="col-sm-3">
      {{ Form::label('birth', 'تاریخ تولد',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
         <input type="text" class="pdate form-control cursor-pointer" id="birth" name="birth" value="" placeholder="" readonly>
        <input type="hidden" id="birthdate" name="birthdate" class="pdate wide ">
    </div>
    <div class="col-sm-5">
      
    </div>
  </div>
  <p></p> 
   <div class="row">
    <div class="col-sm-3">
      {{ Form::label('marriage', 'تاریخ ازدواج',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
         <input type="text" class="pdate form-control cursor-pointer" id="marriage" name="marriage" value="" placeholder="" readonly>
        <input type="hidden" id="marriagedate" name="marriagedate" class="pdate wide ">
    </div>
    <div class="col-sm-5">
      
    </div>
  </div>
  <p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>

	<a href="{{url('users')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
    var objCalBirth = new AMIB.persianCalendar( 'birth', {
        extraInputID: 'birthdate',
        extraInputFormat: 'YYYY-MM-DD'
      }
    );
    var objCalMarriage = new AMIB.persianCalendar( 'marriage', {
        extraInputID: 'marriagedate',
        extraInputFormat: 'YYYY-MM-DD'
      }
    );
  </script>
@stop