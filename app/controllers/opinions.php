<?php
use \View;
use \Input;

class opinions extends \BaseController {

	public function __construct(){
    	parent::__construct();
	}

	public function create()
	{
		$matchACC = TbAdminCenters::where('id','=',Session::get('centerid'))->pluck('matchservice');
		if($matchACC == 1) {
			$software = TBsoftwares::lists('name','id');
	        //$services = str_split(str_replace(',','', Session::get('servicepattern')));
	      //  var_dump(Session::get('servicepattern'));
	        $servicePath = str_split( Session::get('servicepattern') );
	        $services = array();
	       // var_dump($servicePath);
	        foreach ($servicePath as $serviceID) {
	        	if ($serviceID != ','){
	        		$name = TBservices::
	        			where('id', '=', $serviceID)->
	        			where('enable','=','1')-> pluck('name');
	        		$services[$serviceID] = $name ; 
	        	}
	        }

	        $centerGroups = DB::table('tbgroups')->where ('centerid','=',Session::get('centerid'))->get();
	        $subService = TBsubservices::where ('centerid','=',Session::get('centerid'))->get();
	        $servicename = array();
	        $msg = Input::get('msg');

	        if ($subService) {
	        	$subService = $subService->toArray();
	        }
	        

			$data = array(
	  		  	'services'  => $services,
	    		'software'   => $software,
	    		'msg' => $msg,
	    		'centerGroups' => $centerGroups,
	    		'subService' => $subService

			);
			//dd($data);
			$admindata=array();
			if(Session::has('admin') && Session::get('admin')==1 )
			{
				$adcenters=TbAdminCenters::all();
				$adservices=TbAdminServices::all();
				$adsub=TBsubservices::all();
				if(count($adservices)==0)
					$adservices=null;
				if(count($adcenters)>0)
				{
					$i = 0;
					foreach ($adcenters as $ac) {
						$admindata[$i]=array();
						$admindata[$i]['id']=$ac->id;
						$admindata[$i]['name']=$ac->name;
						$admindata[$i]['username']=$ac->username;
						$acc=$ac->serviceACCid;
						if(!$acc || count($adservices)==0)
						{
							$admindata[$i]['service']=null;
							$i++;
							continue;
						}
						$patt=TbAdminServicesACC::where('id','=',$acc)->pluck('pattern');
						$patt=explode(',',$patt);
					//	var_dump($patt);
						$srv=$adservices->filter(function($item) use($patt)
							{
								if(in_array($item->id, $patt))
									return true;
							});
					//	var_dump($srv->toArray());
						if(count($srv)==0)
						{
							$admindata[$i]['service']=null;
							$i++;
							continue;
						}
						//dd($srv->toArray());
						$j=0;
						foreach ($srv as $s) {
							$admindata[$i]['service'][$j]['id']=$s->id;
							$admindata[$i]['service'][$j]['name']=$s->name;
							$sub=$adsub->filter(function($item) use($ac,$s)
										{
											if($item->centerid==$ac->id && $item->serviceid==$s->id)
												return true;
										});
							$k=0;
							//var_dump($sub->toArray());
							foreach ($sub as $key) {
								$admindata[$i]['service'][$j]['sub'][$k]['name']=$key->name;
								$admindata[$i]['service'][$j]['sub'][$k]['id']=$key->id;
								$k++;
							}
							$j++;
						}
						$i++;
					}
				}
			}
			$servicesAd=TbAdminServices::all();
			return View::make('opinion.create')->with($data)->with('allServices',$servicesAd)->with('admindata',$admindata);
		} else {
			return Redirect::to('/panel')->with('accError','شما مجاز به استفاده از سرویس نظرسنجی نمی باشید !');
		}
	}


	public function store()
	{
		$service_checked = Input::get('service');
		$software_checked = Input::get('software');
		$sendType = Input::get('radiousers');
		static $service_choose;
		$users = array();
		$options = array();
		$options[1] = Input::get('option1');
		$options[2] = Input::get('option2');
		$options[3] = Input::get('option3');
		$options = implode("---", $options);

		if(is_array($service_checked)) {			
			foreach ($service_checked as $key => $value) {
				if (count($service_checked) == $key+1) {
					$service_choose = $service_choose . $value ;	
					break;
				}
				$service_choose = $service_choose . $value . ","; 
			}
		}
		
		static $software_choose;
		if(is_array($software_checked)) {
			foreach ($software_checked as $key => $value) {
				if (count($software_checked) == $key+1) {
					$software_choose = $software_choose . $value ;	
					break;
				}
				$software_choose = $software_choose . $value . ",";	
			}
		}

		$serviceaccid = DB::table('tbservicesacc')->where ('pattern','=',$service_choose)->pluck('id');
		$messengerid = DB::table('tbmessengers')->where ('pattern','=',$software_choose)->pluck('id');

		if ($sendType == "allSite"){  //All Site User 
			$opinionID = TBopinion::insertGetId(array(
				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' =>  $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TbCentersUsers::
				where ('centerid','=',Session::get('centerid'))->
				where ('messengerid','=',$messengerid)->
				where ('type','=',0)->
				lists('id');
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/opinions/create')->with('Msg','نظرسنجی با موفقیت ثبت و به کلیه کاربران سایت ارسال شد.');
			} else {
				return Redirect::to('/opinions/create')->with('Error','مرکز شما فاقد کاربر می باشد!');
			}
		}

		if ($sendType == "allService"){
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' => $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => $serviceaccid,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TbCentersUsers::
				where ('centerid','=',Session::get('centerid'))->
				where ('messengerid','=',$messengerid)->
				where ('userServiceACCid','=',$serviceaccid)->
				lists('id');
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/opinions/create')->withmsg('نظرسنجی با موفقیت ثبت و به کاربران سرویس انتخابی ارسال شد.');
			} else {
				return Redirect::to('/opinions/create')->witherror('مرکز شما فاقد کاربر با سرویس انتخابی می باشد!');
			}
		}

		if ($sendType == "subService"){
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' => $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TBsubservicesusers::
				where ('subserviceid','=',Input::get('selectSubService'))->
				lists('userid');
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/opinions/create')->with('Msg','نظرسنجی با موفقیت ثبت و به کاربران زیر سرویس انتخابی ارسال شد.');
			} else {
				return Redirect::to('/opinions/create')->with('error','مرکز شما فاقد کاربر با زیر سرویس انتخابی می باشد!');
			}
		}

		if ($sendType == "userFilter"){
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' => $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TBsubservicesusers::
				where ('subserviceid','=',Input::get('selectSubService'))->
				lists('userid');
			$usersID = array();
			$usersFilter = Input::get('usersfilter');
			foreach ($usersFilter as $filter) {
				if ($filter == "name") {
					$userID = TbCentersUsers::where('name','=', Input::get('usersFilterName'))->where ('centerid','=',Session::get('centerid'))->lists('id');
					if ($userID) {
						array_push($usersID , $userID);
					}
				}
				if ($filter == "age") {
					$date = getdate(date("U"));
					$curentdate =  "$date[year]";
					$birthdate = TbCentersUsers::where('centerid','=',Session::get('centerid'))->lists('birthdate','id');
					$startAge = Input::get('usersFiltersStartAge') ;
					$endAge = Input::get('usersFiltersEndAge') ;
					$age = array();
					foreach ($birthdate as $key => $value) {
						$age = $curentdate - substr($value, 0 , 4) ;
						if ($startAge <= $age && $endAge >= $age ) {
								array_push($userID, $key);
						}
					}
				}
				if ($filter == "sex") {
					$userID = TbCentersUsers::where('sex','=', Input::get('usersFilterSex'))->where ('centerid','=',Session::get('centerid'))->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "marriage") {
					$userID = TbCentersUsers::where('single','=', Input::get('usersFilterMarriage'))->where ('centerid','=',Session::get('centerid'))->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "phone") {
					$userID = TbCentersUsers::where ('centerid','=',Session::get('centerid'))->lists('id');
			    	foreach ($userID as $key => $value) {
						if (strpos($value,Input::get('usersFilterPhone') ) !== false) {
 						   array_push($usersID, $value);
						}
			    	}
				}
				if ($filter == "birthdate") {
					$userID = TbCentersUsers::
			    		where('birthdate','=',Input::get('extraBirthdate'))->
			    		where ('centerid','=',Session::get('centerid'))->lists('id');
			    	if ($phones) {
			        	array_push($usersID , $userID);
			        }
				}
				if ($filter == "marriagedate") {
					$userID = TbCentersUsers::
			    		where('marriagedate','=',Input::get('extraMarriagedate'))->
			    		where ('centerid','=',Session::get('centerid'))->lists('id');
			    	if ($userID) {
			        	array_push($usersID ,$userID);
			        }
				}
			}
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/opinions/create')->with('Msg','نظرسنجی با موفقیت ثبت و به کاربران منتخب ارسال شد');
			} else {
				return Redirect::to('/opinions/create')->with('Error','کاربران با مشخصات وارد شده موجود نمی باشد!');
			}
		}
		if ($sendType == "userFilterService"){
			$usersID = array();
			$usersFilter = Input::get('userservicefilter');
			$serviceID = Input::get('filterservice');
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' => $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => $serviceID,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			$usersID = TBsubservicesusers::
				where ('subserviceid','=',Input::get('selectSubService'))->
				lists('userid');
			foreach ($usersFilter as $filter) {
				if ($filter == "name") {
					$userID = TbCentersUsers::
						where('name','=', Input::get('userserviceFilterName'))->
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
					if ($userID) {
						array_push($usersID , $userID);
					}
				}
				if ($filter == "age") {
					$date = getdate(date("U"));
					$curentdate =  "$date[year]";
					$birthdate = TbCentersUsers::
						where('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->
						lists('birthdate','id');
					$startAge = Input::get('userserviceFiltersStartAge') ;
					$endAge = Input::get('userserviceFiltersEndAge') ;
					$age = array();
					foreach ($birthdate as $key => $value) {
						$age = $curentdate - substr($value, 0 , 4) ;
						if ($startAge <= $age && $endAge >= $age ) {
								array_push($userID, $key);
						}
					}
				}
				if ($filter == "sex") {
					$userID = TbCentersUsers::
						where('sex','=', Input::get('userserviceFilterSex'))->
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "marriage") {
					$userID = TbCentersUsers::
						where('single','=', Input::get('userserviceFilterMarriage'))->
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
			      	if ($userID) {
			      		array_push($usersID, $userID);
			      	}
				}
				if ($filter == "phone") {
					$userID = TbCentersUsers::
						where ('centerid','=',Session::get('centerid'))->
						where ('userServiceACCid','=',$serviceID)->lists('id');
			    	foreach ($userID as $key => $value) {
						if (strpos($value,Input::get('userserviceFilterPhone') ) !== false) {
 						   array_push($usersID, $value);
						}
			    	}
				}
				if ($filter == "birthdate") {
					$userID = TbCentersUsers::
			    		where('birthdate','=',Input::get('extraBirthdate'))->
			    		where ('centerid','=',Session::get('centerid'))->
			    		where ('userServiceACCid','=',$serviceID)->lists('id');
			    	if ($phones) {
			        	array_push($usersID , $userID);
			        }
				}
				if ($filter == "marriagedate") {
					$userID = TbCentersUsers::
			    		where('marriagedate','=',Input::get('extraMarriagedate'))->
			    		where ('centerid','=',Session::get('centerid'))->
			    		where ('userServiceACCid','=',$serviceID)->lists('id');
			    	if ($userID) {
			        	array_push($usersID ,$userID);
			        }
				}
			}
			if ($usersID) {
				foreach ($usersID as $id) {
					$matchAnsId = TBopinionans::insertGetId(array(
						'matchid' => $opinionID,
						'userid' => $id
					));
				}
				return Redirect::to('/opinions/create')->with('Msg','نظرسنجی با موفقیت ثبت و به کاربران منتخب ارسال شد');
			} else {
				return Redirect::to('/opinions/create')->with('Error','کاربران با مشخصات وارد شده موجود نمی باشد!');
			}
		}

		if ($sendType == "group"){ 

			$groupSelectId = json_decode(Input::get('groupSelectId'));
			return $groupSelectId;

			//$serviceID = 
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' => $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}

			$groupUserId = null;
			foreach ($groupSelectId as $groupid) {
				$groupUserId = DB::table('tbusersgroups')->where('groupid','=',$groupid)->lists('userid');
				foreach ($groupUserId as $key => $id) {
					$matchAnsId = DB::table('tbmatchanswers')->insertGetId(array(
						'matchid' => $matchID,
						'userid' => $id
					));
				}
			}

			if ($groupUserId) {
				return Redirect::to('/opinions/create')->with('Msg','نظرسنجی با موفقیت ثبت و به کاربران گروه ارسال شد');
			} else {
				return Redirect::to('/opinions/create')->with('Error','گروه انتخوابی فاقد کاربر می باشد!');
			}
		}
		if($sendType == 'center')
		{
			//dd($messengerid);
			//dd($);
			$opinionID = TBopinion::insertGetId(array(

				'title' => Input::get('opinion_title'),
				'question' => Input::get('opinion_que'),
				'answerRegex' => Input::has('radioans') ? Input::get('radioans') : null,
				'answersJSON' => $options,
				'startDate' => Input::get('extraStart'),
				'endDate' => Input::get('extraEnd'),
				'desc' => Input::get('opinion_desc'),
				'matchOrOpinion' => '1',
				'centerid' => Session::get('centerid'),
				'serviceACCid' => null,
				'messengerid' => $messengerid,
				'backMSG' => Input::get('backmsg')
			));
			if ($opinionID == null) {
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			}
			if(!$this->fillCenterFilter($opinionID))
				return Redirect::to('/opinions/create')->with('Error','خطا در بدقراری ارتباط با پایگاه داده !');
			$count=TBopinionans::where('matchid','=',$opinionID)->get();
			$count=count($count);
			return Redirect::to('/opinions/create')->with('Msg','نظرسنجی با موفقیت ثبت و به تعداد '.$count.' ارسال شد');
		}
	}

	private function fillCenterFilter($param)
	{
		$centerid=Input::get('ad-center');
		$center=TbAdminCenters::find($centerid);
		if(!$center)
			return false;
		$serviceid=Input::get('ad-service');
		$service=TbAdminServices::find($serviceid);
		if(!$service)
			return false;
		$sub=Input::get('ad-subservice');
		//$subId=TBsubservices::whereIn('id',$sub)->lists('id');
		//dd($subId);
		$list=TBsubservicesusers::whereIn('subserviceid',$sub)->lists('userid');
		//dd($users);
		if(count($list)==0)
			return false;
		$users=TbCentersUsers::whereIn('id',$list)->get();
		if(count($users)==0)
			return false;
		//$param->save();
		foreach ($users as $usr) {
			$matchAnsId = DB::table('tbmatchanswers')->insertGetId(array(
						'matchid' => $param,
						'userid' => $usr->id
					));
		}
		return true;
	}

	public function edit()
	{
		
	}

	public function view()
	{
		$selectOpinion = Input::get('selectOpinion');
		
		return $selectOpinion;
	}

	public function end()
	{
		$opinionAns = TBopinion::join('tbmatchanswers', 'tbmatchoropinion.id','=','tbmatchanswers.matchid')
        	->where('tbmatchoropinion.centerid','=',Session::get('centerid'))
        	->where('tbmatchoropinion.matchOrOpinion','=', 1 )
        	->where('finish','!=',1)
        	->get();

		return View::make('opinion.end', array('opinionAns' => $opinionAns));
	}

	public function finish()
	{
		$opinionFinishID = Input::get('finishcheck');
		if ($opinionFinishID) {
			TBopinion::where ('id','=',$opinionFinishID)->update(array (
				'finish' => 1 
				)
			);
			return Redirect::action('opinions@end', array('Msg' => 'اتمام نظرسنجی با موفقیت انجام شد.')); 
		}

	}

}
