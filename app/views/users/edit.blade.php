
@extends('userLayout')

@section('content')
{{--var_dump($user)--}}
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ویرایش کاربر</h1>
			<hr></hr>
			 {{ Form::open(array('url' => '/users/'.$user->id.'/update','class'=>'form-horizontal well')) }}
			 <input name="id" type="hidden" value="{{$user->id}}">
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('fname', 'نام',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('fname',isset($fname)?$fname:$user->fname,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['fname']))
				{{$messages['fname'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('lname', 'نام خانوادگی',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('lname',isset($lname)?$lname:$user->lname,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['lname']))
				{{$messages['lname'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('education', 'تحصیلات',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{-- Form::text('education',isset($education)?$education:$user->education,array('class'=>'form-control')) --}}
			 {{ Form::select('education',array('0'=>'مشخص نیست','1'=>'زیر دیپلم','2'=>'دیپلم','3'=>'فوق دیپلم','4'=>'کارشناسی','5'=>'کارشناسی ارشد','6'=>'دکتری و بالاتر'),$user->education,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['education']))
				{{$messages['education'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('email', 'ایمیل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('email',isset($email)?$email:$user->email,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['email']))
				{{$messages['email'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('phone', 'تلفن',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('phone',isset($phone)?$phone:$user->phone,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['phone']))
				{{$messages['phone'][0]}}
			@endif
		</div>
	</div>
	
	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('sex', 'جنسیت',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('sex',array('0'=>'زن','1'=>'مرد'),$user->sex,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>

	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('single', 'وضعیت تاهل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('single',array('0'=>'مجرد','1'=>'متاهل'),$user->single,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@if(isset($services))
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('services', 'سرویس های کاربر',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   @foreach ($services as $service)
			   	@if($service[2]==1)
	            	<lable> {{$service[1]}} <input tabindex="1" type="checkbox"  name="service[{{$service[0]}}]" id="{{$service[0]}}"  checked="true"></lable>
	          	@else
	          		<lable> {{$service[1]}} <input tabindex="1" type="checkbox" name="service[{{$service[0]}}]" id="{{$service[0]}}"></lable>
	          	@endif
	          @endforeach
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@endif
	@if(isset($softwares))
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('softwares', 'پیام گیر های کاربر',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   @foreach ($softwares as $software)
			   	@if($software[2]==1)
	            	<lable> {{$software[1]}} <input tabindex="1" type="checkbox" name="software[{{$software[0]}}]" id="{{$software[0]}}"  checked="true"></lable>
	            	@else
	            		<lable> {{$software[1]}} <input tabindex="1" type="checkbox" name="software[{{$software[0]}}]" id="{{$software[0]}}"></lable>
	          		@endif  		
	          @endforeach
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@endif
	<p></p> 
   <div class="row">
    <div class="col-sm-3">
      {{ Form::label('birth', 'تاریخ تولد',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
         <input type="text" class="pdate form-control" id="birth" name="birth" value="{{($user->birthdate!=null)?\Miladr\Jalali\jDate::forge($user->birthdate)->format('Y/m/d'):''}}" placeholder="">
        <input type="hidden" id="birthdate" name="birthdate" class="pdate wide" value="{{($user->birthdate!=null)?$user->birthdate:''}}">
    </div>
    <div class="col-sm-5">
      
    </div>
  </div>
  <p></p> 
   <div class="row">
    <div class="col-sm-3">
      {{ Form::label('marriage', 'تاریخ ازدواج',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
         <input type="text" class="pdate form-control" id="marriage" name="marriage" value="{{($user->marriagedate!=null)?\Miladr\Jalali\jDate::forge($user->marriagedate)->format('Y/m/d'):''}}" placeholder="">
        <input type="hidden" id="marriagedate" name="marriagedate" class="pdate wide" value="{{($user->marriagedate!=null)?$user->marriagedate:''}}">
    </div>
    <div class="col-sm-5">
      
    </div>
  </div>
  <p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>

	<a href="{{url('users')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
    var objCalBirth = new AMIB.persianCalendar( 'birth', {
        extraInputID: 'birthdate',
        extraInputFormat: 'YYYY-MM-DD'
      }
    );
    var objCalMarriage = new AMIB.persianCalendar( 'marriage', {
        extraInputID: 'marriagedate',
        extraInputFormat: 'YYYY-MM-DD'
      }
    );
  </script>
@stop