<!DOCTYPE html>
<html>
<head>
<title>ایجاد ربات</title>
<meta name="description" content="An organization chart editor -- edit details and change relationships." />
<!-- Copyright 1998-2015 by Northwoods Software Corporation. -->
<meta charset="UTF-8">

<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
<link href="{{asset('css/sb-admin-rtl.css')}}" rel="stylesheet">
<link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<style type="text/css">
  .rtl {
    direction: RTL;
  }
  .rtl .pull-right {
      float:left !important;
  }
</style>
<script src="{{asset('js/jquery2.1.4.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- Diagram -->
<script src="{{asset('js/diagram/go.js')}}"></script>
<script src="{{asset('js/diagram/goSamples.js')}}"></script>
<script src="{{asset('js/diagram/release/go.js')}}"></script>
<script id="code">
  function init() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
      $(go.Diagram, "myDiagram",  // must name or refer to the DIV HTML element
        {
          initialContentAlignment: go.Spot.Center,
          allowDrop: true,  // must be true to accept drops from the Palette
          "LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
          "LinkRelinked": showLinkLabel,
          "animationManager.duration": 800, // slightly longer than default (600ms) animation
          "undoManager.isEnabled": true  // enable undo & redo
        });

    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function(e) {
      var button = document.getElementById("SaveButton");
      if (button) button.disabled = !myDiagram.isModified;
      var idx = document.title.indexOf("*");
      if (myDiagram.isModified) {
        if (idx < 0) document.title += "*";
      } else {
        if (idx >= 0) document.title = document.title.substr(0, idx);
      }
    });

    // helper definitions for node templates

    function nodeStyle() {
      return [
        // The Node.location comes from the "loc" property of the node data,
        // converted by the Point.parse static method.
        // If the Node.location is changed, it updates the "loc" property of the node data,
        // converting back using the Point.stringify static method.
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        {
          // the Node.location is at the center of each node
          locationSpot: go.Spot.Center,
          //isShadowed: true,
          //shadowColor: "#888",
          // handle mouse enter/leave events to show/hide the ports
          mouseEnter: function (e, obj) { showPorts(obj.part, true); },
          mouseLeave: function (e, obj) { showPorts(obj.part, false); }
        }
      ];
    }

    // Define a function for creating a "port" that is normally transparent.
    // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
    // and where the port is positioned on the node, and the boolean "output" and "input" arguments
    // control whether the user can draw links from or to the port.
    function makePort(name, spot, output, input) {
      // the port is basically just a small circle that has a white stroke when it is made visible
      return $(go.Shape, "Circle",
               {
                  fill: "transparent",
                  stroke: null,  // this is changed to "white" in the showPorts function
                  desiredSize: new go.Size(8, 8),
                  alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                  portId: name,  // declare this object to be a "port"
                  fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                  fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                  cursor: "pointer"  // show a different cursor to indicate potential link point
               });
    }

    // define the Node templates for regular nodes

    var lightText = 'whitesmoke';

    myDiagram.nodeTemplateMap.add("",  // the default category
      $(go.Node, "Spot", nodeStyle(),
        // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
        $(go.Panel, "Auto",
          $(go.Shape, "Rectangle",
            { fill: "#00A9C9", stroke: null },
            new go.Binding("figure", "figure")),
          $(go.TextBlock,
            {
              font: "bold 11pt Helvetica, Arial, sans-serif",
              stroke: lightText,
              margin: 8,
              maxSize: new go.Size(160, NaN),
              wrap: go.TextBlock.WrapFit,
              editable: true
            },
            new go.Binding("text").makeTwoWay())
        ),
        // four named ports, one on each side:
        makePort("T", go.Spot.Top, false, true),
        makePort("L", go.Spot.Left, true, true),
        makePort("R", go.Spot.Right, true, true),
        makePort("B", go.Spot.Bottom, true, false)
      ));

    myDiagram.nodeTemplateMap.add("Start",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(40, 40), fill: "#79C900", stroke: null }),
          $(go.TextBlock, "Start",
            { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
            new go.Binding("text"))
        ),
        // three named ports, one on each side except the top, all output only:
        makePort("L", go.Spot.Left, true, false),
        makePort("R", go.Spot.Right, true, false),
        makePort("B", go.Spot.Bottom, true, false)
      ));

    myDiagram.nodeTemplateMap.add("End",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(40, 40), fill: "#DC3C00", stroke: null }),
          $(go.TextBlock, "End",
            { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
            new go.Binding("text"))
        ),
        // three named ports, one on each side except the bottom, all input only:
        makePort("T", go.Spot.Top, false, true),
        makePort("L", go.Spot.Left, false, true),
        makePort("R", go.Spot.Right, false, true)
      ));

    myDiagram.nodeTemplateMap.add("Comment",
      $(go.Node, "Auto", nodeStyle(),
        $(go.Shape, "File",
          { fill: "#EFFAB4", stroke: null }),
        $(go.TextBlock,
          {
            margin: 5,
            maxSize: new go.Size(200, NaN),
            wrap: go.TextBlock.WrapFit,
            textAlign: "center",
            editable: true,
            font: "bold 12pt Helvetica, Arial, sans-serif",
            stroke: '#454545'
          },
          new go.Binding("text").makeTwoWay())
        // no ports, because no links are allowed to connect with a comment
      ));


    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.AvoidsNodes,
          curve: go.Link.JumpOver,
          corner: 5, toShortLength: 4,
          relinkableFrom: true,
          relinkableTo: true,
          reshapable: true,
          resegmentable: true,
          // mouse-overs subtly highlight links:
          mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
          { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
          { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null, fill: "gray"}),
        $(go.Panel, "Auto",  // the link label, normally not visible
          { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5},
          new go.Binding("visible", "visible").makeTwoWay(),
          $(go.Shape, "RoundedRectangle",  // the label shape
            { fill: "#F8F8F8", stroke: null }),
          $(go.TextBlock, "Yes",  // the label
            {
              textAlign: "center",
              font: "10pt helvetica, arial, sans-serif",
              stroke: "#333333",
              editable: true
            },
            new go.Binding("text", "text").makeTwoWay())
        )
      );

    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    function showLinkLabel(e) {
      var label = e.subject.findObject("LABEL");
      if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    }

    // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
    myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
    myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

    load();  // load an initial diagram from some JSON text

    // initialize the Palette that is on the left side of the page
    myPalette =
      $(go.Palette, "myPalette",  // must name or refer to the DIV HTML element
        {
          "animationManager.duration": 800, // slightly longer than default (600ms) animation
          nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
          model: new go.GraphLinksModel([  // specify the contents of the Palette
            { text: "گره جدید" }
          ])
        });

  }

  // Make all ports on a node visible when the mouse is over the node
  function showPorts(node, show) {
    var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
    node.ports.each(function(port) {
        port.stroke = (show ? "white" : null);
      });
  }


  // Show the diagram's model in JSON format that the user may edit
  function save() {
    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
    myDiagram.isModified = false;
  }
  function load() {
    myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
  }

  // add an SVG rendering of the diagram at the end of this page
  function makeSVG() {
    var svg = myDiagram.makeSvg({
        scale: 0.5
      });
    svg.style.border = "1px solid black";
    obj = document.getElementById("SVGArea");
    obj.appendChild(svg);
    if (obj.children.length > 0) {
      obj.replaceChild(svg, obj.children[0]);
    }
  }
</script>
</head>

<body onload="init()">
      <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="nav navbar-right top-nav">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">فرزان</a>
            </div>
            <ul class="nav navbar-left top-nav">
                <li class="dropdown" style="color : white">
                  <?php 
                    $date = jDate::forge()->format(' %d , %B , %Y');
                    print_r( $date);
                  ?>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> خوش آمدید {{Session::get('centername')}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{url('profile')}}"><i class="fa fa-fw fa-user"></i> پروفایل</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> نامه ها</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> تنظیمات</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{url('logout')}}"><i class="fa fa-fw fa-power-off"></i> خروج</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-ex1-collapse" style="direction:rtl">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="{{url('/panel')}}"><i class="fa fa-fw fa-dashboard"></i><strong> خلاصه وضعیت</strong></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#centerManeger"><i class="fa fa-fw fa-wrench"></i> مدیریت<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="centerManeger" class="collapse">
                            @if(Session::has('admin') && Session::get('admin')==1 && Session::get('adminid')==Session::get('centerid'))
                                @include('adminmenu')
                            @endif
                            <li>
                                <a href={{url('/users')}}>کاربران</a>
                            </li>
                            <li>
                                <a href={{url('/messages')}}>پیام تولد و ازدواج</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href={{url('/bulk/create')}}><i class="fa fa-fw fa-share"></i> ارسال انبوه</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#opinion"><i class="fa fa-fw fa-arrows-v"></i> نظرسنجی <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="opinion" class="collapse">
                            <li>
                                <a class="glyphicon glyphicon-plus-sign" href={{url('/opinions/create')}}> ایجاد </a>
                            </li>
                            <li>
                                <a class="glyphicon glyphicon-ok-sign" href={{url('/opinions/end')}}> اتمام </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#match"><i class="fa fa-fw fa-arrows-v"></i> مسابقه <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="match" class="collapse">
                            <li>
                                <a class="glyphicon glyphicon-plus-sign" href={{url('/match/create')}}> ایجاد </a>
                            </li>
                            <li>
                                <a class="glyphicon glyphicon-ok-sign" href={{url('/match/end')}}> اتمام </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href={{url('/parametrized/create')}}><i class="glyphicon glyphicon-indent-left"></i> ارسال پارامتری</a>
                    </li>
                    <li class="active">
                         <a href="javascript:;" data-toggle="collapse" data-target="#bot"><i class="fa fa-fw fa-arrows-v"></i> ربات<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="bot" class="collapse">
                            <li>
                                <a class="glyphicon glyphicon-plus-sign" href={{url('/bot/create')}}> ایجاد </a>
                            </li>
                            <li>
                                <a class="glyphicon glyphicon-ok-sign" href={{url('/bot/edit')}}> ویرایش</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href={{url('/reports/main')}}><i class="glyphicon glyphicon-list-alt"></i> گزارش گیری</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
          <div class="row rtl">
            <div class="col-lg-12">
              <h1 class="page-header">
                ربات <small>ایجاد ربات جدید</small>
              </h1>
              <ol class="breadcrumb">
                <li class="active">
                  <i class="fa fa-fw fa-share"></i> ربات
                </li>
              </ol>
            </div>
          </div>
          <div style="height:30px;"></div>
          @if (Session::get('Error'))
          <div class="alert alert-danger fade in rtl" >
            <a href="#" class="close pull-right" data-dismiss="alert" aria-label="close">&times;</a>
            <strong > {{Session::get('Error')}} </strong> 
          </div>
          @endif
          @if (Session::get('Msg'))
          <div class="alert alert-success fade in rtl">
            <strong > {{Session::get('Msg')}} </strong> 
              <a href="#" class="close pull-right" data-dismiss="alert" aria-label="close">&times;</a>
          </div>
          @endif
        {{Form::open(array('url' => '/bot/store','class'=>'form-horizontal well'))}}
            <div class="container-fluid">
                @if(Session::has('admin') && Session::get('adminid')!=Session::get('centerid'))
                      <a href="{{url('admin/panel/return')}}" class="btn btn-success" style="margin-right: 3px;">بازگشت به پنل مدیریت</a>
                    <br>
                @endif
              <div class="form-group">
                <div class="col-lg-6">
                  <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">ذخیره تغییرات در پایگاه داده</button>
                  <button class="btn btn-primary btn-md" id="SaveButton" onclick="save()" >ذخیره تغییرات</button>
                  <button type="button" class="btn btn-danger btn-md" onclick="load()">مجدد</button>
                </div>
                <div class="col-lg-6">
                  <div class="panel-group" style="text-align: right">
                    <div class="panel panel-info">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" href="#collapse1">راهنما</a>
                        </h4>
                      </div>
                      <div id="collapse1" class="panel-collapse collapse">
                        <ul class="list-group">
                          <li class="list-group-item">برای ایجاد سوال/جواب جدید روی گره دوبار کلیک کنید</li>
                          <li class="list-group-item">جهت ویرایش سوال/جواب بر روی متن آن دوبار کلیک و یا پس از کلیک روی گره مقدار فیلد باز شده در زیر گراف را تغییر دهید</li>
                          <li class="list-group-item">برای حذف بر روی سوال/پاسخ کلیک و دکمه حذف کیبورد را فشار دهید</li>
                          <li class="list-group-item">جهت طراحی دوباره ربات روی گزینه مجدد کلیک کنید</li>
                          <li class="list-group-item">پس از تکمیل فرآیند طراحی ربات جهت ذخیره آن ابتدا روی دکمه ذخیره تغییرات کلیک و سپس گزینه ذخیره تغییرات در پایگاه داده را انتخاب کنید </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                  <div style="width:100%; white-space:nowrap;">
                    <span style="display: inline-block; vertical-align: top; padding: 5px; width:100px">
                      <div id="myPalette" style="border: solid 1px gray; height: 500px"></div>
                    </span>
                    <span style="display: inline-block; vertical-align: top; padding: 5px; width:80%">
                      <div id="myDiagram" class="form-control" style="background-color: #696969; border: solid 1px black; height: 500px">
                    </span>
                  </div>
                  </div>
                  <div>
                    <div id="propertiesPanel" style="direction:rtl; display: none; background-color: aliceblue; border: solid 1px black">
                      سوال / پاسخ :<input type="text" class="form-control" id="name" onchange="updateData(this.value, 'name')" /></input><br />
                    </div>
                  </div>
                  <div>
                      <textarea id="mySavedModel" name ="jsonbot" style="width:100%;height:1px;visibility: hidden;">
                      { "class": "go.GraphLinksModel",
                        "linkFromPortIdProperty": "fromPort",
                        "linkToPortIdProperty": "toPort",
                        "nodeDataArray": [],
                        "linkDataArray": []
                      }
                    </textarea>
                  </div>
              <!-- Modal -->
              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog"  style="text-align: right">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" >ذخیره ربات</h4>
                    </div>
                    <div class="modal-body">
                      <p>.لطفا نام ربات را وارد و روی ثبت کلیک کنید</p>
                      <input type="text" style="text-align:right;" class="form-control" name="botName" id="botName"/></input>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" >ذخیره</button>
                        {{Form::close()}}
                      <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
</body>

</html>
