<?php
use \Eloquent;
use \Exception;
use \Hash;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class TbCenterUsers extends Eloquent {
	use SoftDeletingTrait;
	//use UserTrait, RemindableTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public $table = 'tbusers';
	//public $timestamps=false;
	protected $dates = ['deleted_at'];
}

?>