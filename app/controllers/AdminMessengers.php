<?php

class AdminMessengers extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $validator;
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('authAdmin');
	}
	public function index()
	{
		$data=TbAdminMessengers::all();
		return View::make('admin/messengers/all')->with('messengers',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin/messengers/create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validate=$this->storeValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$messenger=new TbAdminMessengers;
			$messenger->name=Input::get('name');
			$messenger->enable=Input::get('enable');
			$messenger->save();
			$msgr=TbAdminMessengers::all();
			if(count($msgr)>1)
			{
				$lastid=$msgr->last()->id;
				//echo $lastsrv->id;
				$currentid=$lastid;
				$currentpatt=TbAdminMessengersACC::all();
				$newpatt=array();
				$newpatt[]=$currentid;
				foreach($currentpatt as $patt)
				{
					$newpatt[]=$patt->pattern.','.$currentid;
				}
				foreach($newpatt as $np)
				{
					$add=new TbAdminMessengersACC;
					$add->pattern=$np;
					$add->save();
				}
			}
			else
			{
				$currentid=$msgr->last()->id;
				$add=new TbAdminMessengersACC;
				$add->pattern=$currentid;
				$add->save();
			}
			$ctr=TbAdminMessengers::all();
			return Redirect::to('admin/messengers');
			//return View::make('admin/messengers/all')
					//	->with('messengers',$ctr);
		}
		else
		{
			return View::make('admin/messengers/create')
						->with('name',Input::get('name'))
						->with( 'messages',$messages->getMessages())
						->with('enable',Input::get('enable'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ctr=TbAdminMessengers::find($id);
		//var_dump($ctr);
		if($ctr)
		{
			return View::make('admin/messengers/edit')->with('messenger',$ctr);
		}
		else
		{
			return "bad request";
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$messenger=TbAdminMessengers::find(Input::get('id'));
		if(!$messenger)
		{
			return "Bad request";
		}
		$validate=$this->updateValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$messenger->name=Input::get('name');
			$messenger->enable=Input::get('enable');
			$messenger->save();
			$ctr=TbAdminMessengers::all();
			return Redirect::to('admin/messengers');
			//return View::make('admin/messengers/all')
				//		->with('messengers',$ctr);
		}
		else
		{
			return View::make('admin/messengers/edit')
						->with('name',Input::get('name'))
						->with('messenger',$messenger)
						->with( 'messages',$messages->getMessages())
						->with('enable',Input::get('enable'));
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$center=TbAdminMessengers::find($id);
		if($center)
		{
			$result=TbAdminMessengers::destroy($id);
			return Redirect::to('admin/messengers');
			//return $this->index();
		}
		else
		{
			return "bad request";
		}
	}

	private function storeValidation()
	{
		$role=array('name'=>'required');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private function updateValidation()
	{
		$role=array('name'=>'required');
		$this->validator=\Validator::make(array('name'=>Input::get('name')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
