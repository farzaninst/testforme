<?php
use \BaseController;
use \View;
use \Input;
use \Redirect;
use \Validator;
use \Auth;
use \DB;
use \Session;
use \Hash;
class panel extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$credit = 0;
		$center = TbAdminCenters::
			where('id','=', Session::get('centerid'))->
			get();
		if ($center)
			$center = $center->toArray();
		foreach ($center as $center) {
			$credit = $center['credit'];		}
		
		$bulks = TBbulk::
			where('centerid','=',Session::get('centerid'))->
			get();
		if ($bulks)
			$bulks = $bulks->toArray();
		$match_opinion = TBopinion::
			where('centerid','=',Session::get('centerid'))->
			get();
		if ($match_opinion)
			$match_opinion = $match_opinion->toArray();
		$param = TbParametrizedMSG::join('tbparametrizedrecievers', 'tbparametrizedmsg.id','=','tbparametrizedrecievers.paramid')
	    	->where('tbparametrizedmsg.centerid','=',Session::get('centerid'))
	    	->get();
		if ($param)
			$param = $param->toArray();
		
		$data = array(
			'match_opinion' => $match_opinion,
			'bulks' => $bulks,
			'param' => $param,
			'credit' => $credit
			);
		return View::make('panel')->with($data);
	}

	public function returntoAdminPanel()
	{
		Session::put('centerid',Session::get('adminid'));
		return Redirect::to('admin/panel');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
