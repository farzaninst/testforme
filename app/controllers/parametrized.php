<?php

class Parametrized extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $centerid;
	public $validator;
	public $centername;
	private $paginate=10;
	public function __construct()
	{
		parent::__construct();
		$this->centerid=Session::get('centerid');
		$this->centername=Session::get('centername');
	}

	public function createFromBank()
	{
		Paginator::setPageName('global');
		$globalmsg=TbMessageBank::where('centerid','=',null)->paginate($this->paginate);
		Paginator::setPageName('local');
		$localmsg=TbMessageBank::where('centerid','=',$this->centerid)->paginate($this->paginate);
		
		return View::make('parametrized.selectmsg')
					->with('localmsg',$localmsg)
					->with('globalmsg',$globalmsg);
	}
	public function selectFromBank($id)
	{
		$msg=TbMessageBank::where('id','=',$id)
				->whereNested(function($q){
					$q->where('centerid','=',$this->centerid);
					$q->orWhere('centerid','=',null);
				})->get();
		if(count($msg)==0)
			return "bad request";
		$msg=$msg[0];
		return Redirect::to('parametrized/create')->with('title',$msg->title)->with('msgText',$msg->text);
	}
	public function searchLocal()
	{
		if(!Input::has('search'))
			return $this->createFromBank();
		$searchtxt=Input::get('search');
		//var_dump($searchtxt);
		if(trim($searchtxt)=="")
			return $this->createFromBank('لطفا متنی را برای جست و جو وارد کنید.',null);
		$data=TbMessageBank::where('centerid','=',$this->centerid)
				->whereNested(function($q) use($searchtxt){
					$q->where('title','LIKE','%'.$searchtxt.'%');
					$q->orWhere('text','LIKE','%'.$searchtxt.'%');
				})->get();
		return View::make('parametrized.searchselectmsg')
					->with('data',$data);
	}
	public function searchGlobal()
	{
		if(!Input::has('search'))
			return $this->createFromBank();
		$searchtxt=Input::get('search');
		//var_dump($searchtxt);
		if(trim($searchtxt)=="")
			return $this->createFromBank('لطفا متنی را برای جست و جو وارد کنید.',null);
		$data=TbMessageBank::where('centerid','=',null)
				->whereNested(function($q) use($searchtxt){
					$q->where('title','LIKE','%'.$searchtxt.'%');
					$q->orWhere('text','LIKE','%'.$searchtxt.'%');
				})->get();
		return View::make('parametrized.searchselectmsg')
					->with('data',$data);
	}
	public function index()
	{
		$data=TbParametrizedMSG::where('centerid','=',Session::get('centerid'))->get();
		$modes= array('1'=>'یکبار','2' =>'روزانه','3'=>'هفتگی','4'=>'ماهانه','5'=>'سالانه' );
		return View::make('parametrized.all')->with('data',$data)->with('modes',$modes);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($storemessage=null,$messages=null)
	{
		//dd($storemessage);
		if(Session::has('storemessage'))
			$storemessage=Session::pull('storemessage');
		if(Session::has('messages'))
			$messages=Session::pull('messages');
		$matchACC = TbAdminCenters::where('id','=',Session::get('centerid'))->pluck('matchservice');
		if($matchACC == 1) {
			//$services=TbAdminServices::where('centerid','=',$this->centerid);
			//var_dump($services);
			$center=TbAdminCenters::find($this->centerid);
			$pattern=TbAdminServicesACC::where('id','=',$center->serviceACCid)->pluck('pattern');
			$services=TbAdminServices::all();
			if($pattern)
			{
				$pattid=explode(',',$pattern);
				$srv=$services->filter(function($item) use($pattid)
					{
						if(!$item->enable)
							return false;
						if(in_array($item->id,$pattid))
							return true;
					});
			}
			else
				$srv=null;
			//dd($srv->toArray());
		/*	foreach ($srv as $s) {
				echo $s->id.'='.$s->name.'<br>';
			}*/
			//list of enabled messenger

			$msgr=TbAdminMessengers::where('enable','=',true)->get();
			$admindata=array();
			if(Session::has('admin') && Session::get('admin')==1 )
			{
				$adcenters=TbAdminCenters::all();
				$adservices=TbAdminServices::all();
				$adsub=TBsubservices::all();
				if(count($adservices)==0)
					$adservices=null;
				if(count($adcenters)>0)
				{
					$i=0;
					foreach ($adcenters as $ac) {
						$admindata[$i]=array();
						$admindata[$i]['id']=$ac->id;
						$admindata[$i]['name']=$ac->name;
						$admindata[$i]['username']=$ac->username;
						$acc=$ac->serviceACCid;
						if(!$acc || count($adservices)==0)
						{
							$admindata[$i]['service']=null;
							$i++;
							continue;
						}
						$patt=TbAdminServicesACC::where('id','=',$acc)->pluck('pattern');
						$patt=explode(',',$patt);
					//	var_dump($patt);
						$srv=$adservices->filter(function($item) use($patt)
							{
								if(in_array($item->id, $patt))
									return true;
							});
					//	var_dump($srv->toArray());
						if(count($srv)==0)
						{
							$admindata[$i]['service']=null;
							$i++;
							continue;
						}
						//dd($srv->toArray());
						$j=0;
						foreach ($srv as $s) {
							$admindata[$i]['service'][$j]['id']=$s->id;
							$admindata[$i]['service'][$j]['name']=$s->name;
							$sub=$adsub->filter(function($item) use($ac,$s)
										{
											if($item->centerid==$ac->id && $item->serviceid==$s->id)
												return true;
										});
							$k=0;
							//var_dump($sub->toArray());
							foreach ($sub as $key) {
								$admindata[$i]['service'][$j]['sub'][$k]['name']=$key->name;
								$admindata[$i]['service'][$j]['sub'][$k]['id']=$key->id;
								$k++;
							}
							$j++;
						}
						$i++;
						//dd($admindata[0]['service']);
					}
				}
				//var_dump('dd====');
				//var_dump($admindata[0]['service']['sub']);
			//	var_dump($admindata[1]['service']['sub']);
				//var_dump($admindata[2]['service']['sub']);
				/*foreach ($admindata as $key) {
					if(isset($key['service']['sub']))
					var_dump($key['service']['sub']);
				}*/
				//var_dump($admindata);
			//	dd();
			}
		//	return json_encode($admindata);
			//var_dump(json_encode($admindata));
			//var_dump($admindata[0]['username']);
			//var_dump($admindata[0]['service'][1]['name']);
			//var_dump($admindata[0]['service'][1]['sub'][1]['name']);
			//var_export($admindata);
		//	dd();
			$modes= array('1'=>'یکبار','2' =>'روزانه','3'=>'هفتگی','4'=>'ماهانه','5'=>'سالانه' );
			$centerGroups=TbCenterGroups::where('centerid','=',$this->centerid)->get();
			$subService = TBsubservices::where ('centerid','=',Session::get('centerid'))->get();
			$services=TbAdminServices::all();
			if($pattern)
			{
				$pattid=explode(',',$pattern);
				$srv=$services->filter(function($item) use($pattid)
					{
						if(!$item->enable)
							return false;
						if(in_array($item->id,$pattid))
							return true;
					});
			}
			else
				$srv=null;
			if(Session::has('title'))
				$title=Session::pull('title');
			else
				$title=null;
			if(Session::has('msgText'))
				$msgText=Session::pull('msgText');
			else
				$msgText=null;
			//dd($subService);
			return View::make('parametrized.create')
						->with('services',$srv)
						->with('subService',$subService)
						->with('messengers',$msgr)
						->with('centerGroups',$centerGroups)
						->with('storemessage',$storemessage)
						->with('messages',$messages)
						->with('modes',$modes)
						->with('msgText',$msgText)
						->with('title',$title)
						->with('allServices',$services)
						->with('admindata',$admindata);
		} else {
			return Redirect::to('/panel')->with('accError','شما مجاز به استفاده از سرویس ارسال زماندار نمی باشید !');
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//dd(Input::all());
		//dd(Input::get('msgText'));
		$param=null;
		$validate=$this->storeValidation();
		$messages = $this->validator->messages();
		//var_dump(Input::get('service'));
		if($validate)
		{
			if(Input::has('service'))
				$service_patt_id=$this->setServicePattern(Input::get('service'));
			else
				$service_patt_id=null;
			if(Input::has('software'))
				$software_patt_id=$this->setSoftwarePattern(Input::get('software'));
			else
				$software_patt_id=null;
			//dd($software_patt_id);

			$param=new TbParametrizedMSG;
			//dd($this->centerid);
			$param->centerid=$this->centerid;
			$param->serviceACCid=$service_patt_id;
			$param->messengerid=$software_patt_id;
			$param->sendDate=Input::get('sendDate');
			$param->msgText=Input::get('msgText');
			$param->createDate=date("Y-m-d");
			$param->title=Input::get('title');
			$param->mode=Input::get('mode');
			$param->finish=false;
			//$param->save();
		}
		else
		{
			Session::put('title',Input::get('title'));
			Session::put('msgText',Input::get('msgText'));
			$storemessage['error']="لطفا فیلدها را به دقت پر کنید";
			return $this->create($storemessage,$messages->getMessages());
			//dd($messages);
		}
		$sendType = Input::get('radiousers');

		if ($sendType == 'allSite') 
		{  //All users add
			if(!$this->fillAllSite($param))
			{
				$storemessage['error']="شما هیچ کاربری ندارید لطفا ابتدا تعدادی کاربر اضافه کنید";
				return $this->create($storemessage);
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);
		}
		elseif ($sendType == 'allService') 
		{  //All users add
			if(!$this->fillAllService($param))
			{
				$storemessage['error']="شما هیچ کاربری ندارید لطفا ابتدا تعدادی کاربر اضافه کنید";
				return $this->create($storemessage);
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);
		}
		elseif ($sendType == "subService")
		{
			$usersID = TBsubservicesusers::where ('subserviceid','=',Input::get('selectSubService'))->lists('userid');
			$users=TbCenterUsers::whereIn('id',$usersID)->get();
			if(count($users)==0)
			{
				$storemessage['error']="تعداد کاربران فیلتر 0 میباشد. پیام ارسال نشد";
				return $this->create($storemessage);
			}
			$param->save();
			foreach ($users as $usr) {
				$reciever=new TbParametrizedRecievers;
				$reciever->paramid=$param->id;
				$reciever->userid=$usr->id;
				$reciever->send=false;
				$reciever->phone=$usr->phone;
				$reciever->save();
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);
		}
		else if($sendType == 'group')
		{
			if(!$this->fillGroup($param))
			{
				$storemessage['error']="کاربری در گروه های انتخابی موجود نیست";
				return $this->create($storemessage);
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);

		}
		else if($sendType == 'userFilter')
		{
			if(!$this->fillFilter($param))
			{
				$storemessage['error']="کاربری مطابق با فیلترهای انتخابی وجود ندارد";
				return $this->create($storemessage);
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);
		}
		else if($sendType == 'userFilterService')
		{
			if(!$this->fillServiceFilter($param))
			{
				$storemessage['error']="کاربری مطابق با فیلترهای انتخابی وجود ندارد";
				return $this->create($storemessage);
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);
		}
		else if($sendType == 'center')
		{
			if(!$this->fillCenterFilter($param))
			{
				$storemessage['error']="کاربری مطابق با فیلترهای انتخابی وجود ندارد";
				return $this->create($storemessage);
			}
			$count=TbParametrizedRecievers::where('paramid','=',$param->id)->count();
			$storemessage['success']="پیام شما با موفقیت ثبت شد. تعداد کاربران انتخاب شده برای ارسال ".$count.' کاربر است';
			return Redirect::action('Parametrized@create')->with('storemessage',$storemessage);
			//return $this->create($storemessage);
		}
		else
		{
			return "bad request";
		}

	}
	private function setServicePattern($input)
	{
		//dd($input);
		$services=TbAdminServices::all();
		$patt='';
		foreach ($services as $srv) 
		{
			if(in_array($srv->id,$input))
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminServicesACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
					return $acc->id;
			}
			return null;
		}
	}
	private function setSoftwarePattern($input)
	{
		//dd($input);
		$services=TbAdminMessengers::all();
		$patt='';
		foreach ($services as $srv) 
		{
			if(in_array($srv->id,$input))
				$patt.=$srv->id.',';
		}
		if($patt=='')
			return null;
		else
		{
			$patt=substr($patt,0,strlen($patt)-1);
			$pattacc=TbAdminMessengersACC::all();
			foreach ($pattacc as $acc) 
			{
				if($acc->pattern==$patt)
					return $acc->id;
			}
			return null;
		}
	}
	private function storeValidation()
	{
		$role=array('title'=>'required|min:3',
					'msgText'=>'required|min:3',
					'sendDate'=>'required|date');
		$this->validator=\Validator::make(
			array('title'=>Input::get('title'),
				'msgText'=>Input::get('msgText'),
				'sendDate'=>Input::get('sendDate')),$role);

		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//********************************************************************/
	private function fillAllSite($param)
	{
		$users=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)->get();
		if(count($users)==0)
		{
			return false;
		}
		$param->save();
		foreach ($users as $usr) {
			$reciever=new TbParametrizedRecievers;
			$reciever->paramid=$param->id;
			$reciever->userid=$usr->id;
			$reciever->send=false;
			$reciever->phone=$usr->phone;
			$reciever->save();
		}
		return true;
	}
	private function fillAllService($param)
	{
		$srv=Input::get('service');
		//dd($srv);
		$users=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)->get();

		if(count($users)==0)
			return false;
		$users=$users->filter(function($item) use($srv)
					{
						$patt=TbAdminServicesACC::find($item->userServiceACCid);
						if(!$patt)
							return false;
						$patt=$patt->pattern;
						$patt=explode(',',$patt);
						foreach ($srv as $s) {
							if(in_array($s,$patt))
								return true;
						}
					});
		//dd($users->toArray());
		if(count($users)==0)
		{
			return false;
		}
		$param->save();
		foreach ($users as $usr) {
			$reciever=new TbParametrizedRecievers;
			$reciever->paramid=$param->id;
			$reciever->userid=$usr->id;
			$reciever->send=false;
			$reciever->phone=$usr->phone;
			$reciever->save();
		}
		return true;
	}
	private function fillGroup($param)
	{
		$gr=Input::get('groupSelectId');
		$gr=json_decode($gr,true);
		$usersid=TbUsersGroups::whereIn('groupid',$gr)->lists('userid');
		$usersid=array_unique($usersid);
		$users=TbCenterUsers::where('centerid','=',$this->centerid)->whereIn('id',$usersid)->get();
		//dd(count($users));
		if(count($users)==0)
		{
			return false;
		}
		$param->save();
		foreach ($users as $usr) {
			$reciever=new TbParametrizedRecievers;
			$reciever->paramid=$param->id;
			$reciever->userid=$usr->id;
			$reciever->send=false;
			$reciever->phone=$usr->phone;
			$reciever->save();
		}
		return true;
	}

	private function fillFilter($param)
	{
		//dd(Input::all());
		$users=array();
		if(Input::has('usersfilter')){
			$userfilter=Input::get('usersfilter');
			if(in_array('name',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)
						->where('fname','=',Input::get('usersFilterName'))
						->get(array('id','phone'))->toArray();
			}
			//dd($users);
			if(in_array('age',$userfilter))
			{

				$startAge = Input::get('usersFiltersStartAge');
				$endAge = Input::get('usersFiltersEndAge') ;
				$mindate=date('Y-m-d', strtotime('-'.$endAge.' years'));
				$maxdate=date('Y-m-d', strtotime('-'.$startAge.' years'));
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)
						->where('birthdate','>',$mindate)
						->where('birthdate','<',$maxdate)
						->get(array('id','phone'))->toArray();
			}
			if(in_array('sex',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)
						->where('sex','=',Input::get('usersFilterSex'))
						->get(array('id','phone'))->toArray();
			}
			if(in_array('phone',$userfilter) && Input::get('usersFilterPhone')!='')
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)
						->where('phone','LIKE',Input::get('usersFilterPhone').'%')
						->get(array('id','phone'))->toArray();
			}

			if(in_array('birthdate',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)
						->where('birthdate','=',Input::get('extraBirthdate'))
						->get(array('id','phone'))->toArray();
			}
			if(in_array('marriagedate',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',0)
						->where('marriagedate','=',Input::get('extraMarriagedate'))
						->get(array('id','phone'))->toArray();
			}
			$list=array();
			if(count($users)==0)
			{
				return false;
			}
			$usr=$users[0];
			$exit=true;
			for($i=0;$i<count($usr);$i++)
			{
				$exit=true;
				for($j=1;$j<count($users);$j++)
				{
					$boo=false;
					for($k=0;$k<count($users[$j]);$k++)
					{
						if($usr[$i]['id']==$users[$j][$k]['id'])
						{
							$boo=true;
							break;
						}
					}
					$exit=$exit&&$boo;
					if(!$exit)
						break;
				}
				if($exit)
				{
					$list[]=['id'=>$usr[$i]['id'],'phone'=>$usr[$i]['phone']];
				}
			}
			if(count($list)==0)
			{
				return false;
			}
			$param->save();
			foreach ($list as $l) {
				$reciever=new TbParametrizedRecievers;
				$reciever->paramid=$param->id;
				$reciever->userid=$l['id'];
				$reciever->send=false;
				$reciever->phone=$l['phone'];
				$reciever->save();
			}
			return true;
		}
	}
	private function fillServiceFilter($param)
	{
		//dd(Input::all());
		if(Input::has('filterservice'))
			$serv=Input::get('filterservice');
		else
			$serv=0;
		$users=array();

		if(Input::has('userservicefilter')){
			$userfilter=Input::get('userservicefilter');
			if(in_array('name',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)
						->where('fname','=',Input::get('userserviceFilterName'))
						->get(array('id','phone'))->toArray();
			}
			//dd($users);

			if(in_array('age',$userfilter))
			{

				$startAge = Input::get('userserviceFiltersStartAge');
				$endAge = Input::get('userserviceFiltersEndAge') ;
				$mindate=date('Y-m-d', strtotime('-'.$endAge.' years'));
				$maxdate=date('Y-m-d', strtotime('-'.$startAge.' years'));
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)
						->where('birthdate','>',$mindate)
						->where('birthdate','<',$maxdate)
						->get(array('id','phone'))->toArray();
			}
			if(in_array('sex',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)
						->where('sex','=',Input::get('userserviceFilterSex'))
						->get(array('id','phone'))->toArray();
			}
			if(in_array('phone',$userfilter) && Input::get('userserviceFilterPhone')!='')
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)
						->where('phone','LIKE',Input::get('userserviceFilterPhone').'%')
						->get(array('id','phone'))->toArray();
			}

			if(in_array('birthdate',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)
						->where('birthdate','=',Input::get('extraBirthdate'))
						->get(array('id','phone'))->toArray();
			}
			if(in_array('marriagedate',$userfilter))
			{
				$users[]=TbCenterUsers::where('centerid','=',$this->centerid)->where('type','=',1)
						->where('marriagedate','=',Input::get('extraMarriagedate'))
						->get(array('id','phone'))->toArray();
			}
			$list=array();
			if(count($users)==0)
			{
				return false;
			}
			$usr=$users[0];
			$exit=true;
			for($i=0;$i<count($usr);$i++)
			{
				$exit=true;
				for($j=1;$j<count($users);$j++)
				{
					$boo=false;
					for($k=0;$k<count($users[$j]);$k++)
					{
						if($usr[$i]['id']==$users[$j][$k]['id'])
						{
							$boo=true;
							break;
						}
					}
					$exit=$exit&&$boo;
					if(!$exit)
						break;
				}
				if($exit)
				{
					$list[]=['id'=>$usr[$i]['id'],'phone'=>$usr[$i]['phone']];
				}
			}
			if(count($list)==0)
			{
				return false;
			}
			$list2=array();

			foreach($list as $l)
			{
				$acc=TbCenterUsers::where('centerid','=',$this->centerid)->where('id','=',$l['id'])->pluck('userServiceACCid');
				if($acc)
				{
					$patt=TbAdminServicesACC::find($acc);
					if($patt)
					{
						$patt=$patt->pattern;
						$patt=explode(',', $patt);
						if(in_array($l['id'], $patt))
							$list2[]=$l;
					}
				}
			}
			$list=$list2;
			//dd($list);
			//dd(Input::all());
			if(count($list)==0)
			{
				return false;
			}
			$param->save();
			foreach ($list as $l) {
				$reciever=new TbParametrizedRecievers;
				$reciever->paramid=$param->id;
				$reciever->userid=$l['id'];
				$reciever->send=false;
				$reciever->phone=$l['phone'];
				$reciever->save();
			}
			return true;
		}
	}
	private function fillCenterFilter($param)
	{
		$centerid=Input::get('ad-center');
		$center=TbAdminCenters::find($centerid);
		if(!$center)
			return false;
		$serviceid=Input::get('ad-service');
		$service=TbAdminServices::find($serviceid);
		if(!$service)
			return false;
		$sub=Input::get('ad-subservice');
		//$subId=TBsubservices::whereIn('id',$sub)->lists('id');
		//dd($subId);
		$list=TBsubservicesusers::whereIn('subserviceid',$sub)->lists('userid');
		//dd($users);
		if(count($list)==0)
			return false;
		$users=TbCentersUsers::whereIn('id',$list)->get();
		if(count($users)==0)
			return false;
		$param->save();
		foreach ($users as $usr) {
			$reciever=new TbParametrizedRecievers;
			$reciever->paramid=$param->id;
			$reciever->userid=$usr->id;
			$reciever->send=false;
			$reciever->phone=$usr->phone;
			$reciever->save();
		}
		return true;
	}
	//*********************************************************************/

}
