<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$centername = DB::table('tbcenters')->where ('id','=',Session::get('centerid'))->pluck('name');
		$logo=DB::table('tbcenters')->where ('id','=',Session::get('centerid'))->pluck('logo');
		$adminlogo=DB::table('tbcenters')->where ('admin','=',1)->pluck('logo');
		if($logo!=null)
			View::share('logo',$logo);
		else
			View::share('logo',$adminlogo);
		Session::put('centername', $centername);
        //View::share('centername', $centername);
	}


	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
