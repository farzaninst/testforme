@extends('userLayout')

@section('header')
  <h1> ROBOT </h1>

@stop

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم ربات <small>ایجاد ربات</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-bold"></i> ربات
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <hr></hr>
      <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            برای حذف یک گزینه، مقدار فیلد گزینه را پاک کنید
      </div>
      @if(isset($successmsg))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$successmsg}}
        </div>
      @endif
      @if(isset($errormsg))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$errormsg}}
        </div>
      @endif
    {{Form::open(array('url'=>'/bot/'.$bot->id.'/editTree/'.$question->id,'class'=>'form-horizontal well',  'method' => 'post'))}}
      <input type="hidden" name="botid" value="{{$bot->id}}"></input>
      <input type="hidden" name="questionid" value="{{$question->id}}"></input>
      <div class="row">
        <div class="col-sm-2">
          {{ Form::label('question', 'پرسش',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-7">
             {{ Form::text('question',$question->text,array('class'=>'form-control')) }}
        </div>
      </div>
      <p></p>
      @if(isset($options))
        <?php $counter=1; ?>
        @foreach($options as $opt)
          <div class="row" style="background-color:#D9EDF7;min-height: 50px;padding: 10px;">
            <div class="col-sm-1">
              <label for="option[{{$opt['id']}}]" class="control-label"> گزینه {{$counter}} </label>
            </div>
            <div class="col-sm-2">
                 <input class="form-control" name="option[{{$opt['id']}}]" type="text" value="{{$opt['title']}}" id="option">
            </div>
            <div class="col-sm-1">
                 <label for="subquestion[]" class="control-label">سوال بعدی </label>
            </div>
            <div class="col-sm-4">
                 <select class="form-control" id="subquestion-{{$counter}}" name="subquestion[]"><option value="{{0}}">ندارد</option>
                  @if(isset($opt['next'])&& is_array($opt['next']))
                      <option value="{{$opt['next'][0]}}" selected>{{$opt['next'][1]}}</option>
                  @endif
                  @foreach($arrque as $que)
                    @if(isset($opt['next']))
                      @if($que[0]!=$opt['next'][0])
                        <option value="{{$que[0]}}">{{$que[1]}}</option>
                      @endif
                    @else
                      <option value="{{$que[0]}}">{{$que[1]}}</option>
                    @endif
                  @endforeach
                </select>
            </div>
            <div class="col-sm-1">
              <label for="answer[]" class="control-label">جواب </label>
            </div>
             <div class="col-sm-3">
              <input class="form-control" name="answer[]" type="text" value="{{$opt['answer']}}" placeholder="جواب در صورتی که گزینه سوال بعدی ندارد" id="answer-{{$counter++}}">
            </div>
          </div>
          <p></p>
        @endforeach
      @endif
      <div id="questiondiv">
      </div>
      {{------------------------------------------------------------------}}
        
      {{------------------------------------------------------------------}}
      <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-6">
        </div>
        <div class="col-sm-2">
          <a type='button' class="btn btn-success" id='addButton'>گزینه جدید</a>
        </div>
      </div>
      <p></p>
    <div class="row">
      <div class="col-sm-2"></div>
      <div class="col-sm-2">
         {{-- Form submit button. --------------------}}
         {{ Form::submit('ویرایش سوال',array('class'=>'form-control btn btn-primary','id'=>'submit','name'=>'submit')) }}
      </div>
    </div>
  {{Form::close()}}
  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a target="_blank" href="{{url('/bot/'.$bot->id.'/show')}}" class="btn btn-success">مشاهده ساختار درختی ربات</a>
    <a href="{{url('/bot/'.$bot->id.'/editTree')}}" type='button' class="btn btn-success" >بازگشت</a>
  </div>
</div>
  </div>
</div>
@if(isset($arrque))
  <script type="text/javascript">
  var allque={{json_encode($arrque)}};
  var questionid={{$question->id}};
  var questiontext="{{$question->text}}";
  var usedque=[];
  //var usedsubque[];
  var current=0;
  $(document).ready(function(){
      var counter = {{$counter}};
      $("#addButton").click(function () {
        
        var sel='<div class="row" style="background-color:#DFF0D8;min-height: 50px;padding: 10px;"><div class="col-sm-1"><label for="newoption[]" class="control-label">گزینه '+counter+') </label></div><div class="col-sm-2"><input class="form-control" name="newoption[]" type="text" value="" id="option"></div><div class="col-sm-1"><label for="subquestion[]" class="control-label">سوال بعدی </label></div><div class="col-sm-4"><select class="form-control" id="subquestion-'+counter+'" name="newsubquestion[]"><option value="{{0}}">ندارد</option>';
        var select=document.createElement("SELECT");
        select.setAttribute("id", "subquestion-"+counter);
        select.setAttribute("name", "subquestion[]");
        //var selid="#subquestion-"+counter;     
        select.setAttribute("class","form-control");
       /* $("#subquestion1").on("change",function()
                {
                  var value=$(this).val();
                  alert(value);
                });*/
        for(var j=0; j<usedque.length;j++)
          console.log(usedque[j]);
        for(var i=0; i< allque.length ;i++)
        {
          var x= allque[i][0];
          var bool=false;
          for(var j=0; j<usedque.length;j++)
            if(usedque[j]==allque[i][0])
            {
              //console.log(j);
              bool=true;
            }
              
          if(!bool)
          {
            var id=allque[i][0];
            var q=allque[i][1];
            sel=sel+'<option value="'+id+'">'+q+'</option>';
          }
          
          //alert(sel);
        }
        sel+='</select></div>';
        sel+='<div class="col-sm-1"><label for="newanswer[]" class="control-label">جواب </label></div>';
        sel+='<div class="col-sm-3">'
        sel+='<input class="form-control" name="newanswer[]" type="text" placeholder="جواب در صورتی که گزینه سوال بعدی ندارد" id="answer-'+counter+'"></div>';
        sel+='</div><p></p>';
            $('#questiondiv').append(sel);       
            counter++;
    });

    $("#submit").click(function () 
    {
      var sub=$("[name='subquestion']");
      for (var i = 0; i < sub.length; i++) {
        alert($(sub[i]).val());
      };
      //return false;
    });
    
  });
  </script>
@endif
@stop