<?php

class Messages extends \BaseController {

	private $centerid;
	public function __construct()
	{
    	parent::__construct();
    	$this->centerid=Session::get('centerid');
	}
	public function index($msg=null)
	{
		$center=TbAdminCenters::find($this->centerid);
		return View::make('messages/all')
					->with('birthMessage',$center->birthMessage)
					->with('marriageMessage',$center->marriageMessage)
					->with('msg',$msg);
	}

	public function storeBirth()
	{
		$msg=Input::get('birthMessage');
		$center=TbAdminCenters::find($this->centerid);
		if($center)
		{
			$center->birthMessage=$msg;
			$center->save();
			return $this->index('پیام تولد با موفقیت ویرایش شد.');
		}
		return Redirect::to('login');

	}

	public function storeMarriage()
	{
		$msg=Input::get('marriageMessage');
		$center=TbAdminCenters::find($this->centerid);
		if($center)
		{
			$center->marriageMessage=$msg;
			$center->save();
			return $this->index('پیام ازدواج با موفقیت ویرایش شد.');
		}
		return Redirect::to('login');
	}
}
