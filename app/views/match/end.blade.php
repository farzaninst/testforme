@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      انتخاب مسابقه <small>اتمام مسابقه</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-ok-sign"></i> مسابقه
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    @if ($errors->has('finishcheck')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('finishcheck') }} </strong> 
      </div>
    @endif
    @if (Session::get('Msg'))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{Session::get('Msg')}} </strong> 
      </div>
    @endif

    {{Form::open(array('url' => 'match/finish','class'=>'form-horizontal well'))}}
      <div class="form-group">
        <div class="col-lg-4">
          <label> مسابقه : </label>
          <select class="form-control" id="selectMatch" name="selectMatch">
              <option value = 0>لطفا مسابقه مورد نظر را انتخاب کنید ...</option>
              <?php
                foreach ($matchs as $match) {
                  echo "<option  class='option' value='" . $match['id'] ."'>". $match['title'] ."</option>"; 
                }
              ?>
          </select>
        </div>
        <div class="col-lg-4">
        </div>  
        <div class="col-lg-4">
        </div>
      </div>

      <div class="form-group">
        <div class="col-lg-6">
          <input type="hidden" id="groupSelectId" name="groupSelectId">
          <div id = "divgroup">
            <table class="table table-bordered" id="grouptable" name="grouptable" >
              <thead>
                <tr>
                  <th>عنوان مسابقه</th>
                  <th>کاربران شرکت داده شده</th>
                  <th>تعداد پاسخ ها</th>
                  <th>پاسخ صحیح</th>
                  <th>اتمام</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6">
          
        </div>
       </div>
    {{Form::close()}}
  </div>
</div>

<script type="text/javascript">

var groupid = [];
var select = [];

$('#selectMatch').on('change', function() {
  var bool=false;
  var value = $(this).val();
  var ansCount = 0 ;
  var userCount = 0 ;
  var matchName = "";
  var regex = "";
  var options;
  for (var i = 0 ; i < select.length ; i++) {
    if (select[i] == value) {
      bool = true ;
      break;
    }
  }
  if (value == 0) {bool=true;};
  if (!bool) {
  @foreach ($matchs as $match)
    if (value == {{$match['id']}}) {
      matchName = "{{$match['title']}}";
      regex = "{{$match['answerRegex']}}";
      if (regex == "[text]") {
        <?php
          $options = explode("---", $match['answersJSON']);
          //$optioncount = count($options);
        ?>
        var options = [];
        
      }
    }
  @endforeach
  @foreach ($matchans as $count)
    @foreach ($count as $ans)
      if (value == {{$ans['matchid']}} ) {
        userCount += 1;
        if ("{{$ans['answer']}}" != null){
          ansCount += 1;
        }
        groupid.push(value);
      }
    @endforeach
  @endforeach
  if (regex == "[0-1]") {
    $("#grouptable tbody").append( 
      "<tr>"+
      "<td>"+ matchName +"</td>"+ 
      "<td>"+ userCount +"</td>"+ 
      "<td>"+ ansCount +"</td>"+
      "<td><select name='correctAnswer'><option value='1'>بلی</option><option value='0'>خیر</option></select></td>"+
      "<td><button type='submit' name ='finishbtn' class='btn btn-primary' value='"+value+"'>اتمام</button></td>"+
      "</tr>"
    );
  }
  if (regex == "[text]") {
    $("#grouptable tbody").append( 
      "<tr>"+
      "<td>"+ matchName +"</td>"+ 
      "<td>"+ userCount +"</td>"+ 
      "<td>"+ ansCount +"</td>"+
      "<td><select name='correctAnswer'>@if(isset($options)) @foreach($options as $option)<option value='{{$option}}'>{{$option}}</option>@endforeach @endif</select></td>"+
      "<td><button type='submit' name ='finishbtn' class='btn btn-primary' value='"+value+"'>اتمام</button></td>"+
      "</tr>"
    );
  }
   
  $('#groupSelectId').val(JSON.stringify(groupid));
  select.push(value);
  }
});

</script>

@stop