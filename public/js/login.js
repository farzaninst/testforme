//common script for every page which has text input and datepickers
$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
$("input[type=text]").attr("autocomplete", "off");
$('input[type=text]:first').focus();

//common script for create user and update user
$("#department").blur(function(){
  var department = $.trim(this.value);
  $("#role").val("");
  $("#role").attr("list", department);
});