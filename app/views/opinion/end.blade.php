@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      انتخاب نظرسنجی <small>اتمام نظرسنجی</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-ok-sign"></i> نظرسنجی
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    @if ($errors->has('finishcheck')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('finishcheck') }} </strong> 
      </div>
    @endif
    @if (isset($Msg))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{$Msg}} </strong> 
      </div>
    @endif

    {{Form::open(array('url' => 'opinions/finish','class'=>'form-horizontal well'))}}
      <div class="form-group">
        <div class="col-lg-3">
          <label class="control-label col-sm-5" for=""> نام نظرسنجی : </label>
        </div>
        <div class="col-lg-3">
            <select class="form-control" id="selectOpinion" name="selectOpinion">
              <option value="0">لطفا نظر سنجی مورد نظر را انتخاب کنید ...</option>
              <?php
              $opinionsid = array(); 
                foreach ($opinionAns as $key) {
                  array_push($opinionsid, $key->matchid);  
                  echo "<option  class='option' value='" .$key->matchid ."'>". $key->title ."</option>"; 
                 }
              ?>
            </select>
        </div>
        <div class="col-lg-6"></div>
      </div>

      <div class="form-group">
        <div class="col-lg-6">
          <input type="hidden" id="groupSelectId" name="groupSelectId">
          <div id = "divgroup">
            <table class="table table-bordered" id="grouptable" name="grouptable" >
              <thead>
                <tr>
                  <th>عنوان نظر سنجی</th>
                  <th>کاربران شرکت داده شده</th>
                  <th>تعداد پاسخ ها</th>
                  <th>اتمام</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6">
          
        </div>
       </div>

      <div class="form-group">
        <div class="col-lg-7">
        </div>
        <div class="col-lg-3">
          <button type="submit" name ="submit" class="btn btn-primary">اتمام</button>
        </div>
       </div>
      {{Form::close()}}

  </div>
</div>

<script type="text/javascript">

var child = document.getElementById( 'selectOpinion').getElementsByClassName( 'option' );
var temp =[];
for (var i = 0; i < child.length; i++) {
  temp[i] = child[i] ;
  for (var j = 0; j < temp.length; j++) {
    var bol = false;

    if (temp[j] == child[i]) {
      bol = true;
      break;
    }
  }
  if (bol){
      document.getElementById( 'selectOpinion' ).removeChild(child[i]);  
  }
    
}


var groupid = [];
var select = [];
select.push(0);
  $('#selectOpinion').on('change', function() {

    var value = $(this).val();
    var bool=false;
    var ansCount = 0 ;
    var userCount = 0 ;
    var opinionName = "";
    for (var i = 0 ; i < select.length ; i++) {
      if (select[i] == value) {
        bool = true ;
        break;
      }
    }
  if (!bool) {
    @foreach ($opinionAns as $key)
      if (value == {{$key->matchid}} ) {
        opinionName = "{{$key->title}}";
        userCount += 1;
        if ("{{$key->answer}}" != ""){
          ansCount += 1;  
        }

        groupid.push(value);
      }
     @endforeach
     $("#grouptable tbody").append( 
          "<tr>"+
          "<td>"+ opinionName +"</td>"+ 
          "<td>"+ userCount +"</td>"+ 
          "<td>"+ ansCount +"</td>"+ 
          "<td><input type= 'checkbox' name='finishcheck' value="+ value +"</td>"+ 
          "</tr>");
     
     
    $('#groupSelectId').val(JSON.stringify(groupid));
    select.push(value);
  }
});
</script>
@stop