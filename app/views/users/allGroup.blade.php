@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت کاربران <small>گروه های کاربری</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست گروه های کاربران
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <a href="{{url('users')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
    <a href="{{url('users/groups/create')}}" class="btn btn-success hvr-radial-out-success">گروه جدید</a>
    <p></p>
    @if(isset($messages))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$messages}}
    </div>
    @endif
    <p></p>
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>نام گروه</th>
                    <th>توضیحات</th>
                    <th>کاربران</td>
                    <th style="color:transparent;">........................</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($groups as $ctr)
                <tr>
                    <td>{{ $ctr->name }}</td>
                    <td>{{ $ctr->desc}}</td>
                    <td>
                        <a href="{{url('users/groups/'.$ctr->id.'/users')}}" class="btn btn-success hvr-radial-out-success">مدیریت کاربران</a>
                    </td>  
                    <td>
                        {{ Form::open(['url' => 'users/groups/delete/' . $ctr->id, 'method' => 'POST']) }}
                        {{ Form::submit('حذف', ['class' => 'btn btn-danger pull-right hvr-bounce-to-right-danger'])}}
                        {{ Form::close() }}
                         <a href="{{url('users/groups/'.$ctr->id.'/edit')}}" class="btn btn-info pull-left hvr-radial-out-info" style="margin-right: 3px;">ویرایش</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </div>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop