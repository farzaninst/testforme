<?php

use \View;
use \Input;
use \Hash;
class AdminLogin extends \BaseController {

	/*public function __construct(){
    	parent::__construct();
	}*/
	public function index()
	{
		//dd(Hash::make('admin'));
		//dd(Hash::make('1234567'));
		return View::make ('admin.login');
	}

	public function login()
	{
		$username = Input::get('form-username');
		$password = Input::get('form-password');
		$errmsg;

		if (isset($username) && $username == "")
		{
			 $errmsg = "لطفا نام کاربری را وارد کنید !";
			 return View::make('login')->with ('errmsg',$errmsg);
		}
		if (isset($password) && $password == "")
		{
			 $errmsg = "لطفا رمز عبور را وارد کنید !";
		  	 return View::make('admin.login')->with ('errmsg',$errmsg);
		}
		
		//dd(User::all());
		//dd(DB::table('user')->get());
		if(Auth::attempt(array('username'=>$username,'password'=>$password,'admin'=>1)))
		{ //attempt to login the user
			$centerid = TbAdminCenters::where ('username','=',$username)->pluck('id');
			$serviceaccid =TbAdminCenters::where ('username','=',$username)->pluck('serviceACCid');
			$servicepattern = DB::table('tbservicesacc')->where('id','=',$serviceaccid)->pluck('pattern');
			Session::put('admin',Hash::make('farzanABC123qwe#'));
			Session::put('centerid',$centerid);
			Session::put('servicepattern',$servicepattern);
			return View::make('admin.panel');
			//return Redirect::intended('/admin');
		}
		else
		{
			$errmsg = ".نام کاربری یا رمز عبور اشتباه است";
			return View::make('admin.login')->with ('errmsg',$errmsg);
		}


		//return View::make('admin'); 
	}

	public function logout()
	{
		$id=Auth::id();
		if($id>0)
		{
			$user=TbAdminCenters::find($id);
			$user->remember_token=null;
			$user->save();
		}
		Auth::logout();
		Session::forget('admin');
		Session::forget('centerid');
		Session::forget('servicepattern');
		return View::make ('admin.login');
	}
}
