<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   <title>موسسه فرزان</title>
    <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('css/sb-admin-rtl.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/morris.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <link rel='stylesheet' href="{{asset('css/js-persian-cal.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <link rel='stylesheet' href="{{asset('css/bootstrap.rtl.min.css')}}"> 
    <link  rel='stylesheet' href="{{asset('css/JSTreeGraph.css')}}">
    <link href="{{asset('css/hover.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/js-persian-cal.min.js')}}"></script>
    <script src="{{asset('http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js')}}"></script>
    <script src="{{asset('http://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js')}}"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- Morris Charts JavaScript -->
    <script src="{{asset('js/plugins/morris/raphael-min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/morris-min.js')}}"></script>
    <script src="{{asset('js/plugins/morris/donut-chart.js')}}"></script>
    <!-- Mapael JavaScript -->
    <script src="{{asset('js/map/jquery.mapael.js')}}"></script>
    <script src="{{asset('js/map/tehran_region.js')}}"></script>
    <style type="text/css">
      
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">منو</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                @if(isset($logo))
                <a class="navbar-brand" href="{{url('/')}}"><img class="center-banner" src="{{url($logo)}}"></a>
                @else
                    <a class="navbar-brand" href="{{url('/')}}">فرزان</a>
                @endif
            </div>
            <ul class="nav navbar-left top-nav">
                <li class="dropdown" style="color : white">
                	<?php 
     	 				$date = jDate::forge()->format(' %d , %B , %Y');
      					print_r( $date);
    				?>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> خوش آمدید {{Session::get('centername')}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{url('profile')}}"><i class="fa fa-fw fa-user"></i> پروفایل</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> نامه ها</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> تنظیمات</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{url('logout')}}"><i class="fa fa-fw fa-power-off"></i> خروج</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="{{url('/panel')}}"><i class="fa fa-fw fa-dashboard"></i><strong> خلاصه وضعیت</strong></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#centerManeger"><i class="fa fa-fw fa-wrench"></i> مدیریت<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="centerManeger" class="collapse">
                           
                            <li>
                                <a href={{url('/users')}}>کاربران</a>
                            </li>
                            <li>
                                <a href={{url('/messages')}}>پیام تولد و ازدواج</a>
                            </li>
                             @if(Session::has('admin') && Session::get('admin')==1 && Session::get('adminid')==Session::get('centerid'))
                                @include('adminmenu')
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a href={{url('/bulk/create')}}><i class="fa fa-fw fa-share"></i> ارسال انبوه</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#opinion"><i class="fa fa-fw fa-arrows-v"></i> نظرسنجی <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="opinion" class="collapse">
                            <li>
                                <a class="glyphicon glyphicon-plus-sign" href={{url('/opinions/create')}}> ایجاد </a>
                            </li>
                            <li>
                                <a class="glyphicon glyphicon-ok-sign" href={{url('/opinions/end')}}> اتمام </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#match"><i class="fa fa-fw fa-arrows-v"></i> مسابقه <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="match" class="collapse">
                            <li>
                                <a class="glyphicon glyphicon-plus-sign" href={{url('/match/create')}}> ایجاد </a>
                            </li>
                            <li>
                                <a class="glyphicon glyphicon-ok-sign" href={{url('/match/end')}}> اتمام </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href={{url('/parametrized/create')}}><i class="glyphicon glyphicon-indent-left"></i> ارسال زماندار</a>
                    </li>
                    <li>
                         <a href="javascript:;" data-toggle="collapse" data-target="#bot"><i class="fa fa-fw fa-arrows-v"></i> ربات<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="bot" class="collapse">
                            <li>
                                <a class="glyphicon glyphicon-plus-sign" href={{url('/bot/create')}}> ایجاد </a>
                            </li>
                            <li>
                                <a class="glyphicon glyphicon-ok-sign" href={{url('/bot/edit')}}> ویرایش</a>
                            </li>
                        </ul>
                    </li>
                    </li>
                    <li>
                        <a href={{url('/reports/main')}}><i class="glyphicon glyphicon-list-alt"></i> گزارش گیری</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <br>
                <div style="height:70px;"></div>
                @if(Session::has('admin') && Session::get('adminid')!=Session::get('centerid'))
                      <a href="{{url('admin/panel/return')}}" class="btn btn-success" style="margin-right: 3px;">بازگشت به پنل مدیریت</a>
                    <br>
                @endif

                @yield('content')
                <!-- /.row -->
            </div>
            
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper --> 
    <script src="{{assets('js/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{asset('js/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('js/plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('js/plugins/flot/flot-data.js')}}"></script>
</body>
</html>
