@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت کاربران
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست کاربران
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    @if(isset($actionmsg))
        {{$actionmsg}}
    @endif
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
 
            <thead>
                <tr>
                    <th>نام</th>
                    <th>نام کاربری</th>
                    <th>ایمیل</th>
                    <th>تاریخ وزمان ثبت نام</th>
                    <th style="color:transparent;">........................</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td><a target="_blank" href="{{url('/admin/users/'.$user->id)}}">{{ $user->username }}</a></td>
                    <td>{{ $user->lname.' '.$user->fname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ \Miladr\Jalali\jDate::forge($user->created_at)->format('Y/m/d -- g:i a')}}</td>
                    <td>
                        <a href="{{url('admin/users/'.$user->id.'/edit')}}" class="btn-lg btn-info hvr-bounce-to-right-info" style="margin-right: 3px;">ویرایش</a>
                        @if($user->superAdmin!=1)
                            {{ Form::open(['url' => '/admin/users/delete/' . $user->id, 'method' => 'POST']) }}
                            {{ Form::submit('حذف', ['class' => 'btn-lg btn-danger pull-right hvr-bounce-to-right-danger'])}}
                            {{ Form::close() }}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </div>
 
    <a href="{{url('admin/users/create')}}" class="btn btn-success">کاربر جدید</a>
 
</div>
@stop