@extends('userLayout')
@section('content')
<div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          پروفایل
        </h1>
        <ol class="breadcrumb">
          <li class="active">
            <i class=""></i> کاربر {{$center->name}}
          </li>
        </ol>
      </div>
    </div>
    
    <p></p>
    @if(isset($successmsg))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$successmsg}}
    </div>
    @endif
    <p></p> 
<fieldset>
    <legend>مشخصات کاربری</legend>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">نام کاربری</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{$center->username}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">شماره موبایل</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->phone==null||$center->phone=="")?"مشخص نشده":$center->phone}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">نام مرکز</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{$center->name}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">کد ملی</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->nationalIdentity==null||$center->nationalIdentity=="")?"مشخص نشده":$center->nationalIdentity}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">ایمیل</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->email==null||$center->email=="")?"مشخص نشده":$center->email}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">آدرس</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->address==null||$center->address=="")?"مشخص نشده":$center->address}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">وب سایت</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->website==null||$center->website=="")?"مشخص نشده":$center->website}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">تاریخ عضویت</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->created_at==null||$center->created_at=="")?"مشخص نشده":\Miladr\Jalali\jDate::forge($center->created_at)->format('Y/m/d')}}</div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-2">
             <div class="prety-label">تاریخ آخرین ویرایش</div>
        </div>
        <div class="col-sm-8">
             <div class="prety-text">{{($center->updated_at==null||$center->updated_at=="")?"مشخص نشده":\Miladr\Jalali\jDate::forge($center->updated_at)->format('Y/m/d')}}</div>
        </div>
    </div>
    <p></p>
</fieldset>
<p></p>

<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <fieldset>
      <legend>لوگوی شما</legend> 
        <img class="img-rounded" alt="لوگوی شما"  src="{{url($center->logo)}}"> 
      <p></p>
    </fieldset>
  </div>
  <br>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
   <style type="text/css">
        .btn-file {
          position: relative;
          overflow: hidden;
      }
      .btn-file input[type=file] {
          position: absolute;
          top: 0;
          right: 0;
          min-width: 100%;
          min-height: 100%;
          font-size: 100px;
          text-align: right;
          filter: alpha(opacity=0);
          opacity: 0;
          outline: none;
          background: white;
          cursor: inherit;
          display: block;
      }
    </style>
    <fieldset>
      <legend>تغییر لوگو </legend>
      <div class="alert alert-info">
          <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
          تصویر باید با فرمت jpeg, jpg باشد و حداکثر حجم باید کمتر 200 کیلو بایت باشد.
      </div>
      <p></p>
      {{ Form::open(['url' => 'profile/storelogo', 'method' => 'POST','files'=>true]) }}
        <div class="row">
          <div class="col-sm-12">
             <span class="btn btn-default btn-file form-control">
                انتخاب فایل  {{ Form::file('file','',array('class'=>"form-control",'required'=>'required')) }}
            </span>
          </div>
        </div>
        <p></p>
         <div class="row">
          <div class="col-sm-12">
            {{ Form::submit('ارسال', ['class' => 'btn btn-success pull-right'])}}
          </div>
        </div>
      {{ Form::close() }}
  </div>
</div>
<br>
<a href="{{url('profile/'.$center->id.'/edit')}}" class="btn-lg btn-success hvr-radial-out-success">ویرایش پروفایل</a>
</div>
<script type="text/javascript">
      $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
        });
            $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label){
                var input = $(this).parent();
                input.children("span").remove();
                input.append("<span> :"+label+"</span>");
                //console.log(input.text());
                //console.log(numFiles);
               // console.log(label);
            });
        });
</script>
@stop