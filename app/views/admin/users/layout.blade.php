<div class="content">
	<div class="row content-top">
		@section('header')

		@show
	</div>
	<div class="row content-body">
		@section('body')

		@show
	</div>
	<div class="row content-footer">
		@section('footer')
		@show
	</div>
</div>