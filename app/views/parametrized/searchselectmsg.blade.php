@extends('userLayout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('css/search.css')}}">
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     انتخاب پیام برای ارسال زماندار
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> نتایج جست و جو
      </li>
    </ol>
  </div>
</div>
{{------------------------------------------------------}}
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive clear back-white">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>عنوان</th>
                        <th>متن پیام</th>
                        <th style="color:transparent;">........................</th>
                    </tr>
                </thead>
     
                <tbody>
                    <?php $i=1; ?>
                    @foreach ($data as $ctr)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{ $ctr->title}}</td>
                        <td>{{ $ctr->text}}</td>
                        <td>
                            <a href="{{url('/parametrized/createfrombank/'.$ctr->id)}}" class="btn btn-info" style="margin-right: 3px;">انتخاب</a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
       
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <a href="{{url('/parametrized/create')}}" class="btn btn-success">بازگشت به ایجاد پیام زماندار</a>
        <a href="{{url('/parametrized/createfrombank')}}" class="btn btn-success">بازگشت به لیست پیامها</a>
    </div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
@stop