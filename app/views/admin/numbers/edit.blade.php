@extends('userLayout')

@section('content')

<div class="col-lg-12">
    @if (Session::get('Msg'))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{Session::get('Msg')}} </strong> 
      </div>
    @endif
    {{Form::open(array('url' => '/admin/numbers/editsave','class'=>'form-horizontal well','method'=>'get'))}}
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>شماره</th>
                    <th >مسنجر</th>
                    <th >مسدود شده</th>
                    <th >مرکز</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($numbers as $number)
                <tr>
                    <td>
                        <input type="text" name="number" class="form-control" >{{$number['number']}}</input>    
                    </td>
                    <td>
                        <select id ="softwares" name ="softwares" class="form-control">
                        @foreach($softwares as $software)
                            <option value="{{$software['id']}}">{{$software['name']}}</option>
                        @endforeach
                        </select>
                    </td>
                    <td>
                        <select id ="block" name ="block" class="form-control">
                            <option value="0">آزاد</option>
                            <option value="1">مسدود</option>
                        </select>
                    </td>
                    <td>
                    	<select id ="centers" name ="centers" class="form-control">
                        @foreach($centers as $center)
                            <option value="{{$center['id']}}">{{$center['name']}}</option>
                        @endforeach
                        </select>  
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <button type="submit" name="numberid" class="btn btn-primary" value="{{$number['id']}}">ذخیره</button>
    {{Form::close()}}
    <a href="{{url('/admin/numbers')}}" class="btn btn-success">بازگشت</a>
</div>
@stop