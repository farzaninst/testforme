@extends('userLayout')
@section('content')

<div class="col-lg-12"> 
    <h3>ویرایش کاربران گروه: <span class="label label-info">{{$group->name}} </span></h3>
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        کاربرانی که انتخاب نشده اند در گروه عضویت ندارند.
        برای حذف کاربران از گروه تیک مقابل آن ها را بردارید و سپس روی دکمه ویرایش کلیک کنید.
    </div>
    <p></p>
    @if(isset($updatemessage))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$updatemessage}}
    </div>
    <p></p>
    @endif
     <p></p>
    <div class="row">
        <p>
       <a href="{{url('users/groups')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
        </p>
    </div>
    <div class="table-responsive clear back-white">
        {{ Form::open(['url' => '/users/groups/' . $group->id.'/users/update?page='.Paginator::getCurrentPage(), 'method' => 'POST']) }}
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>انتخاب</th>
                    <th>نام کاربر</th>
                    <th>تلفن</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($allusers as $ausr)
                <tr>
                    <td>
                        @if(in_array($ausr->id,$groupusers))
                            <input type="checkbox" name="selecteduser[{{$ausr->id}}]" id="" value="{{$ausr->id}}" checked="true">
                        @else
                            <input type="checkbox" name="selecteduser[{{$ausr->id}}]" id="" value="{{$ausr->id}}">
                        @endif
                        <?php $pageusers.=$ausr->id.','; ?>
                    </td>
                    <td>{{ $ausr->fname.' '.$ausr->lname}}</td>
                    <td>{{ $ausr->phone}}</td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
        <input name="pageusers" type="hidden" value="{{$pageusers}}">
        {{ Form::submit('ویرایش', ['class' => 'btn btn-info hvr-bounce-to-right-info'])}}
        {{ Form::close() }}
    </div>
    <div class="row">
        {{$allusers->links()}}
    </div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop