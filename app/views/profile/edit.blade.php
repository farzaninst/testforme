@extends('userLayout')
@section('content')
<div class="col-lg-12">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">
          ویرایش پروفایل
        </h1>
        <ol class="breadcrumb">
          <li class="active">
            <i class=""></i>ویرایش کاربر {{$center->name}}
          </li>
        </ol>
      </div>
    </div>
    <p></p>
    @if(isset($successmsg))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$successmsg}}
    </div>
    @endif
    <p></p>
      @if(isset($errormsg))
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$errormsg}}
    </div>
    @endif
    <p></p>
<fieldset>
    <legend>فرم ویرایش مشخصات </legend>
    {{ Form::open(array('url' => '/profile/'.$center->id.'/update','class'=>'form-horizontal')) }}
                <input name="id" type="hidden" value="{{$center->id}}">
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('name', 'نام مرکز*',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::text('name',isset($name)?$name:$center->name,array('class'=>'form-control','required' => 'required')) }}
        </div>
        <div class="col-sm-5">
            @if(isset($messages)&& isset($messages['name']))
                {{$messages['name'][0]}}
            @endif
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('username', 'نام کاربری مرکز*',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::text('username',$center->username,array('class'=>'form-control','required' => 'required','disabled')) }}
        </div>
        <div class="col-sm-5">
            @if(isset($messages)&& isset($messages['username']))
                {{$messages['username'][0]}}
            @endif
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('email', 'ایمیل',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::text('email',isset($email)?$email:$center->email,array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-5">
            @if(isset($messages)&& isset($messages['email']))
                {{$messages['email'][0]}}
            @endif
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('nationalIdentity', 'کد ملی',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::text('nationalIdentity',isset($nationalIdentity)?$nationalIdentity:$center->nationalIdentity,array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-5">
            @if(isset($messages)&& isset($messages['nationalIdentity']))
                {{$messages['nationalIdentity'][0]}}
            @endif
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('phone', 'موبایل',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::text('phone',isset($phone)?$phone:$center->phone,array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-5">
            @if(isset($messages)&& isset($messages['phone']))
                {{$messages['phone'][0]}}
            @endif
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('address', 'آدرس',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::textarea('address',isset($address)?$address:$center->address,array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-5">
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
             {{ Form::label('website', 'وب سایت',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
             {{ Form::text('website',isset($website)?$website:$center->website,array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-5">
            @if(isset($messages)&& isset($messages['website']))
                {{$messages['website'][0]}}
            @endif
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-3">
            {{ Form::label('tik','اضافه کردن نام مرکز به انتهای پیام های ارسالی',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-4">
               {{ Form::select('tik',array('0'=>'نه','1'=>'بله'),$center->tik,array('class'=>'form-control')) }}
        </div>
        <div class="col-sm-5">
        
        </div>
    </div>
    <p></p> 
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-4">
             {{-- Form submit button. --------------------}}
             {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
        </div>
    </div>
    {{ Form::close() }}
</fieldset> 
    <br>
<br>
<p></p>
<fieldset>
    <legend>فرم تغییر رمز عبور </legend>
    <div>
            {{ Form::open(array('url' => '/profile/storepassword','class'=>'form-horizontal')) }}
            <div class="row">
                <div class="col-sm-3">
                     {{-- Password field. ------------------------}}
                     {{ Form::label('password', 'رمز عبور جدید',array('class'=>'control-label')) }}
                </div>
                <div class="col-sm-4">
                     {{ Form::password('password',array('class'=>'form-control','required' => 'required')) }}
                </div>
                <div class="col-sm-5">
                    @if(isset($messages)&& isset($messages['password']))
                        {{$messages['password'][0]}}
                    @endif
                </div>
            </div>
            <p></p> 
            <div class="row">
                <div class="col-sm-3">
                    {{-- Password confirmation field. -----------}}
                    {{ Form::label('password_confirmation', 'تکرار رمز عبور جدید',array('class'=>'control-label')) }}
                </div>
                <div class="col-sm-4">
                       {{ Form::password('password_confirmation',array('class'=>'form-control','required' => 'required')) }}
                </div>
                <div class="col-sm-5">
                
                </div>
            </div>

            <p></p>
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-4">
                     {{-- Form submit button. --------------------}}
                     {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
                </div>
            </div>
            {{ Form::close() }}
    </div>
 </fieldset>
 <style type="text/css">
.btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>
<br>
<br>
<p>
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        تصویر باید با فرمت jpeg, jpg باشد و حداکثر حجم باید کمتر 200 کیلو بایت باشد.
    </div>
</p>

<fieldset>
    <legend>انتخاب فایل تصویری </legend>
            {{ Form::open(['url' => 'profile/storelogo', 'method' => 'POST','files'=>true]) }}
            <div class="row">
              <div class="col-sm-4">
                 <span class="btn btn-default btn-file form-control">
                    انتخاب فایل  {{ Form::file('file','',array('class'=>"form-control",'required'=>'required')) }}
                </span>
              </div>
            </div>
            <p></p>
             <div class="row">
              <div class="col-sm-3">
              </div>
              <div class="col-sm-4">
                {{ Form::submit('ارسال', ['class' => 'btn btn-success pull-right'])}}
              </div>
            </div>
            {{ Form::close() }}
</fieldset>
<p></p>
<a href="{{url('profile')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
      $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
        });
            $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label){
                var input = $(this).parent();
                input.children("span").remove();
                input.append("<span> :"+label+"</span>");
                //console.log(input.text());
                //console.log(numFiles);
               // console.log(label);
            });
        });
</script>
@stop