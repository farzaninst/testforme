
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      پیام زماندار <small>فرم ایجاد </small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-indent-left"></i> ارسال زماندار
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a href="{{url('/parametrized')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
      
      <hr></hr>
      @if(isset($storemessage)&& isset($storemessage['success']))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$storemessage['success']}}
        </div>
      @endif
      @if(isset($storemessage)&& isset($storemessage['error']))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$storemessage['error']}}
        </div>
      @endif
       {{ Form::open(array('url' => '/parametrized/store','class'=>'form-horizontal well')) }}
  <div class="row">
    <div class="col-sm-3">
       {{ Form::label('title', 'عنوان *',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
       {{ Form::text('title',isset($title)?$title:'',array('class'=>'form-control','required' => 'required')) }}
    </div>
    <div class="col-sm-5">
      @if(isset($messages)&& isset($messages['title']))
        {{$messages['title'][0]}}
      @endif
    </div>
  </div>
  <p></p>
<div class="row">
    <div class="col-sm-3">
       {{ Form::label('msgText', 'متن *',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
       {{ Form::textarea('msgText',isset($msgText)?$msgText:'',array('class'=>'form-control','id'=>'msgText','required' => 'required')) }}
       <p>می توانید از عبارات با قاعده زیر استفاده کنید. مثلا به جای عبارت با قاعده #fname# نام کاربر قرار می گیرد</p>
         <a id="firstname" class="btn btn-default"> نام</a>
         <a id="family" class="btn btn-default">نام خانوادگی</a>
         <a id="education" class="btn btn-default"> تحصیلات</a>
         <a id="gender" class="btn btn-default"> جنسیت</a>
       <p></p>
    </div>
    <div class="col-sm-5">
      <a class="btn btn-success hvr-radial-out-success" href="{{url('/parametrized/createfrombank')}}">انتخاب پیام از بانک پیام</a>
      @if(isset($messages)&& isset($messages['msgText']))
        {{$messages['msgText'][0]}}
      @endif
    </div>
  </div>
  <p></p>
  <div class="row">
    <div class="col-sm-3">
       {{ Form::label('software[]', 'انتخاب پیام رسان',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
      @foreach($messengers as $msgr)
       {{$msgr->name}}<input type="checkbox" name="software[{{$msgr->id}}]" id="{{$msgr->id}}" value="{{$msgr->id}}" checked="true">
       @endforeach
    </div>
    <div class="col-sm-5">
    </div>
  </div>
  <p></p>

  <div class="row">
    <div class="col-sm-3">
      {{ Form::label('mode', 'زمانبندی ارسال',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
         {{ Form::select('mode',$modes,'1',array('class'=>'form-control')) }}
    </div>
    <div class="col-sm-5">
    
    </div>
  </div>
  <p></p> 
   <div class="row">
    <div class="col-sm-3">
      {{ Form::label('sendDate', 'تاریخ اولین ارسال',array('class'=>'control-label')) }}
    </div>
    <div class="col-sm-4">
         <input type="text" class="pdate form-control cursor-pointer" id="date" name="date" value="" placeholder="" readonly>
        <input type="hidden" id="sendDate" name="sendDate" class="pdate wide ">
    </div>
    <div class="col-sm-5">
      @if(isset($messages)&& isset($messages['sendDate']))
        {{$messages['sendDate'][0]}}
      @endif
    </div>
  </div>
  <p></p> 
{{-------------------------------------------------------------------------------------}}
  <p></p> 
   <div class="row">
    <div class="col-sm-3">
      <label class="control-label" for=""> ارسال به : </label>
    </div>
    <div class="col-sm-4">
         <label> <input type="radio" name = "radiousers" value = "allSite" checked ="true"> تمام کاربران سایت</label><br>
         @if(isset($services))
         <label> <input type="radio" name = "radiousers" value = "allService"> تمام کاربران سرویس ها</label><br>
              <div id = "divallservice">
                @foreach ($services as $service)
                  {{$service->name}} <input tabindex="1" type="checkbox" name="service[{{$service->id}}]" id="{{$service->id}}" value="{{$service->id}}" checked="true">
                @endforeach
              </div>
        @endif
        @if(isset($services))
        <label> <input type="radio" name = "radiousers" value = "subService" > فیلتر سرویس ها :</label>
              <input type="hidden" id="subserviceID" name="subserviceID">
              
               <div id = "divsubservice">
                 <label>  سرویس را انتخاب کنید   </label><br>
                 @foreach ($services as $service)
                  <label> <input type="radio" name="filterservice" id="filterservice" value="{{$service->id}}"> {{$service->name}}</label><br>
                 @endforeach
                  <select class="form-control" id="selectSubService" name="selectSubService">
                    <option>لطفا زیر سرویس را انتخاب کنید</option>
                  </select>
               </div>
              
            <br>
           @endif
        <label><input type="radio" name = "radiousers" value = "userFilter" > فیلتر کاربران سایت :</label>
               <div id="divuserfilter" >
                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="name" checked="true">   نام </lable> 
                <input type="text" class="form-control" id="usersFilterName" name="usersFilterName" placeholder="">
                <br>

                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="age" >   سن </lable>
                <div class="input-group input-daterange" id="usersFilterAge" name="usersFilterAge">
                  <select class="form-control" id="usersFiltersStartAge" name="usersFiltersStartAge">
                    @for ($i=1 ; $i<=100 ; $i++){
                      <option value="{{$i}}"> {{$i}} </option>
                    }
                    @endfor
                  </select>
                    <span class="input-group-addon">تا</span>
                  <select class="form-control" id="usersFiltersEndAge" name="usersFiltersEndAge">
                    @for ($i=1 ; $i<=100 ; $i++){
                      <option value="{{$i}}"> {{$i}} </option>
                    }
                    @endfor
                  </select>
                </div>
               <br>

                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="sex" >   جنسیت </lable>
                <select class="form-control" id="usersFilterSex" name="usersFilterSex">
                  <option value="1"> مرد </option>
                  <option value="0"> زن </option>
                </select>
                <br>

                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="marriage" >   وضعیت تاهل </lable>
                <select class="form-control" id="usersFilterMarriage" name="usersFilterMarriage">
                  <option value="0"> مجرد </option>
                  <option value="1"> متاهل </option>
                </select>
                <br>

                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="phone" >   کد شهر </lable>
                <input type="text" class="form-control" id="usersFilterPhone" name="usersFilterPhone" placeholder="">
                <br>

                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="birthdate" >   تاریخ تولد </lable>
                <input type="text" class="pdate form-control cursor-pointer" id="usersFilterBirthdate" name="usersFilterBirthdate" readonly>
                <input type="hidden" id="extraBirthdate" name="extraBirthdate" class="pdate wide">
                <br>

                <lable><input tabindex="1" type="checkbox" name="usersfilter[]" value="marriagedate" >   تاریخ ازدواج </lable>
                <input type="text" class="pdate form-control cursor-pointer" id="usersFilterMarriagedate" name="usersFilterMarriagedate" readonly>
                <input type="hidden" id="extraMarriagedate" name="extraMarriagedate" class="pdate wide">
                <br>
              </div>
            <br>
          @if(isset($services))
            <label><input type="radio" name = "radiousers" value = "userFilterService" > فیلتر کاربران سرویس ها </label>
            <div id = "divuserfilterservice">
              <label>  سرویس را انتخاب کنید   </label><br>
              @foreach ($services as $service)
                <lable>  <input type="radio" name="filterservice" id="filterservice" value="{{$service->id}}"> {{$service->name}}</lable><br>
              @endforeach

              <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="name" checked="true">   نام </lable> 
                <input type="text" class="form-control" id="userserviceFilterName" name="userserviceFilterName" placeholder="">
                <br>

                <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="age" >   سن </lable>
                <div class="input-group input-daterange" id="userserviceFilterAge" name="userserviceFilterAge">
                  <select class="form-control" id="userserviceFiltersStartAge" name="userserviceFiltersStartAge">
                    @for ($i=1 ; $i<=100 ; $i++){
                      <option value="{{$i}}"> {{$i}} </option>
                    }
                    @endfor
                  </select>
                    <span class="input-group-addon">تا</span>
                  <select class="form-control" id="userserviceFiltersEndAge" name="userserviceFiltersEndAge">
                    @for ($i=1 ; $i<=100 ; $i++){
                      <option value="{{$i}}"> {{$i}} </option>
                    }
                    @endfor
                  </select>
                </div>
               <br>

                <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="sex" >   جنسیت </lable>
                <select class="form-control" id="userserviceFilterSex" name="userserviceFilterSex">
                  <option value="1"> مرد </option>
                  <option value="0"> زن </option>
                </select>
                <br>

                <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="marriage" >   وضعیت تاهل </lable>
                <select class="form-control" id="userserviceFilterMarriage" name="userserviceFilterMarriage">
                  <option value="0"> مجرد </option>
                  <option value="1"> متاهل </option>
                </select>
                <br>

                <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="phone" >   کد شهر </lable>
                <input type="text" class="form-control" id="userserviceFilterPhone" name="userserviceFilterPhone" placeholder="">
                <br>

                <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="birthdate" >   تاریخ تولد </lable>
                <input type="text" class="pdate form-control cursor-pointer" id="userserviceFilterBirthdate" name="userserviceFilterBirthdate" readonly>
                <input type="hidden" id="extraServiceBirthdate" name="extraServiceBirthdate" class="pdate wide">
                <br>

                <lable><input tabindex="1" type="checkbox" name="userservicefilter[]" value="marriagedate" >   تاریخ ازدواج </lable>
                <input type="text" class="pdate form-control cursor-pointer" id="userserviceFilterMarriagedate" name="userserviceFilterMarriagedate" readonly>
                <input type="hidden" id="extraServiceMarriagedate" name="extraServiceMarriagedate" class="pdate wide">
                <br>
            </div>
            <br>
         @endif
            <label><input type="radio" name = "radiousers" value = "group" > فیلتر گروه کاربران :</label>
            <div id = "divgroup">
            <input type="hidden" id="groupSelectId" name="groupSelectId">
               <div id = "divgroup">
                 <label>  لیست گروههای ایجاد شده   </label>
                  <select class="form-control" id="groupselect">
                    <option>لطفا یک گروه را انتخاب کنید</option>
                    @foreach ($centerGroups as $key)
                      <option value="{{$key->id}}"> {{$key->name}} </option>
                    @endforeach
                  </select>
                  <table class="table table-bordered" id="grouptable" name="grouptable" >
                    <thead>
                      <tr>
                        <th>نام گروه</th>
                        <th>توضیحات</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </label>
            <br>
            </div>
            <br>
            
            @if(isset($admindata) && count($admindata))
            {{----------- start admin view -------------------------------}}
              <label><input type="radio" name = "radiousers" value = "center" > فیلتر کاربران مراکز</label>
                <div id = "divcenter">
                  <input type="hidden" id="centerSelectId" name="centerSelectId">
                  <input type="hidden" id="serviceSelectId" name="serviceSelectId">
                     <div id = "divcenter">
                       <label>انتخاب سرویس</label>
                        <select name="ad-service" class="form-control" id="allServiceSelect">
                          <option>لطفا یک سرویس را انتخاب کنید</option>
                          @foreach ($allServices as $key)
                            <option value="{{$key['id']}}"> {{$key['name']}}</option>
                          @endforeach
                        </select>
                        <label>انتخاب مرکز</label>
                        <select name="ad-center" class="form-control" id="allCenterSelect">
                          <option>لطفا یک مرکز را انتخاب کنید</option>
                        </select>
                        <div id="ad-subService">
                        </div>
                      </div>
                </div>
            {{----------- end admin view -------------------------------}}
            @endif
      </div>
      

      <div class="col-lg-7"></div>
    </div>
  <p></p> 
{{---------------------------------------------------------------------------------------------------}}
  <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-4">
       {{-- Form submit button. --------------------}}
       {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
    </div>
  </div>
  {{ Form::close() }}
</div>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});

@if(isset($admindata))
var jsondata={{json_encode($admindata)}}
@endif
  $('#allServiceSelect').on('change', function() {
    $('#allCenterSelect').empty();
    $("#ad-subService").empty();
    var value = $(this).val();
    //alert(value);
    var ctr=[];
    for(var i=0;i<jsondata.length;i++)
    {
      if(jsondata[i].service!=null)
      {
       // alert('aaa');
        var srv2=jsondata[i].service;
        for(var j=0;j<srv2.length;j++)
        {
          if(srv2[j].id== value)
          {
            ctr.push([jsondata[i].id,jsondata[i].name,jsondata[i].username]);
            //break;
          }
        }
      }
    }
//alert(ctr.length);
    $('#allCenterSelect').append($('<option>لطفا یک مرکز را انتخاب کنید</option>'));
    for(var k=0;k<ctr.length;k++)
    {
     // alert(ctr[k][2]);
       $('#allCenterSelect').append($('<option>', {
              value: ctr[k][0],
              text: ctr[k][1]+'->'+ctr[k][2]
            }));
    }
  });

   $('#allCenterSelect').on('change', function() {
      $("#ad-subService").empty();
      //alert(jsondata[0]['service'][1]['sub'][1]['name']);
      var value = $(this).val();
      var srvval=$('#allServiceSelect').val();
      var ctr2=null;
      for(var i=0;i<jsondata.length;i++)
      {
        if(jsondata[i].id==value)
        {
          ctr2=i;
          break;
        }
      }
     // alert(ctr2.id +' -- '+value);
      if(ctr2==null)
        return;
      if(jsondata[ctr2].service==null)
        return;
      var srv=null;
      for (var j = 0; j < jsondata[ctr2]['service'].length; j++)
       {
        if(jsondata[ctr2]['service'][j].id==srvval)
        {
          srv=j;
          break;
        }
      }
      // alert(srv.id +' -- '+srvval);
      if(srv==null)
        return;
      if(jsondata[ctr2]['service'][srv].sub==null)
        return;
     var sub=jsondata[ctr2]['service'][srv]['sub'];
     var elm =$('<h5> حداقل یک زیر سرویس را انتخاب کنید </h5>');
     $("#ad-subService").append(elm);
      for(var k=0;k<sub.length;k++)
      {
        var elm=$('<lable><input type="checkbox" name="ad-subservice[]" value ='+sub[k]['id']+'> '+sub[k]['name']+' </lable> ');
         $("#ad-subService").append(elm);
      }

   });
</script>
<script type="text/javascript">

   function inyectarTexto(elemento,valor){
     var elemento_dom=document.getElementsByName(elemento)[0];
     if(document.selection){
      elemento_dom.focus();
      sel=document.selection.createRange();
      sel.text=valor;
      return;
     }if(elemento_dom.selectionStart||elemento_dom.selectionStart=="0"){
      var t_start=elemento_dom.selectionStart;
      var t_end=elemento_dom.selectionEnd;
      var val_start=elemento_dom.value.substring(0,t_start);
      var val_end=elemento_dom.value.substring(t_end,elemento_dom.value.length);
      elemento_dom.value=val_start+valor+val_end;
     }else{
      elemento_dom.value+=valor;
     }
    }
                
                
$(document).ready(function(){

    $("#gender").click(function(){
     inyectarTexto('msgText','#gender#');
     $("#msgText").focus();
    });
    $("#firstname").click(function(){
     inyectarTexto('msgText',' #fname# ');
     $("#msgText").focus();
    });
    $("#family").click(function(){
     inyectarTexto('msgText',' #lname# ');
     $("#msgText").focus();
    });
    $("#education").click(function(){
     inyectarTexto('msgText',' #education# ');
     $("#msgText").focus();
    });

});

</script>

 <script type="text/javascript">
    var objCalStart = new AMIB.persianCalendar( 'date', {
        extraInputID: 'sendDate',
        extraInputFormat: 'YYYY-MM-DD'
      }
    );

  </script>

  <script type="text/javascript">

 $(document).ready(function(){

  $("#usersFilterAge").hide();
  $("#usersFilterSex").hide();
  $("#usersFilterMarriage").hide();
  $("#usersFilterPhone").hide();
  $("#usersFilterBirthdate").hide();
  $("#usersFilterMarriagedate").hide();


  $("#userserviceFilterAge").hide();
  $("#userserviceFilterSex").hide();
  $("#userserviceFilterMarriage").hide();
  $("#userserviceFilterPhone").hide();
  $("#userserviceFilterBirthdate").hide();
  $("#userserviceFilterMarriagedate").hide();

  $('input:checkbox[name="usersfilter[]"]').click(function() {
    var checkevalue = $(this).val();
    switch(checkevalue) {
      case "name":
        $("#usersFilterName").toggle(250);
      break;
      case "age":
        $("#usersFilterAge").toggle(250);
      break;
      case "sex":
        $("#usersFilterSex").toggle(250);
      break;
      case "marriage":
        $("#usersFilterMarriage").toggle(250);
      break;
      case "phone":
        $("#usersFilterPhone").toggle(250);
      break;
      case "birthdate":
        $("#usersFilterBirthdate").toggle(250);
      break;
      case "marriagedate":
        $("#usersFilterMarriagedate").toggle(250);
      break;
    }
  });
  $('input:checkbox[name="userservicefilter[]"]').click(function() {
    var checkevalue = $(this).val();
    switch(checkevalue) {
      case "name":
        $("#userserviceFilterName").toggle(250);
      break;
      case "age":
        $("#userserviceFilterAge").toggle(250);
      break;
      case "sex":
        $("#userserviceFilterSex").toggle(250);
      break;
      case "marriage":
        $("#userserviceFilterMarriage").toggle(250);
      break;
      case "phone":
        $("#userserviceFilterPhone").toggle(250);
      break;
      case "birthdate":
        $("#userserviceFilterBirthdate").toggle(250);
      break;
      case "marriagedate":
        $("#userserviceFilterMarriagedate").toggle(250);
      break;
    }
  });

  $("#divallservice").hide();
  $("#divsubservice").hide();
  $("#divuserfilter").hide();
  $("#divuserfilterservice").hide();
  $("#divgroup").hide();
  $("#divcenter").hide();
  

  $("input:radio[name=radiousers]").click(function() {
    var value = $(this).val();
    if (value == 'allSite') {
      $("#divallservice").hide(500);
      $("#divsubservice").hide(500);
      $("#divuserfilter").hide(500);
      $("#divuserfilterservice").hide(500);
      $("#divgroup").hide(500);
      $("#divcenter").hide(500);
    }
    if (value == 'allService') {
      $("#divallservice").show(500);
      $("#divsubservice").hide(500);
      $("#divuserfilter").hide(500);
      $("#divuserfilterservice").hide(500);
      $("#divgroup").hide(500);
      $("#divcenter").hide(500);
    }
    if (value == 'userFilter') {
      $("#divallservice").hide(500);
      $("#divsubservice").hide(500);
      $("#divuserfilter").show(500);
      $("#divuserfilterservice").hide(500);
      $("#divgroup").hide(500);
      $("#divcenter").hide(500);
    }
    if (value == 'subService') {
      $("#divallservice").hide(500);
      $("#divsubservice").show(500);
      $("#divuserfilter").hide(500);
      $("#divuserfilterservice").hide(500);
      $("#divgroup").hide(500);
      $("#divcenter").hide(500);
    }
    if (value == 'userFilterService') {
      $("#divallservice").hide(500);
      $("#divsubservice").hide(500);
      $("#divuserfilter").hide(500);
      $("#divuserfilterservice").show(500);
      $("#divgroup").hide(500);
      $("#divcenter").hide(500);
    }
    if (value == 'group') {
      $("#divallservice").hide(500);
      $("#divsubservice").hide(500);
      $("#divuserfilter").hide(500);
      $("#divuserfilterservice").hide(500);
      $("#divgroup").show(500);
      $("#divcenter").hide(500);
    }
    if (value == 'center') {
      $("#divallservice").hide(500);
      $("#divsubservice").hide(500);
      $("#divuserfilter").hide(500);
      $("#divuserfilterservice").hide(500);
      $("#divgroup").hide(500);
      $("#divcenter").show(500);
    }
    
  });

  $("input:radio[name=filterservice]").click(function() {
    var value = $(this).val();
    $('#selectSubService').empty();
        @foreach($subService as $sub)
          if ({{$sub->serviceid}} == value) {
            $('#selectSubService').append($('<option>', {
              value: value,
              text: '{{$sub->name}}'
            }));
          }
        @endforeach
  });

  var select = [];
  $('#groupselect').on('change', function() {
    var value = $(this).val();
    var groupid = [];
    var bool=false;
    for (var i = 0 ; i < select.length ; i++) {
      if (select[i] == value) {
        bool = true ;
        break;
      }
    }
  if (!bool) {
    @foreach ($centerGroups as $key)
      if (value == {{$key->id}}) {
        groupid.push(value);
        $("#grouptable tbody").append( 
          "<tr>"+
          "<td>"+ "{{$key->name}}" +"</td>"+ 
          "<td>"+ "{{$key->desc}}" +"</td>"+ 
          "</tr>");
      }
     @endforeach
    select.push(value); 
    $('#groupSelectId').val(JSON.stringify(select)); //store array
    
  }
  });

});
 </script>



 <script type="text/javascript">
  var objCalStart = new AMIB.persianCalendar( 'usersFilterBirthdate', {
    extraInputID: 'extraBirthdate',
    extraInputFormat: 'YYYY-MM-DD'
  });

  var objCalEnd = new AMIB.persianCalendar( 'usersFilterMarriagedate', {
    extraInputID: 'extraMarriagedate',
    extraInputFormat: 'YYYY-MM-DD'
  });
  var objCalStart2 = new AMIB.persianCalendar( 'userserviceFilterBirthdate', {
    extraInputID: 'extraServiceBirthdate',
    extraInputFormat: 'YYYY-MM-DD'
  });

  var objCalEnd2 = new AMIB.persianCalendar( 'userserviceFilterMarriagedate', {
    extraInputID: 'extraServiceMarriagedate',
    extraInputFormat: 'YYYY-MM-DD'
  });
 </script>
@stop