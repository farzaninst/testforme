@extends('userLayout')
@section('content')
<style type="text/css">
.btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت کاربران <small>اکسل</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-plus"></i> افزودن کاربران از طریق فایل اکسل
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <a href="{{url('users')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
    <p></p>
    @if(isset($messages))
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$messages}}
    </div>
    @endif
    <p></p>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-info">
                
                فایل Excel باید دقیقا مشابه فایل نمونه باشد. لطفا فایل نمونه را دانلود کرده و با توجه به موارد ذکر شده در فایل راهنما فایل اکسل را پر کنید.
                <p>

                </p>
                <p>
                    <a href="{{url('users/excel/sample')}}" class="btn btn-default">دانلود فایل نمونه</a>
                </p>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-lg-offset-2 well">
            {{ Form::open(['url' => 'users/excel/upload', 'method' => 'POST','files'=>true]) }}
            <div class="form-group">
                <p>
                    <code>فرم ارسال فایل اکسل</code>
                </p>
                <hr></hr>
                <span class="btn btn-default btn-file form-control">
                    انتخاب فایل  {{ Form::file('file','',array('class'=>"form-control",'required'=>'required')) }}
                </span>
            </div>
            <div class="form-group">
                {{ Form::submit('ارسال', ['class' => 'btn btn-success pull-right'])}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
      $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
        });
            $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label){
                var input = $(this).parent();
                input.children("span").remove();
                input.append("<span> :"+label+"</span>");
                //console.log(input.text());
                //console.log(numFiles);
               // console.log(label);
            });
        });
</script>
@stop