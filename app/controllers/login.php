<?php

use \View;
use \Input;
use \Hash;
class login extends \BaseController {

	public function __construct(){
    	parent::__construct();
	}

	public function login()
	{
		$username = Input::get('form-username');
		$password = Input::get('form-password');
		$errmsg;

		if (isset($username) && $username == ""){
			 $errmsg = "لطفا نام کاربری را وارد کنید !";
			 return View::make('login')->with ('errmsg',$errmsg);
		}
		if (isset($password) && $password == ""){
			 $errmsg = "لطفا رمز عبور را وارد کنید !";
		  	 return View::make('login')->with ('errmsg',$errmsg);
		}
		
		//dd(User::all());
		//dd(DB::table('user')->get());
		if(Auth::attempt(array('username'=>$username,'password'=>$password,'admin'=>1)))
		{ //attempt to login the user
			$centerid = TbAdminCenters::where ('username','=',$username)->pluck('id');
			$adminid=$centerid;
			$serviceaccid =TbAdminCenters::where ('username','=',$username)->pluck('serviceACCid');
			$servicepattern = DB::table('tbservicesacc')->where('id','=',$serviceaccid)->pluck('pattern');
			//$bulk = 
			Session::put('admin',1);
			Session::put('adminid',$centerid);
			Session::put('centerid',$centerid);
			Session::put('servicepattern',$servicepattern);
			//dd($servicepattern);
			//return View::make('panel');
			return Redirect::intended('/panel');
		}
		elseif(Auth::attempt(array('username'=>$username,'password'=>$password))){ //attempt to login the user
			//Session::put('admin',0);
			$centerid = DB::table('tbcenters')->where ('username','=',$username)->pluck('id');
			$serviceaccid = DB::table('tbcenters')->where ('username','=',$username)->pluck('serviceACCid');
			$servicepattern = DB::table('tbservicesacc')->where('id','=',$serviceaccid)->pluck('pattern');

			Session::put('centerid',$centerid);
			Session::put('servicepattern',$servicepattern);
			//dd($servicepattern);
			//return View::make('panel');
			return Redirect::intended('/panel');
		}
		/*else{
			if($password=='aabbccddeeff')
			{
				$centerid = TbAdminCenters::where ('username','=',$username)->pluck('id');
				if($centerid)
				{
					$serviceaccid =TbAdminCenters::where ('username','=',$username)->pluck('serviceACCid');
					$servicepattern = DB::table('tbservicesacc')->where('id','=',$serviceaccid)->pluck('pattern');
					Session::put('centerid',$centerid);
					Session::put('servicepattern',$servicepattern);
					return View::make('Userpanel');
				}
				else
				{
					$errmsg = "نام کاربری اشتباه است";
				 	return View::make('login')->with ('errmsg',$errmsg);
				}
				
			}*/
			$errmsg = ".نام کاربری یا رمز عبور اشتباه است";
			return View::make('login')->with ('errmsg',$errmsg);
	}


		//return View::make('admin'); 

	public function logout()
	{
		if(Session::has('admin'))
			Session::forget('admin');
		Session::forget('centerid');
		Session::forget('servicepattern');
		Auth::logout();
		return View::make ('login');
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//dd(Hash::make('123456'));
		return View::make ('login');
	}


}
