@extends('userLayout')

@section('header')

	<h1> ROBOT </h1>

@stop

@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      فرم ربات <small>ویرایش درخت ربات: {{$bot->name}}</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-bold"></i> ربات
      </li>
    </ol>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    @if(isset($errormsg))
        <div class="alert alert-danger ">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$errormsg}}
        </div>
    @endif
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    @if(isset($successmsg))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$successmsg}}
        </div>
    @endif
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <a href="{{url('/bot/')}}" type='button' class="btn btn-success hvr-bounce-to-right-success" >بازگشت</a>
    <a target="_blank" href="{{url('/bot/'.$bot->id.'/show')}}" class="btn btn-success hvr-radial-out-success">مشاهده ساختار درختی ربات</a>
    <p></p>
    {{Form::open(array('url'=>'/bot/'.$bot->id.'/deleteTree', 'method' => 'post'))}}
    {{ Form::submit('حذف درخت ربات',array('class'=>'btn btn-danger','id'=>'deleteTree')) }}
    {{Form::close()}}
    <p></p>
    {{Form::open(array('url'=>'/bot/'.$bot->id.'/editTree/addquestion','class'=>'form-horizontal well',  'method' => 'post'))}}
      <div class="row">
        <h3>افزودن سوال</h3><hr></hr>
      </div>
      <div class="row">
        <div class="col-sm-2">
          {{ Form::label('question[]', 'پرسش 1',array('class'=>'control-label')) }}
        </div>
        <div class="col-sm-7">
             {{ Form::text('question[]','',array('class'=>'form-control')) }}
        </div>
      </div>
      <p></p>
      <div id="question">
      </div>

      <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-6">
        </div>
        <div class="col-sm-2">
          <a type='button' class="btn btn-success hvr-radial-out-success" id='addButton'>سوال جدید</a>
        </div>
      </div>
      <p></p>
    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-2">
       {{-- Form submit button. --------------------}}
       {{ Form::submit('ثبت سوالات جدید',array('class'=>'form-control btn btn-primary')) }}
    </div>
  </div>
	{{Form::close()}}

  </div>
</div>
<div class="row">
  <h3>لیست سوالات ربات</h3>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
    <div class="table-responsive clear back-white">
          <table class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>سوال</th>
                      <th> ریشه</th>
                      <th style="color:transparent;">........................</th>
                  </tr>
              </thead>
   
              <tbody>
                  <?php $counter=1; ?>
                  @foreach ($questions as $q)
                  <tr>
                      <td>{{$counter++}}</td>
                      <td>{{ $q->text }}</td>
                      <td>{{ ($q->root==1)? 'سوال ریشه':'' }}</td>
                      <td>
                          <a href="{{url('bot/'.$bot->id.'/editTree/'.$q->id)}}" class="btn btn-info pull-left hvr-radial-out-info" style="margin-right: 3px;">ویرایش</a>
                          {{Form::open(['url' => 'bot/'.$bot->id.'/deleteQuestion/'.$q->id, 'method' => 'POST','onsubmit'=>'return myFunction();'])}}
                          {{Form::submit('حذف', ['class' => 'btn btn-danger pull-right','id'=>'deleteQuestion'])}}
                          {{Form::close()}}
                      </td>
                  </tr>
                  @endforeach
              </tbody>
   
          </table>
      </div>
    </div>
</div>


<script type="text/javascript">
function myFunction()
{
  if (!confirm("آیا می خواهید سوال را حذف کنید؟ حذف سوال ارتباطات وابسته به این سوال در درخت ربات را نیز حذف می کند")){
    return false;
  }
}
$(document).ready(function(){
    var counter = 2;    
    $("#addButton").click(function () {
    var newTextBoxDiv = $(document.createElement('div'));      
      newTextBoxDiv.after().html('<div class="row"><div class="col-sm-2"><label for="question[]" class="control-label">پرسش '+counter+' </label></div><div class="col-sm-7"><input class="form-control" name="question[]" type="text" value="" id="question[]"></div></div><p></p>');     
      newTextBoxDiv.appendTo("#question");       
      //$("#question").append(newTextBoxDiv);
      counter++;
    });
});
$(document).ready(function(){
      $("#deleteTree").click(function(){
        if (!confirm("آیا می خواهید آیتم را حذف کنید؟ خذف درخت تمام ساختار درختی را حذف می کند. سوالات موجود در ربات حذف نخواهند شد.")){
          return false;
        }
      });
      $("#deleteQuestion").click(function(){
        if (!confirm("آیا می خواهید سوال را حذف کنید؟ حذف سوال ارتباطات وابسته به این سوال در درخت ربات را نیز حذف می کند")){
          return false;
        }
      });
});
</script>
@stop