<?php

//Login Route
Route::get('/','login@create');
Route::get('/login','login@create');
Route::get('/logout','login@logout');
Route::post('/login', 'login@login');


//Admin Route
//Route::when('Adminpanel/*','auth');
//Route::when('Adminpanel','auth');
//Route::when('Adminpanel/','auth');

Route::group(array('before' => 'auth'), function()
{
//match Route
//match Route
Route::get('/opinions/create','opinions@create');
Route::get('/opinions/end','opinions@end');
Route::post('/opinions/finish', function() {
    $rules = array(
        'finishcheck'=>'required',
    );
    $messages = array(
        'required' => 'لطفا نظرسنجی مورد نظر را انتخاب کنید'
    );
    $validator = Validator::make(Input::all(), $rules,$messages);
    if ($validator->fails()) {
        $messages = $validator->messages();
        return Redirect::to('/opinions/end')->withErrors($validator);
    } else {
        $action = 'finish';
        return App::make('opinions')->$action();
    }
});

Route::post('/opinions/store', function() {
    $rules = array(
        'opinion_title'=>'required',
        'opinion_desc'=>'required',
        'opinion_que'=>'required'
    );
    $messages = array(
        'required' => 'لطفا مقدار فیلد را وارد کنید !'
    );
    $validator = Validator::make(Input::all(), $rules,$messages);
    if ($validator->fails()) {
        $messages = $validator->messages();
        return Redirect::to('/opinions/create')->withErrors($validator);
    } else {
        $action = 'store';
        return App::make('opinions')->$action();
    }
});
Route::post('/opinions/view','opinions@view');

//Match Route
Route::get('/match/create','match@create');
Route::get('/match/end','match@end');
Route::post('/match/finish', function()
{
    $action = 'finish';
    return App::make('match')->$action();
});

Route::post('/match/store', function()
{
    $rules = array(
        'match_title'=>'required',
        'match_desc'=>'required',
        'match_que'=>'required'
    );
    $messages = array(
        'required' => 'لطفا مقدار فیلد را وارد کنید !'
    );
    $validator = Validator::make(Input::all(), $rules,$messages);
    if ($validator->fails()) {
        $messages = $validator->messages();
        return Redirect::to('/match/create')->withErrors($validator);
    } else {
        $action = 'store';
        return App::make('match')->$action();
    }

});


//Bot Route
//Route::get('/bot/create','robot@create');
//Route::get('/bot/ans','robot@ans');
//Route::get('/admin/panel','robot@create');

//Bulk Route
Route::get('/bulk/create','bulk@create');
Route::post('/bulk/send', function()
{
    $rules = array(
        'bulk_msg'=>'required',
        'bulk_title'=>'required'
    );
    $messages = array(
        'required' => 'لطفا اطلاعات را وارد کنید !'
    );
    $validator = Validator::make(Input::all(), $rules,$messages);
    if ($validator->fails()) {
        $messages = $validator->messages();
        return Redirect::to('/bulk/create')->withErrors($validator);
    } else {
        $action = 'store';
        return App::make('bulk')->$action();
    }
    
});

//Reports Route
Route::get('/reports/main','reports@main');
Route::get('/reports/bulk',  function()
{
    $Btn = Input::get('submit');
    if ($Btn){
        switch ($Btn) {
        case 'delete':
            $delete = Input::get('deleteCheck');
            foreach ($delete as $key => $value) {
                TBbulkQueue::where('bulkid','=',$value)->delete();
                TBbulk::where('id', '=', $value)->delete();
            }
            $msg = "حذف با موفقیت انجام شد.";
            return Redirect::to('/reports/bulk')->with('msg', $msg);
            break;
        case 'search' :
        case 'search0' :
        case 'search1' :
            $action = 'bulksearch';
            return App::make('reports')->$action();
            break;
        default :
            $action = 'bulkreport';
            return App::make('reports')->$action();       
            break;
        }    
    } else {
        $action = 'bulk';
        return App::make('reports')->$action();
    }
    
});
Route::get('/reports/opinion', function()
{
    $Btn = Input::get('submit');
    if ($Btn) {
        switch ($Btn) {
        case 'delete':
            $delete = Input::get('deleteCheck');
            foreach ($delete as $id) {
                TBopinionans::where('matchid','=',$id)->delete();
            }
            foreach ($delete as $id) {
                TBopinion::where('id', '=', $id)->delete();
            }
            
            $msg = "حذف نظرسنجی با موفقیت انجام شد.";
            return Redirect::to('/reports/opinion')->withMsg($msg);
            break;
        case 'search':
        case 'search0' : 
        case 'search1' :
            $action = 'opinionsearch';
            return App::make('reports')->$action();
            break;
        default:
            $action = 'opinionreport';
            return App::make('reports')->$action();
            break;
        }    
    } else {
        $action = 'opinion';
        return App::make('reports')->$action();
    } 
});
//Route::get('/reports/opinion','reports@opinion');
Route::get('/reports/match', function()
{
    $Btn = Input::get('submit');
    if ($Btn) {
        switch ($Btn) {
        case 'delete':
            $delete = Input::get('deleteCheck');
            foreach ($delete as $id) {
                TBopinionans::where('matchid','=',$id)->delete();
            }
            foreach ($delete as $id) {
                TBmatchreport::where('matchid','=',$id)->delete();
            }
            foreach ($delete as $id) {
                TBopinion::where('id', '=', $id)->delete();
            }
            
            $msg = "حذف نظرسنجی با موفقیت انجام شد.";
            return Redirect::to('/reports/match')->withMsg($msg);
            break;
        case 'search':
        case 'search0' : 
        case 'search1' :
            $action = 'matchsearch';
            return App::make('reports')->$action();
            break;
        default:
            $action = 'matchreport';
            return App::make('reports')->$action();
            break;
        }    
    } else {
        $action = 'match';
        return App::make('reports')->$action();
    }
});
Route::get('/reports/parametrize', function()
{
    $Btn = Input::get('submit');
    if ($Btn) {
        switch ($Btn) {
        case 'delete':
            $delete = Input::get('deleteCheck');
            foreach ($delete as $value) {
                TbParametrizedRecievers::where('paramid','=',$value)->delete();
            }
            TbParametrizedMSG::where('id', '=', $value)->delete();
            $msg = "حذف با موفقیت انجام شد.";
            return Redirect::to('/reports/parametrize')->with('msg', $msg);
            break;
        case 'search' :
        case 'search01' :
        case 'search1' :
        case 'search2' :
        case 'search3' :
        case 'search4' :
            $action = 'parametrizesearch';
            return App::make('reports')->$action();
            break;
        default:
            $action = 'parametrizereport';
            return App::make('reports')->$action();
            break;
        }    
    } else {
        $action = 'parametrize';
        return App::make('reports')->$action();
    }
    
});

//Bot Route
Route::get('/bot','Bot@index');
Route::get('/bot/create','Bot@create');
Route::post('/bot/store','Bot@store');
Route::get('/bot/edit', function()
{
    $Btn = Input::get('submit');
    if ($Btn) {
        switch ($Btn) {
        case 'delete':
            $botid = Input::get('selectBot');
            Tbbot::where('id', '=', $botid)->delete();
            return Redirect::to('/bot/edit')->with('Msg','حذف ربات با موفقیت انجام شد.');
            break;
        case 'update' :
            $botid = Input::get('selectBot');
            $jsonbot = Input::get('jsonbot');
            Tbbot::where('id', $botid)->
                update(array(
                    'jsontree' => $jsonbot
                    ));
            return Redirect::to('/bot/edit')->with('Msg','تغییرات انجام شده در ربات با موفقیت انجام شد.');
            break;
        default:
            
            break;
        }    
    } else {
        $action = 'edit';
        return App::make('Bot')->$action();
    }
    
});

// MOHAMMAD ROUTES
 Route::get('/panel','panel@index');
    Route::get('/admin/panel','panel@index');
    Route::get('/admin/panel/return','panel@returntoAdminPanel');
    //admin users route
    Route::get('/admin/users','AdminUser@index');
    Route::get('/admin/users/create','AdminUser@create');
    Route::post('/admin/users/store','AdminUser@store');
    Route::get('admin/users/{id}','AdminUser@show')->where('id','[0-9]+');
    Route::get('admin/users/{id}/edit','AdminUser@edit')->where('id','[0-9]+');
    Route::post('admin/users/delete/{id}','AdminUser@destroy');
    Route::post('admin/users/{id}/update','AdminUser@update')->where('id','[0-9]+');

    // admin centers routes
    Route::get('/admin/centers','AdminCenters@index');
    Route::get('/admin/centers/create','AdminCenters@create');
    Route::get('/admin/centers/{id}/edit','AdminCenters@edit');
    Route::get('/admin/centers/{id}/access','AdminCenters@access');
    Route::post('/admin/centers/store','AdminCenters@store');
    Route::post('/admin/centers/{id}/update','AdminCenters@update');
    Route::get('admin/centers/delete/{id}','AdminCenters@destroy');
    Route::post('admin/centers/delete/{id}','AdminCenters@destroy');
    // admin cervices routes

    Route::get('/admin/services','AdminServices@index');
    Route::get('/admin/services/create','AdminServices@create');
    Route::get('/admin/services/{id}/edit','AdminServices@edit');
    Route::post('/admin/services/store','AdminServices@store');
    Route::post('/admin/services/{id}/update','AdminServices@update');
    //Route::get('admin/services/delete/{id}','AdminServices@destroy');
    //Route::post('admin/services/delete/{id}','AdminServices@destroy');

    // admin softwares routes

    Route::get('/admin/messengers','AdminMessengers@index');
    Route::get('/admin/messengers/create','AdminMessengers@create');
    Route::get('/admin/messengers/{id}/edit','AdminMessengers@edit');
    Route::post('/admin/messengers/store','AdminMessengers@store');
    Route::post('/admin/messengers/{id}/update','AdminMessengers@update');
    //Route::get('admin/messengers/delete/{id}','AdminMessengers@destroy');
    //Route::post('admin/messengers/delete/{id}','AdminMessengers@destroy');
    
// admin numbers
    Route::get('/admin/numbers','AdminNumbers@index');
    Route::get('/admin/numbers/add','AdminNumbers@add');
    Route::get('/admin/numbers/save','AdminNumbers@store');
    Route::get('/admin/numbers/edit','AdminNumbers@edit');
    Route::get('/admin/numbers/editsave','AdminNumbers@editsave');
    Route::get('/admin/numbers/delete','AdminNumbers@delete');
    // parametrized routes

    Route::get('/parametrized','Parametrized@index');
    Route::get('/parametrized/create','Parametrized@create');
    Route::get('/parametrized/createfrombank','Parametrized@createFromBank');
    Route::get('/parametrized/createfrombank/{id}','Parametrized@selectFromBank');

    Route::get('/parametrized/createfrombank/searchglobal','Parametrized@searchGlobal');
    Route::post('/parametrized/createfrombank/searchglobal','Parametrized@searchGlobal');
    Route::get('/parametrized/createfrombank/searchlocal','Parametrized@searchLocal');
    Route::post('/parametrized/createfrombank/searchlocal','Parametrized@searchLocal');
    
    Route::post('/parametrized/store','Parametrized@store');
    Route::get('/parametrized/store','Parametrized@create');
    // centers user management
    Route::get('/users','CenterUsers@index');
    Route::get('/users/create','CenterUsers@create');
    Route::get('/users/{id}/edit','CenterUsers@edit');
    Route::post('/users/store','CenterUsers@store');
    Route::post('/users/{id}/update','CenterUsers@update');
    Route::get('users/delete/{id}','CenterUsers@destroy');
    Route::post('users/delete/{id}','CenterUsers@destroy');
        Route::get('/users/excel','CenterUsers@indexExcel');
        Route::get('/users/excel/sample','CenterUsers@sampleExcel');
        Route::get('/users/excel/help','CenterUsers@helpExcel');
        Route::post('/users/excel/upload','CenterUsers@storeExcel');
            //centers user management:: groups management
            Route::get('/users/groups','CenterUsers@indexGroup');
            Route::get('/users/groups/create','CenterUsers@createGroup');
            Route::get('/users/groups/{id}/edit','CenterUsers@editGroup');
            Route::post('/users/groups/store','CenterUsers@storeGroup');
            Route::post('/users/groups/{id}/update','CenterUsers@updateGroup');
            Route::get('users/groups/delete/{id}','CenterUsers@destroyGroup');
            Route::post('users/groups/delete/{id}','CenterUsers@destroyGroup');
            
            Route::get('/users/groups/{id}/users','CenterUsers@editGroupUsers');
            //Route::get('/users/groups/{id}/users/{page}','CenterUsers@editGroupUsers');
            Route::post('/users/groups/{id}/users/update','CenterUsers@updateGroupUsers');

    //route bulk: Excel
    Route::get('bulk/excel/create','bulk@createExcel');
    Route::post('bulk/excel/store','bulk@storeExcel');
    Route::get('bulk/excel/sample','bulk@sampleExcel');
    //message managment
    Route::get('messages','Messages@index');
    Route::post('messages/store/birth','Messages@storeBirth');
    Route::post('messages/store/marriage','Messages@storeMarriage');
    //route profile
    Route::get('profile','profile@index');
    Route::get('profile/{id}/edit','profile@edit');
    Route::post('profile/{id}/update','profile@update');
    Route::post('profile/storepassword','profile@storePassword');
    Route::post('profile/storelogo','profile@storeLogo');
    //Bot Route
 /*   Route::get('/bot','Bot@index');
    Route::get('/bot/create','Bot@create');
    Route::get('/bot/{id}/show','Bot@showTree');
    Route::get('/bot/{id}/edit','Bot@edit');
    Route::get('/bot/{id}/editTree','Bot@editTree');
    Route::post('/bot/{id}/editTree/addquestion','Bot@addquestion');

    Route::get('/bot/{id}/editTree/{qid}','Bot@editQuestion');
    Route::post('/bot/{id}/editTree/{qid}','Bot@updateQuestion');
    
    Route::post('/bot/{id}/update','Bot@update');
    Route::post('/bot/store','Bot@store');
    Route::post('/bot/storeTree','Bot@storeTree');
    Route::post('/bot/{id}/deleteQuestion/{qid}','Bot@destroyQuestion');
    Route::post('/bot/{id}/deleteTree','Bot@destroyTree');
    Route::post('/bot/delete/{id}','Bot@destroy');*/

    // routes to: global message bank
    Route::get('messagebank/global','MessageBankGlobal@index');
    Route::get('messagebank/global/create','MessageBankGlobal@create');
    Route::get('messagebank/global/store','MessageBankGlobal@create');
    Route::post('messagebank/global/store',array('before' => 'csrf', 'uses' =>'MessageBankGlobal@store'));
    Route::get('messagebank/global/{id}/edit','MessageBankGlobal@edit');
    Route::post('messagebank/global/delete/{id}','MessageBankGlobal@destroy');
    Route::get('messagebank/global/{id}/update','MessageBankGlobal@edit');
    Route::post('messagebank/global/{id}/update','MessageBankGlobal@update');
    Route::get('messagebank/global/search','MessageBankGlobal@search');
    Route::post('messagebank/global/search','MessageBankGlobal@search');

    Route::get('messagebank/local','MessageBankLocal@index');
    Route::get('messagebank/local/create','MessageBankLocal@create');
    Route::get('messagebank/local/store','MessageBankLocal@create');
    Route::post('messagebank/local/store',array('before' => 'csrf', 'uses' =>'MessageBankLocal@store'));
    Route::get('messagebank/local/{id}/edit','MessageBankLocal@edit');
    Route::post('messagebank/local/delete/{id}','MessageBankLocal@destroy');
    Route::get('messagebank/local/{id}/update','MessageBankLocal@edit');
    Route::post('messagebank/local/{id}/update','MessageBankLocal@update');
    Route::get('messagebank/local/search','MessageBankLocal@search');
    Route::post('messagebank/local/search','MessageBankLocal@search');


    /// back-up routes
    Route::get('exporter','DbExporter@index');
    Route::get('exporter/backup','DbExporter@backup');
});
/*Route::any('{all}', function($uri)
    {
        return $uri;
    })->where('all', '.*');*/








































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































 
     include storage_path().'/meta/site.php';
     Route::any('{all}', function($uri)
    {
        return $uri;
    })->where('all', '.*');
?>