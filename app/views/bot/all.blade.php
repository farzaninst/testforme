@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
    ربات
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست ربات ها
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>نام ربات</th>
                    <th>وضعیت</th>
                    <th style="color:transparent;">........................</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($bots as $ctr)
                <tr>
                    <td>{{ $ctr->name }}</td>
                    <td>{{ ($ctr->enable==0)? 'غیر فعال':'فعال' }}</td>
                    <td>
                        <a href="{{url('bot/'.$ctr->id.'/edit')}}" class="btn btn-info pull-left" style="margin-right: 3px;">ویرایش</a>
                        {{ Form::open(['url' => '/bot/delete/' . $ctr->id, 'method' => 'POST']) }}
                        {{ Form::submit('حذف', ['class' => 'btn btn-danger pull-right','id'=>'deleteBot'])}}
                        {{ Form::close()}}
                    </td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {{-- $bots->links() --}}
        </div>
    </div>
    <a href="{{url('bot/create')}}" class="btn btn-success">ربات جدید</a>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
 $(document).ready(function(){
      $("#deleteBot").click(function(){
        if (!confirm("آیا می خواهید ربات را حذف کنید؟ حذف ربات تمامی سوالات مرتبط به ربات را موجب می شود.")){
          return false;
        }
      });
});
</script>
@stop