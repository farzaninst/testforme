@extends('userLayout')
@section('content')
<div class="col-lg-12"> 
    <a href="{{url('/parametrized/create')}}" class="btn btn-success">ایجاد پیام زماندار</a>
    <p></p>
    @if(isset($messages))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$messages}}
    </div>
    @endif
    <p></p>
    @if(isset($data) && count($data)>0)
        <div class="table-responsive clear back-white">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>عنوان</th>
                        <th>تاریخ اولین ارسال</th>
                         <th>زمانبندی</th>
                        <th>متن پیام</th>
                        <th>وضعیت</th>
                    </tr>
                </thead>
     
                <tbody>
                    @foreach ($data as $ctr)
                    <tr>
                        <td>{{ $ctr->title }}</td>
                        <td>{{\Miladr\Jalali\jDate::forge($ctr->sendDate)->format('Y/m/d')}}</td>
                        <td>{{ $modes[$ctr->mode]}}</td>
                        <td>{{ $ctr->msgText}}</td>
                        <td>
                            {{($ctr->finish==0)?'فعال':'غیر فعال'}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
     
            </table>
        </div>
    @else
        <div class="alert alert-info">پیام زماندار وجود ندارد</div>
    @endif
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop