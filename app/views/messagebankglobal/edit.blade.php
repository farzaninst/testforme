
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ویرایش پیام عمومی : <small>{{$msg->title}}</small></h1>
			<hr></hr>
	@if(isset($successmsg))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$successmsg}}
        </div>
      @endif
      @if(isset($errormsg))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
            {{$errormsg}}
        </div>
      @endif
	{{ Form::open(array('url' => '/messagebank/global/'.$msg->id.'/update','class'=>'form-horizontal well')) }}
  <input type="hidden" name="id" value="{{$msg->id}}">
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('title', 'عنوان *',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('title',isset($title)?$title:'',array('class'=>'form-control','required' => 'required')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['title']))
				{{$messages['title'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
	    <div class="col-sm-3">
	       {{ Form::label('text', 'متن *',array('class'=>'control-label')) }}
	    </div>
	    <div class="col-sm-4">
	       {{ Form::textarea('text',isset($text)?$text:'',array('class'=>'form-control','id'=>'text','required' => 'required')) }}
	       <p>می توانید از عبارات با قاعده زیر استفاده کنید. مثلا به جای عبارت با قاعده #fname# نام کاربر قرار می گیرد</p>
	         <a id="firstname" class="btn btn-default"> نام</a>
	         <a id="family" class="btn btn-default">نام خانوادگی</a>
	         <a id="education" class="btn btn-default"> تحصیلات</a>
	         <a id="gender" class="btn btn-default"> جنسیت</a>
	       <p></p>
	    </div>
	    <div class="col-sm-5">
	      @if(isset($messages)&& isset($messages['text']))
	        {{$messages['text'][0]}}
	      @endif
	    </div>
  	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>

	<a href="{{url('messagebank/global')}}" class="btn btn-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
    ///*************************************************/
    var elements = document.getElementsByTagName("TEXTAREA");
    for (var i = 0; i < elements.length; i++) {
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">

   function inyectarTexto(elemento,valor){
     var elemento_dom=document.getElementsByName(elemento)[0];
     if(document.selection){
      elemento_dom.focus();
      sel=document.selection.createRange();
      sel.text=valor;
      return;
     }if(elemento_dom.selectionStart||elemento_dom.selectionStart=="0"){
      var t_start=elemento_dom.selectionStart;
      var t_end=elemento_dom.selectionEnd;
      var val_start=elemento_dom.value.substring(0,t_start);
      var val_end=elemento_dom.value.substring(t_end,elemento_dom.value.length);
      elemento_dom.value=val_start+valor+val_end;
     }else{
      elemento_dom.value+=valor;
     }
    }
                
                
$(document).ready(function(){

    $("#gender").click(function(){
     inyectarTexto('text','#gender#');
     $("#text").focus();
    });
    $("#firstname").click(function(){
     inyectarTexto('text',' #fname# ');
     $("#text").focus();
    });
    $("#family").click(function(){
     inyectarTexto('text',' #lname# ');
     $("#text").focus();
    });
    $("#education").click(function(){
     inyectarTexto('text',' #education# ');
     $("#text").focus();
    });

});

</script>
@stop