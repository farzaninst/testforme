<?php

class AdminServices extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $validator;
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('authAdmin');
	}
	public function index()
	{
		$data=TbAdminServices::all();
		return View::make('admin/services/all')->with('services',$data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin/services/create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validate=$this->storeValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$service=new TbAdminServices;
			$service->name=Input::get('name');
			$service->enable=Input::get('enable');
			$service->save();
			$srv=TbAdminServices::all();
			if(count($srv)>1)
			{
				$lastid=$srv->last()->id;
				//echo $lastsrv->id;
				$currentid=$lastid;
				$currentpatt=TbAdminServicesACC::all();
				$newpatt=array();
				$newpatt[]=$currentid;
				foreach($currentpatt as $patt)
				{
					$newpatt[]=$patt->pattern.','.$currentid;
				}
				foreach($newpatt as $np)
				{
					$add=new TbAdminServicesACC;
					$add->pattern=$np;
					$add->save();
				}
			}
			else
			{
				$currentid=$srv->last()->id;
				$add=new TbAdminServicesACC;
				$add->pattern=$currentid;
				$add->save();
			}
			$ctr=TbAdminServices::all();
			return Redirect::to('admin/services');
			return View::make('admin/services/all')
						->with('services',$ctr);
		}
		else
		{
			return View::make('admin/services/create')
						->with('name',Input::get('name'))
						->with( 'messages',$messages->getMessages())
						->with('enable',Input::get('enable'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ctr=TbAdminServices::find($id);
		//var_dump($ctr);
		if($ctr)
		{
			return View::make('admin/services/edit')->with('service',$ctr);
		}
		else
		{
			return "bad request";
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$service=TbAdminServices::find(Input::get('id'));
		if(!$service)
		{
			return "Bad request";
		}
		$validate=$this->updateValidation();
		$messages = $this->validator->messages();
		if($validate)
		{
			$service->name=Input::get('name');
			$service->enable=Input::get('enable');
			$service->save();
			$ctr=TbAdminServices::all();
			return Redirect::to('admin/services');
				//	return View::make('admin/services/all')
						//		->with('services',$ctr);
		}
		else
		{
			return View::make('admin/services/edit')
						->with('name',Input::get('name'))
						->with('service',$service)
						->with( 'messages',$messages->getMessages())
						->with('enable',Input::get('enable'));
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$center=TbAdminServices::find($id);
		if($center)
		{
			$result=TbAdminServices::destroy($id);
			return Redirect::to('admin/services');
			//return $this->index();
		}
		else
		{
			return "bad request";
		}
	}

	private function storeValidation()
	{
		$role=array('name'=>'required');
		$this->validator=\Validator::make(
			array('name'=>Input::get('name')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	private function updateValidation()
	{
		$role=array('name'=>'required');
		$this->validator=\Validator::make(array('name'=>Input::get('name')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
