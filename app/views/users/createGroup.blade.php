
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ثبت گروه کاربری جدید</h1>
			<hr></hr>
			 {{ Form::open(array('url' => '/users/groups/store','class'=>'form-horizontal well')) }}
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('name', 'نام گروه',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('name',isset($name)?$name:'',array('class'=>'form-control','required')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['name']))
				{{$messages['name'][0]}}
			@endif
		</div>
	</div>
	  <p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('desc', 'توضیحات',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::textarea('desc',isset($desc)?$desc:'',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['desc']))
				{{$messages['desc'][0]}}
			@endif
		</div>
	</div>
  <p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>

	<a href="{{url('users/groups')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
@stop