<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use \Eloquent;
use \Exception;
use \Hash;
class TbAdminServices extends Eloquent {
	use SoftDeletingTrait;
	//use UserTrait, RemindableTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $dates = ['deleted_at'];
	public $table = 'tbservices';
	public $timestamps=false;
}

?>