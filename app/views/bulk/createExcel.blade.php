@extends('userLayout')
@section('content')
<style type="text/css">
.btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
</style>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     ارسال انبوه <small>اکسل</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-send"></i> ارسال از طریق فایل اکسل
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <a href="{{url('bulk/create')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
    <p></p>
    @if(isset($successmessage))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$successmessage}}
    </div>
    @endif
    @if(isset($failmessage))
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$failmessage}}
    </div>
    @endif
    <p></p>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-info">
                
                فایل Excel باید دقیقا مشابه فایل نمونه باشد. لطفا فایل نمونه را دانلود کرده و با توجه به موارد ذکر شده در sheet راهنما،  sheet ورود شماره تلفن را پر کنید.
                <p>

                </p>
                <p>
                    <a href="{{url('bulk/excel/sample')}}" class="btn btn-default">دانلود فایل نمونه</a>
                </p>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12 well">
            {{ Form::open(['url' => 'bulk/excel/store', 'method' => 'POST','files'=>true]) }}
            <p></p>
            <div class="row">
              <div class="col-sm-3">
                 <label class="control-label" for=""> عنوان پیغام : </label>
              </div>
              <div class="col-sm-4">
                 {{ Form::text('bulk_title',isset($bulk_title)?$bulk_title:'',array('class'=>'form-control')) }}
              </div>
              <div class="col-sm-5">
                @if(isset($messages)&& isset($messages['bulk_title']))
                  {{$messages['bulk_title'][0]}}
                @endif
              </div>
            </div>
            <p></p>
            <div class="row">
              <div class="col-sm-3">
                 <label class="control-label" for=""> پیغام : </label>
              </div>
              <div class="col-sm-4">
                 {{ Form::textarea('bulk_msg',isset($bulk_msg)?$bulk_msg:'',array('class'=>'form-control')) }}
              </div>
              <div class="col-sm-5">
                @if(isset($messages)&& isset($messages['bulk_msg']))
                  {{$messages['bulk_msg'][0]}}
                @endif
              </div>
            </div>
            <p></p>
            <div class="row">
              <div class="col-sm-3">
                 <label class="control-label" for="">
                    <code>انتخاب فایل اکسل</code></label>
              </div>
              <div class="col-sm-4">
                 <span class="btn btn-default btn-file form-control">
                    انتخاب فایل  {{ Form::file('file','',array('class'=>"form-control",'required'=>'required')) }}
                </span>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <p></p>
             <div class="row">
              <div class="col-sm-3">
              </div>
              <div class="col-sm-4">
                {{ Form::submit('ارسال', ['class' => 'btn btn-success pull-right'])}}
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
      $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
        });
            $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label){
                var input = $(this).parent();
                input.children("span").remove();
                input.append("<span> :"+label+"</span>");
                //console.log(input.text());
                //console.log(numFiles);
               // console.log(label);
            });
        });
</script>
@stop