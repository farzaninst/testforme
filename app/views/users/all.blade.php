@extends('userLayout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
     مدیریت کاربران
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="glyphicon glyphicon-th-list"></i> لیست کاربران
      </li>
    </ol>
  </div>
</div>
<div class="col-lg-12"> 
    <a href="{{url('users/groups')}}" class="btn-lg btn-success hvr-radial-out-success">مدیریت گروه های کاربری</a>
    <a href="{{url('users/create')}}" class="btn-lg btn-success hvr-radial-out-success">کاربر جدید</a>
    <a href="{{url('users/excel')}}" class="btn-lg btn-success hvr-radial-out-success">افزودن کاربران جدید از طریق فایل excel</a>
    <p></p>
    @if(isset($messages))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
        {{$messages}}
    </div>
    @endif
    <p></p>
    <div class="table-responsive clear back-white">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>نام کاربر</th>
                    <th>ایمیل</th>
                     <th>تحصیلات</th>
                    <th>موبایل</th>
                    <th style="color:transparent;">........................</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($users as $ctr)
                <tr>
                    <td>{{ $ctr->fname.' '.$ctr->lname }}</td>
                    <td>{{ $ctr->email}}</td>
                    <td>{{ (isset($education[$ctr->education]))?$education[$ctr->education]:$education[0]}}</td>
                    <td>{{ $ctr->phone}}</td>
                    <td>
                        <a href="{{url('users/'.$ctr->id.'/edit')}}" class="btn btn-info pull-left hvr-radial-out-info" style="margin-right: 3px;">ویرایش</a>
                        {{ Form::open(['url' => 'users/delete/' . $ctr->id, 'method' => 'POST']) }}
                        {{ Form::submit('حذف', ['class' => 'btn btn-danger pull-right hvr-bounce-to-right-danger'])}}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </div>
 
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
<script type="text/javascript">
            $(document).ready(function(){
              $("input.btn-danger").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
              $("a#delete").click(function(){
                if (!confirm("آیا می خواهید آیتم را حذف کنید؟")){
                  return false;
                }
              });
            });
</script>
@stop