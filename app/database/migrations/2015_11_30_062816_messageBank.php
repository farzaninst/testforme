<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessageBank extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbmessagebank', function($table)
		 {
			 $table->increments('id');
			 $table->integer('centerid')->unsigned()->nullable();
			 $table->string('title');
			 $table->text('text')->nullable();
			 $table->timestamps();
			 $table->engine='InnoDB';
		 });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbmessagebank');
	}

}
