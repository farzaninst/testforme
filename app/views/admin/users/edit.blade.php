@extends('userLayout')

@section('header')
<h1 class="">مدیریت کاربران: ویرایش کاربر</h1>
@stop
@section('content')
<div class="row">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h1>ثبت کاربر مدیر جدید</h1>
		<hr></hr>
		@if(isset($actionmsg))
        	{{$actionmsg}}
    	@endif
		 {{ Form::open(array('url' => 'admin/users/'.$id.'/update','class'=>'form-horizontal well')) }}
		 {{-- Username field. ------------------------}}
		<div class="row">
			<div class="col-sm-3">
				 {{ Form::label('username', 'نام کاربری*',array('class'=>'control-label')) }}
			</div>
			<div class="col-sm-4">
				 {{ Form::text('username',(isset($newuser)?$newuser['username']:''),array('class'=>'form-control','disabled')) }}
			</div>
			<div class="col-sm-5">
				 @if(isset($messages,$messages['username']))
				<span class="error">{{$messages['username'][0]}}</span>
				@endif
			</div>
		</div>
		<p></p>
		<div class="row">
			<div class="col-sm-3">
				 {{-- Email address field. -------------------}}
				 {{ Form::label('email', 'ایمیل*',array('class'=>'control-label')) }}
			</div>
			<div class="col-sm-4">
				{{ Form::email('email',(isset($newuser)? $newuser['email']:''),array('class'=>'form-control','disabled')) }}
			</div>
			<div class="col-sm-5">
				 @if(isset($messages,$messages['email']))
				<span class="error">{{$messages['email'][0]}}</span>
				@endif
			</div>
		</div>
		<p></p>
				 {{-- fname field. ------------------------}}
		<div class="row">
			<div class="col-sm-3">	 
				 {{ Form::label('fname', 'نام',array('class'=>'control-label')) }}
			</div>
			<div class="col-sm-4">	 
				 {{ Form::text('fname',(isset($newuser)? $newuser['fname']:''),array('class'=>'form-control')) }}
			</div>
			<div class="col-sm-5">
				 @if(isset($messages,$messages['fname']))
				<span class="error">{{$messages['fname'][0]}}</span>
				@endif
			</div>
		</div>
		<p></p>
		<div class="row">
			<div class="col-sm-3">
				 {{-- lname field. ------------------------}}
				 {{ Form::label('lname', 'نام خانوادگی',array('class'=>'control-label')) }}
			</div>
			<div class="col-sm-4">	 
				 {{ Form::text('lname',(isset($newuser)? $newuser['lname']:''),array('class'=>'form-control')) }}
			</div>
			<div class="col-sm-5">
				 @if(isset($messages,$messages['lname']))
				<span class="error">{{$messages['lname'][0]}}</span>
				@endif
			</div>
		</div>
		<p></p>
		<div class="row">
			<div class="col-sm-3">
				 {{-- Password field. ------------------------}}
				 {{ Form::label('password', 'رمز عبور*',array('class'=>'control-label')) }}
			</div>
			<div class="col-sm-4">
				 {{ Form::password('password',array('class'=>'form-control')) }}
			</div>
			<div class="col-sm-5">
				<span class="error"> در صورتی که نمی خواهید رمز عبور را تغییر دهید فیلد های مربوط به رمز عبور را پر نکنید</span>
			</div>
		</div>
		<p></p>
		<div class="row">
			<div class="col-sm-3">
				{{-- Password confirmation field. -----------}}
				{{ Form::label('password_confirmation', 'تکرار رمز عبور*',array('class'=>'control-label')) }}
			</div>
			<div class="col-sm-4">
				   {{ Form::password('password_confirmation',array('class'=>'form-control')) }}
			</div>
			<div class="col-sm-5">
				@if(isset($passerr))
					<span class="error">
					    {{$passerr}}
					</span>
				@endif
			</div>
		</div>
		<p></p>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-4">
				 {{-- Form submit button. --------------------}}
				 {{ Form::submit('ویرایش',array('class'=>'form-control btn btn-primary')) }}
			</div>
		</div>
		{{ Form::close() }}
	</div>

</div>
<div class="col-lg-2 col-lg-offset-1">	
	<a href="{{url('admin/users')}}" class="btn btn-success">بازگشت</a>
</div>
@stop