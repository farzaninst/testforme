
@extends('userLayout')

@section('header')
<h1> مدیریت کاربران: کاربر جدید </h1>
@if(isset($createSuccess))
    {{$createSuccess}}
@endif
@stop

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ثبت کاربر مدیر جدید</h1>
			<hr></hr>
			 {{ Form::open(array('url' => 'admin/users/store','class'=>'form-horizontal well')) }}
			 {{-- Username field. ------------------------}}
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('username', 'نام کاربری*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('username',(isset($newuser)?$newuser['username']:''),array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			 @if(isset($messages,$messages['username']))
			<span class="error">{{$messages['username'][0]}}</span>
			@endif
		</div>
	</div>
	<p></p>
			 {{-- fname field. ------------------------}}
	<div class="row">
		<div class="col-sm-3">	 
			 {{ Form::label('fname', 'نام',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">	 
			 {{ Form::text('fname',(isset($newuser)? $newuser['fname']:''),array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			 @if(isset($messages,$messages['fname']))
			<span class="error">{{$messages['fname'][0]}}</span>
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{-- lname field. ------------------------}}
			 {{ Form::label('lname', 'نام خانوادگی',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">	 
			 {{ Form::text('lname',(isset($newuser)? $newuser['lname']:''),array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			 @if(isset($messages,$messages['lname']))
			<span class="error">{{$messages['lname'][0]}}</span>
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{-- Email address field. -------------------}}
			 {{ Form::label('email', 'ایمیل*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			{{ Form::email('email',(isset($newuser)? $newuser['email']:''),array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			 @if(isset($messages,$messages['email']))
			<span class="error">{{$messages['email'][0]}}</span>
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{-- Password field. ------------------------}}
			 {{ Form::label('password', 'رمز عبور*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::password('password',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			 @if(isset($messages,$messages['password']))
			<span class="error">{{$messages['password'][0]}}</span>
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			{{-- Password confirmation field. -----------}}
			{{ Form::label('password_confirmation', 'تکرار رمز عبور*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::password('password_confirmation',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages,$messages['password']))
				<span class="error">
				    {{$messages['password'][0]}}
				</span>
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
			 {{ Form::close() }}
		 </div>

	<a href="{{url('admin/users')}}" class="btn btn-success">بازگشت</a>
</div>

@stop