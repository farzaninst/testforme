<?php //<--How I wish I could remove this
    $error = Session::get('error'); //this is pass through: with('key', 'value') on form redirect
    //and this--> ?> 
<style>
.registration{
    margin:0;
    max-width:600px;
    padding:10px;
    direction:rtl;
    box-shadow: rgba(0,0,0, 0.1) 0px 0px 4px; 
    -moz-box-shadow: rgba(0,0,0, 0.1) 0px 0px 4px; 
    -webkit-box-shadow: rgba(0,0,0, 0.1) 0px 0px 4px; 
}
.registration h1{
    align:center;
}
form label{ 
    font: 14px Verdana, Tahoma, sans-serif; 
    margin-left:0px;
    color: #555555; 
    width: 120px;
    display: inline-block;
}

input{ 
    padding: 9px; 
    border: solid 1px #d5d5d5; 
    outline: 0; 
    font: 13px Verdana, Tahoma, sans-serif; 
    width: 300px; 
    background: #FFFFFF; 
    box-shadow: rgba(0,0,0, 0.1) 0px 0px 4px; 
    -moz-box-shadow: rgba(0,0,0, 0.1) 0px 0px 4px; 
    -webkit-box-shadow: rgba(0,0,0, 0.1) 0px 0px 4px; 
    } 
input:hover,.preview-block input:focus{ 
    border-color: #b1b1b1; 
   box-shadow: rgba(0,0,0, 0.20) 0px 0px 8px; 
   -moz-box-shadow: rgba(0,0,0, 0.2) 0px 0px 8px; 
   -webkit-box-shadow: rgba(0,0,0, 0.2) 0px 0px 8px; 
   transition: all .2s;
    }
.submit input{ 
    width: auto; 
    padding: 9px 20px; 
    background: #3FB8AF; 
    border: 0; 
    font-size: 14px; 
    color: #FFFFFF; 
    cursor: pointer;
    }
.submit input:hover{ 
    background: #FF3D7F; 
    border-bottom: 3px solid #FF9E9D; 
    transition: all .1s;
    }
form label .remember{
    width:auto;
    direction: rtl;
    //margin-right: 200px;
}
form input .remember{
    direction: rtl;
}
</style>
<div class="registration">
<h1>فرم ثبت نام </h1>
<hr></hr>
<?php //if(!is_null($data)) echo '<p>'.$data.'<p>' ?>
 {{ Form::open(array('url' => 'login')) }}
<ul class="errors">
@if(!empty($error))
	<li>{{$error}}</li>
@endif
@foreach($errors->all('<li>:message</li>') as $message)
	{{$message}}
@endforeach
</ul>

 {{-- Username field. ------------------------}}
 {{ Form::label('username', 'نام کاربری') }}
 {{ Form::text('username') }}
</p>
<p class="">
 {{-- Password field. ------------------------}}
 {{ Form::label('password', 'رمزعبور') }}
 {{ Form::password('password') }}
</p>
<label class="remember" for="persist"><input class="remember" type="checkbox" name="persist" tabindex="3">مرا بیاد آور</label>
 <p class="submit">
  <a href="{{URL::to('registration')}}" >registration</a>
 <a href="{{URL::to('forget')}}" >forget my password</a>
 {{-- Form submit button. --------------------}}
 {{ Form::submit('Register') }}
 </p>
 {{ Form::close() }}
</div>
