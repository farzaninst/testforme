<?php

class MessageBankLocal extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $paginate=10;
	private $centerid;
	public function __construct()
	{
		parent::__construct();
		$this->centerid=Session::get('centerid');
	}
	public function index($errormsg=null,$successmsg=null)
	{
		$data=TbMessageBank::where('centerid','=',$this->centerid)->paginate($this->paginate);
		//dd($data->toArray());
		if(Session::has('successmsg'))
			$successmsg=Session::get('successmsg');
		return View::make('messagebanklocal.all')->with('data',$data)->with('errormsg',$errormsg)->with('successmsg',$successmsg);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($errormsg=null,$successmsg=null)
	{
		if(Session::has('successmsg'))
			$successmsg=Session::pull('successmsg');
		return View::make('messagebanklocal.create')->with('errormsg',$errormsg)->with('successmsg',$successmsg);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$msg=new TbMessageBank;
		$msg->centerid=$this->centerid;
		$msg->title=Input::get('title');
		$msg->text=Input::get('text');
		$msg->save();
		return Redirect::action('MessageBankLocal@create')->with('successmsg','پیام با موفقیت ثبت شد');
		//return $this->create(null,'پیام با موفقیت ثبت شد');
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id,$errormsg=null,$successmsg=null)
	{
		$msg=TbMessageBank::where('centerid','=',$this->centerid)->find($id);
		if(!$msg)
			return "bad request";
		if(Session::has('successmsg'))
			$successmsg=Session::pull('successmsg');
		return View::make('messagebanklocal.edit')
				->with('errormsg',$errormsg)
				->with('title',$msg->title)
				->with('text',$msg->text)
				->with('msg',$msg)
				->with('successmsg',$successmsg);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//dd($id);
		$msg=TbMessageBank::find($id);
		if(!$msg)
			return "bad request";
		$msg->centerid=$this->centerid;
		$msg->title=Input::get('title');
		$msg->text=Input::get('text');
		$msg->save();
		//Session::put('successmsg','پیام با موفقیت ویرایش شد');
		return Redirect::action('MessageBankLocal@edit',[$id])->with('successmsg','پیام با موفقیت ویرایش شد');
		//return $this->edit($id,null,'پیام با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$msg=TbMessageBank::where('centerid','=',$this->centerid)->find($id);
		if(!$msg)
			return "bad request";
		$msg->delete();
		return Redirect::to('messagebank/local')->with('successmsg','پیام با موفقیت حذف شد.');
		//return $this->index(null,'پیام با موفقیت حذف شد.');
	}

	public function search()
	{
		if(!Input::has('search'))
			return $this->index();
		$searchtxt=Input::get('search');
		//var_dump($searchtxt);
		if(trim($searchtxt)=="")
			return $this->index('لطفا متنی را برای جست و جو وارد کنید.',null);
		$data=TbMessageBank::where('centerid','=',$this->centerid)
				->whereNested(function($q) use($searchtxt){
					$q->where('title','LIKE','%'.$searchtxt.'%');
					$q->orWhere('text','LIKE','%'.$searchtxt.'%');
				})->get();
		//dd($data->toArray());
		return View::make('messagebanklocal.all')->with('data',$data);

	}
}
