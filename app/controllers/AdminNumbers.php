<?php

class AdminNumbers extends \BaseController {
	private $validator;
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('authAdmin');
	}

	public function index()
	{
		$numbers = TBNumbers::get();
		$centers = TbAdminCenters::get();
		$centersnumbers = TBCentersNumbers::get();
		$softwares = TbAdminMessengers::get();
		if ($numbers) {
			$numbers = $numbers->toArray();
		}
		if ($centers) {
			$centers = $centers->toArray();
		}
		if ($centersnumbers){
			$centersnumbers = $centersnumbers->toArray();
		}
		if ($softwares) {
			$softwares = $softwares->toArray();
		}
		$data = array(
			'numbers' => $numbers,
			'centersnumbers' => $centersnumbers,
			'softwares' => $softwares,
			'centers' => $centers
		);
		return View::make('admin/numbers/create')->with($data);
	}

	public function add()
	{
		$centers = TbAdminCenters::get();
		$centersnumbers = TBCentersNumbers::get();
		$softwares = TbAdminMessengers::get();
		if ($centers) {
			$centers = $centers->toArray();
		}
		if ($centersnumbers){
			$centersnumbers = $centersnumbers->toArray();
		}
		if ($softwares) {
			$softwares = $softwares->toArray();
		}
		$data = array(
			'centersnumbers' => $centersnumbers,
			'softwares' => $softwares,
			'centers' => $centers
		);
		$errormsg=null;
		if(Session::has('errormsg'))
			$errormsg=Session::pull('errormsg');
		return View::make('admin/numbers/add')->with($data)->with('errormsg',$errormsg);
	}
	private function phoneValidation()
	{
		$role=array('phone'=>'phone');
		$this->validator=\Validator::make(array('phone'=>Input::get('number')),$role);
		if($this->validator->passes())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function store()
	{
		/*if(!$this->phoneValidation())
		{
			//$messages = $this->validator->messages();
			return Redirect::to('admin/numbers/add')->with('errormsg','شماره اشتباه می باشد.');
		}*/
		$software = Input::get('softwares');
		$center = Input::get('centers');
		$number = Input::get('number');
		$numberid = TBNumbers::insertGetId(array(
			'number' => $number,
			'softwareid' => $software
		));
		if ($center) {
			$centernumber = TBCentersNumbers::insertGetId(array(
				'numberid' => $numberid,
				'centerid'=> $center
			));	
		}
		return Redirect::to('admin/numbers')->with('Msg','شماره با موفقیت در سیستم ثبت شد .');
	}

	public function edit()
	{
		$numberid = Input::get('numberid');
		$numbers = TBNumbers::where('id','=',$numberid)->get();
		$centersnumbers = TBCentersNumbers::where('numberid','=',$numberid)->get();
		$centers = TbAdminCenters::get();
		$softwares = TbAdminMessengers::get();
		if ($numbers) {
			$numbers = $numbers->toArray();
		}
		if ($centers) {
			$centers = $centers->toArray();
		}
		if ($centersnumbers){
			$centersnumbers = $centersnumbers->toArray();
		}
		if ($softwares) {
			$softwares = $softwares->toArray();
		}
		$data = array(
			'numbers' => $numbers,
			'centersnumbers' => $centersnumbers,
			'softwares' => $softwares,
			'centers' => $centers
		);
		return View::make('admin/numbers/edit')->with($data);
	}

	public function editsave() 
	{
		$number = Input::get('number');
		$centerid = Input::get('centers');
		$softwareid = Input::get('softwares');
		$block = Input::get('block');
		return $block;
		return $center;
	}

	public function delete()
	{
		$numberid =  Input::get('numberid');
		TBCentersNumbers::where('numberid','=',$numberid)->delete();
		TBNumbers::where('id','=',$numberid)->delete();
		return Redirect::to('admin/numbers')->with('Msg','شماره با موفقیت از سیستم حذف شد .');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
