<?php
use \Eloquent;
use \Exception;
use \Hash;

class TBbulk extends Eloquent {
	public $table = 'tbbulk';
	public $timestamps=false;
}

class TBbulkQueue extends Eloquent {
	public $table = 'tbbulkqueue';
	public $timestamps=false;
}

?>