
@extends('userLayout')

@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>ویرایش مرکز</h1>
			<hr></hr>
			 {{ Form::open(array('url' => '/admin/centers/'.$center->id.'/update','class'=>'form-horizontal well')) }}
			 	<input name="id" type="hidden" value="{{$center->id}}">
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('name', 'نام مرکز*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('name',isset($name)?$name:$center->name,array('class'=>'form-control','required' => 'required')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['name']))
				{{$messages['name'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('username', 'نام کاربری مرکز*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('username',$center->username,array('class'=>'form-control','required' => 'required','disabled')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['username']))
				{{$messages['username'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('email', 'ایمیل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('email',isset($email)?$email:$center->email,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['email']))
				{{$messages['email'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('nationalIdentity', 'کد ملی',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('nationalIdentity',isset($nationalIdentity)?$nationalIdentity:$center->nationalIdentity,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['nationalIdentity']))
				{{$messages['nationalIdentity'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('phone', 'موبایل',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('phone',isset($phone)?$phone:$center->phone,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['phone']))
				{{$messages['phone'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('address', 'آدرس',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::textarea('address',isset($address)?$address:$center->address,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('birthMessage', 'متن پیام تبریک تولد',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::textarea('birthMessage',isset($birthMessage)?$birthMessage:$center->birthMessage,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('marriageMessage', 'متن پیام تبریک ازدواج',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::textarea('marriageMessage',isset($marriageMessage)?$marriageMessage:$center->marriageMessage,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{ Form::label('website', 'وب سایت',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::text('website',isset($website)?$website:$center->website,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['website']))
				{{$messages['website'][0]}}
			@endif
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			 {{-- Password field. ------------------------}}
			 {{ Form::label('password', 'رمز عبور*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			 {{ Form::password('password',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
			@if(isset($messages)&& isset($messages['password']))
				{{$messages['password'][0]}}
			@endif
		</div>
	</div>
	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{-- Password confirmation field. -----------}}
			{{ Form::label('password_confirmation', 'تکرار رمز عبور*',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::password('password_confirmation',array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>


	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('bulk', 'ارسال انبوه',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('bulk',array('0'=>'نه','1'=>'بله'),$center->bulk,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>

	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('birthmsg', 'پیام تبریک تولد',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('birthmsg',array('0'=>'نه','1'=>'بله'),$center->birthmsg,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('marriagemsg', 'پیام تبریک ازدواج',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('marriagemsg',array('0'=>'نه','1'=>'بله'),$center->marriagemsg,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('schedulemsg', 'پیام زمان بندی شده',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('schedulemsg',array('0'=>'نه','1'=>'بله'),$center->scheduledmsg,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>	
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('matchservice', 'مسابقه',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('matchservice',array('0'=>'نه','1'=>'بله'),$center->matchservice,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('opinion', 'نظرسنجی',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('opinion',array('0'=>'نه','1'=>'بله'),$center->opinion,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>		
	<div class="row">
		<div class="col-sm-3">
			{{ Form::label('enable', 'فعال',array('class'=>'control-label')) }}
		</div>
		<div class="col-sm-4">
			   {{ Form::select('enable',array('0'=>'نه','1'=>'بله'),$center->enabled,array('class'=>'form-control')) }}
		</div>
		<div class="col-sm-5">
		
		</div>
	</div>
	<p></p>
	@if(isset($services))
		@foreach($services as $srv)
			<div class="row">
				<div class="col-sm-3">
					{{ Form::label($srv[1],'سرویس '.$srv[1],array('class'=>'control-label')) }}
				</div>
				<div class="col-sm-4">
					   {{ Form::select($srv[0],array('0'=>'ندارد','1'=>'دارد'),$srv[2],array('class'=>'form-control')) }}
				</div>
				<div class="col-sm-5">
				
				</div>
			</div>
			<p></p>
		@endforeach

	@endif
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-4">
			 {{-- Form submit button. --------------------}}
			 {{ Form::submit('ثبت',array('class'=>'form-control btn btn-primary')) }}
		</div>
	</div>
 	{{ Form::close() }}
</div>

	<a href="{{url('admin/centers')}}" class="btn btn-success hvr-bounce-to-right-success">بازگشت</a>
</div>
<script>
document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
    //alert(elements[i].getAttribute("name")+" === "+elements[i].getAttribute("required"));
        if(elements[i].getAttribute("required")!=null){
            elements[i].oninvalid = function(e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("این فیلد نباید خالی باشد");
                }
            };
            elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
            };
        }
    }
});
</script>
@stop