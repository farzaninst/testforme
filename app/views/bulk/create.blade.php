@extends('userLayout')

@section('header')
<h1> header section content</h1>
@stop


@section('content')

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">
      پیام انبوه <small>ایجاد پیام انبوه</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">
        <i class="fa fa-fw fa-share"></i> ارسال انبوه
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    @if (isset($msg))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{$msg}} </strong> 
      </div>
    @endif

    @if ($errors->has('bulk_msg')) 
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> {{ $errors->first('bulk_msg') }} </strong> 
      </div>
    @endif
    
    @if (Session::get('Error'))
      <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> {{Session::get('Error')}} </strong> 
        </div>
    @endif

    @if (Session::get('Msg'))
      <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> {{Session::get('Msg')}} </strong> 
        </div>
    @endif

  {{Form::open(array('url' => '/bulk/send','class'=>'form-horizontal well'))}}
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label" for=""> عنوان پیغام : </label>
      </div>
      <div class="col-lg-5">
        <input type = "text" class="form-control" id="bulk_title" name="bulk_title" placeholder="عنوان پیام خود را وارد کنید ..." value="{{$bulk_title}}"> </input>
      </div>
      <div class="col-lg-5">
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-2">
 			  <label class="control-label" for=""> پیغام : </label>
 		  </div>
 		  <div class="col-lg-5">
        <textarea row="3" class="form-control" id="bulk_msg" name="bulk_msg">{{$bulk_msg}} </textarea>
      </div>
      <div class="col-lg-5">
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label" for=""> واردکردن شماره : </label>
      </div>
      <div class="radio col-lg-5" >
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">
                <a data-toggle="collapse" href="#collapse2" class="fa fa-long-arrow-right"> لطفا شماره را وارد کنید </a>
            </h3>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                <input type="hidden" id="numbers" name="numbers">
                <input type = "text" class="form-control" id="bulk_number" name="bulk_number" placeholder="شماره را وارد و روی ثبت کلیک کنید ..."> </input>
                <table class="table table-bordered" id="grouptable" name="grouptable" >
                    <thead>
                        <tr>
                            <th>شماره</th>
                            <th>حذف</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr></tr>
                        <tr></tr>
                    </tbody>
                </table>
                <button type="button" id="btnRegisterNumber" name="btnRegisterNumber" class="btn btn-primary" value="ثبت">ثبت</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5"></div>
    </div>

    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label" for=""> انتخاب منطقه : </label>
      </div>
      <div class="col-lg-5">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">
                لطفا منطقه را انتخاب کنید
            </h3>
          </div>
            <div class="panel-body">
                <div class="mapcontainer">
                    <div class="map"></div>
                </div>
            </div>
                <table class="table table-bordered" id="regiontable" name="regiontable" >
                    <thead>
                        <tr>
                            <th>مناطق انتخاب شده</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr></tr>
                    </tbody>
                </table>
        <input type="hidden" id="areaID" name="areaID">
        </div>
        </div>
        <div class="col-lg-5">
        </div>
    </div>
        
    <div class="form-group">
    	<div class="col-sm-2">
      </div>
      <div class="col-sm-5">
        <button type="submit" name ="submit" class="btn-lg btn-primary btn-block">ارسال پیام انبوه</button>
        <a href="{{url('bulk/excel/create')}}" class="btn btn-success btn-block hvr-radial-out-success">ارسال از طریق فایل اکسل</a>
      </div>
      <div class="col-sm-5">
      </div>
    </div>
<div class="form-group">

<div class="col-lg-7">
</div>
</div>
    {{Form::close()}} 
       </div>
    </div>
<script type="text/javascript">
  
$(document).ready(function(){

    var number = [];
    $('#grouptable').on('click','tr a.remove',function(e){
      $(this).closest('tr').remove();
      var trid = $(this).closest('tr').attr('id');
        for (var i = 0; i < number.length; i++) {
            if (number[i] == trid) {
                number[i] = "";
            }
        }
    });
    $('#btnRegisterNumber').click(function() {
    var num = $('#bulk_number').val();
    var lenght = $('#bulk_number').val().length;
    if ($.isNumeric(num) && lenght >=9) {      
      $('#bulk_number').val('');
      number.push(num);
      $('#numbers').val(JSON.stringify(number));
      $("#grouptable tbody").append( 
        "<tr id='"+ num +"'>"+
        "<td>"+ num +"</td>"+ 
        "<td><a class='remove'>حذف</a></td>"+
        "</tr>");
    } else {
      alert("لطفا شماره را به صورت صحیح وارد کنید !");
    }
    });  
  });
 </script>

<script type="text/javascript">
var region = [];
        $(function () {
            $(".mapcontainer").mapael({
                map: {
                    name: "tehran_region",
                    zoom: {
                        enabled: true
                    },
                    defaultArea: {
                        attrs: {
                            fill: "#5ba4ff",
                            stroke: "#99c7ff",
                            cursor: "pointer"
                        },
                        attrsHover: {
                            animDuration: 0
                        },
                        text: {
                            attrs: {
                                cursor: "pointer",
                                "font-size": 10,
                                fill: "#000"
                            },
                            attrsHover: {
                                animDuration: 0
                            }
                        },
                        eventHandlers: {
                            click: function (e, id, mapElem, textElem) {
                                var newData = {
                                    'areas': {}
                                };
                                //alert(region);
                                if (mapElem.originalAttrs.fill == "#5ba4ff") {
                                    region.push(id);
                                    $('#regiontable tbody').append (
                                        "<tr id='"+ id +"'>"+
                                        "<td>منطقه"+ id +"</td>"+
                                        "</tr>"
                                        );
                                    newData.areas[id] = {
                                        attrs: {
                                            fill: "#0088db"
                                        }
                                    };
                                } else {
                                    $('#'+id).remove();
                                    for (var i = 0; i < region.length; i++) {
                                        if (region[i] == id) {

                                            region[i] = "";
                                        }
                                    }
                                    newData.areas[id] = {
                                        attrs: {
                                            fill: "#5ba4ff"
                                        }
                                    };
                                }
                                $(".mapcontainer").trigger('update', [{mapOptions: newData}]);
                                $('#areaID').val(region);
                            }
                        }
                    }
                },
                areas: {
                    "22": {
                        "value": "35320445",
                        "attrs": {

                        },
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 22<\/span><br \/>"
                        }
                    },
                    "21": {
                        "value": "50586757",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 21<\/span><br \/>"
                        }

                    },
                    "20": {
                        "value": "3215988",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 20<\/span><br \/>"
                        }
                    },
                    "19": {
                        "value": "35980193",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 19<\/span><br \/>"
                        }
                    },
                    "18": {
                        "value": "81726000",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 18<\/span><br \/>"
                        }
                    },
                    "17": {
                        "value": "86165",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 17<\/span><br \/>"
                        }
                    },
                    "16": {
                        "value": "19618432",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 16<\/span><br \/>"
                        }
                    },
                    "15": {
                        "value": "89612",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 15<\/span><br \/>"
                        }
                    },
                    "14": {
                        "value": "28082541",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 14<\/span><br \/>"
                        }
                    },
                    "13": {
                        "value": "40764561",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 13<\/span><br \/>"
                        }
                    },
                    "12": {
                        "value": "3100236",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 12<\/span><br \/>"
                        }
                    },
                    "11": {
                        "value": "22620600",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 11<\/span><br \/>"
                        }
                    },
                    "10": {
                        "value": "8419000",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 10<\/span><br \/>"
                        }
                    },
                    "9": {
                        "value": "9168000",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 9<\/span><br \/>"
                        }
                    },
                    "8": {
                        "value": "347176",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 8<\/span><br \/>"
                        }
                    },
                    "7": {
                        "value": "1323535",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 7<\/span><br \/>"
                        }
                    },
                    "6": {
                        "value": "150493658",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 6<\/span><br \/>"
                        }
                    },
                    "5": {
                        "value": "273925",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 5<\/span><br \/>"
                        }
                    },
                    "4": {
                        "value": "11008000",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 4<\/span><br \/>"
                        }
                    },
                    "3": {
                        "value": "356600",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 3<\/span><br \/>"
                        }
                    },
                    "2": {
                        "value": "9099922",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 2<\/span><br \/>"
                        }
                    },
                    "1": {
                        "value": "738267",
                        "tooltip": {
                            "content": "<span style=\"font-weight:bold;\">منطقه 1<\/span><br \/>"
                        }
                    }
                }
            });
        });
</script>
@stop